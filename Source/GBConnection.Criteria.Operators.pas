unit GBConnection.Criteria.Operators;

interface

uses
  GBConnection.Criteria.Interfaces,
  GBConnection.Criteria.Types,
  GBConnection.Criteria.Utils,
  System.SysUtils,
  System.StrUtils,
  System.Variants;

type TGBConnectionCriteriaOperator = class(TInterfacedObject, IGBConnectionCriteriaOperator)

  private
    function ArrayValueToString: string;

    function IsEmptyValue: Boolean;
  protected
    FColumnName: String;
    FCompare: TGBConnectionCriteriaCompare;
    FValue: Variant;
    FIsNullIfEmpty: Boolean;
    FDataType: TGBConnectionCriteriaFieldType;

    function GetOperator: string;
    function GetCompareValue: String;

    function GetColumnName: string;
    function GetCompare: TGBConnectionCriteriaCompare;
    function GetValue: Variant;
    function GetDataType: TGBConnectionCriteriaFieldType;
    function GetIsNullIfEmpty: Boolean;

    procedure SetColumnName(const Value: String);
    procedure SetIsNullIfEmpty(const Value: Boolean);
    procedure SetCompare(const Value: TGBConnectionCriteriaCompare);
    procedure SetValue(const Value: Variant);
    procedure SetDataType(const Value: TGBConnectionCriteriaFieldType);

  public
    function AsString: string;

    constructor Create;
    class function New: IGBConnectionCriteriaOperator;
end;

type TGBConnectionCriteriaOperators = class(TInterfacedObject, IGBConnectionCriteriaOperators)

  private
    function CreateOperator(const AColumnName: String;
                            const AValue: Variant;
                            const ACompare: TGBConnectionCriteriaCompare;
                            const ADataType: TGBConnectionCriteriaFieldType): IGBConnectionCriteriaOperator;
  public
    class function New: IGBConnectionCriteriaOperators;

    function IsEqual(const AValue: Extended): String; overload;
    function IsEqual(const AValue: Integer): String; overload;
    function IsEqual(const AValue: String): String; overload;

    function IsNotEqual(const AValue: Extended): String; overload;
    function IsNotEqual(const AValue: Integer): String; overload;
    function IsNotEqual(const AValue: String): String; overload;

    function IsGreaterThan(const AValue: Extended): String; overload;
    function IsGreaterThan(const AValue: Integer): String; overload;

    function IsGreaterEqThan(const AValue: Extended): String; overload;
    function IsGreaterEqThan(const AValue: Integer): String; overload;

    function IsLessThan(const AValue: Extended): String; overload;
    function IsLessThan(const AValue: Integer): String; overload;

    function IsLessEqThan(const AValue: Extended): String; overload;
    function IsLessEqThan(const AValue: Integer): String; overload;

    function IsNull: String;
    function IsNotNull: String;

    function IsLikeFull(const AValue: String): String;
    function IsLikeLeft(const AValue: String): String;
    function IsLikeRight(const AValue: String): String;
    function IsNotLikeFull(const AValue: String): String;
    function IsNotLikeLeft(const AValue: String): String;
    function IsNotLikeRight(const AValue: String): String;

    function IsIn(const AValue: TArray<Double>): string; overload;
    function IsIn(const AValue: TArray<String>): string; overload;
    function IsIn(const AValue: String): string; overload;

    function IsNotIn(const AValue: TArray<Double>): string; overload;
    function IsNotIn(const AValue: TArray<String>): string; overload;
    function IsNotIn(const AValue: String): string; overload;

    function IsExists(const AValue: String): string; overload;
    function IsNotExists(const AValue: String): string; overload;

end;

implementation

{ TGBConnectionCriteriaOperator }

function TGBConnectionCriteriaOperator.ArrayValueToString: string;
var
  LFor: Integer;
  LValue: Variant;
  LValues: array of Variant;
begin
  Result := '(';
  LValues:= FValue;
  for LFor := 0 to Length(LValues) -1 do
  begin
    LValue := LValues[LFor];
    Result := Result + IfThen(LFor = 0, EmptyStr, ', ');
    Result := Result + IfThen(VarTypeAsText(VarType(LValue)) = 'OleStr',
                              QuotedStr(VarToStr(LValue)),
                              ReplaceStr(VarToStr(LValue), ',', '.'));
  end;
  Result := Result + ')';
end;

function TGBConnectionCriteriaOperator.AsString: string;
begin
  if (Self.IsEmptyValue) and (Self.FIsNullIfEmpty) then
    Exit( TGBConnectionCriteriaUtils.Concat( [FColumnName, 'IS NULL']));

  Result := TGBConnectionCriteriaUtils.Concat([FColumnName, GetOperator, GetCompareValue] );
end;

constructor TGBConnectionCriteriaOperator.Create;
begin
  FIsNullIfEmpty := False;
end;

function TGBConnectionCriteriaOperator.GetColumnName: string;
begin
  Result := FColumnName;
end;

function TGBConnectionCriteriaOperator.GetCompare: TGBConnectionCriteriaCompare;
begin
  Result := FCompare;
end;

function TGBConnectionCriteriaOperator.GetCompareValue: String;
begin
  if VarIsNull(FValue) then
    Exit;
  case FDataType of
    dftString:
      begin
        Result := VarToStrDef(FValue, EmptyStr);
        case FCompare of
          fcLikeFull,
          fcNotLikeFull: Result := QuotedStr(TGBConnectionCriteriaUtils.Concat(['%', Result, '%'], EmptyStr));
          fcLikeLeft,
          fcNotLikeLeft: Result := QuotedStr(TGBConnectionCriteriaUtils.Concat(['%', Result], EmptyStr));
          fcLikeRight,
          fcNotLikeRight: Result := QuotedStr(TGBConnectionCriteriaUtils.Concat([Result, '%'], EmptyStr));
        else
          Result := QuotedStr(Result);
        end;
      end;
    dftInteger: Result := VarToStrDef(FValue, EmptyStr);
    dftFloat: Result := ReplaceStr(FloatToStr(FValue), ',', '.');
    dftDate: Result := QuotedStr(VarToWideStrDef(FValue, EmptyStr));
    dftArray: Result := ArrayValueToString;
    dftText: Result := '(' + FValue + ')';
  end;
end;

function TGBConnectionCriteriaOperator.GetDataType: TGBConnectionCriteriaFieldType;
begin
  Result := FDataType;
end;

function TGBConnectionCriteriaOperator.GetIsNullIfEmpty: Boolean;
begin
  Result := FIsNullIfEmpty;
end;

function TGBConnectionCriteriaOperator.GetOperator: string;
begin
  case FCompare of
    fcEqual: Result := '=';
    fcNotEqual: Result := '<>';
    fcGreater: Result := '>';
    fcGreaterEqual: Result := '>=';
    fcLess: Result := '<';
    fcLessEqual: Result := '<=';
    fcIn: Result := 'in';
    fcNotIn: Result := 'not in';
    fcIsNull: Result := 'is null';
    fcIsNotNull: Result := 'is not null';
    fcBetween: Result := 'between';
    fcNotBetween: Result := 'not between';
    fcExists: Result := 'exists';
    fcNotExists: Result := 'not exists';
    fcLikeFull,
    fcLikeLeft,
    fcLikeRight: Result := 'like';
    fcNotLikeFull,
    fcNotLikeLeft,
    fcNotLikeRight: Result := 'not like';
  end;
end;

function TGBConnectionCriteriaOperator.GetValue: Variant;
begin
  Result := FValue;
end;

function TGBConnectionCriteriaOperator.IsEmptyValue: Boolean;
begin
  Result := False;

  if FValue = Null then
  begin
    Result := True;
    Exit;
  end;

  if VarIsStr(FValue) then
  begin
    if (VarToStr(FValue).IsEmpty) then
      Result := True;
    Exit;
  end;

  if VarIsNumeric(FValue) then
  begin
    if (FValue = 0) then
      Result := True;
    Exit;
  end;

  if VarIsFloat(FValue) then
  begin
    if (FValue = 0) then
      Result := True;
    Exit;
  end;

  if VarIsArray(FValue) then
    Exit(False);

  if VarToDateTime(FValue) = 0 then
    Exit(True);
end;

class function TGBConnectionCriteriaOperator.New: IGBConnectionCriteriaOperator;
begin
  Result := Self.Create;
end;

procedure TGBConnectionCriteriaOperator.SetColumnName(const Value: String);
begin
  FColumnName := Value;
end;

procedure TGBConnectionCriteriaOperator.SetCompare(const Value: TGBConnectionCriteriaCompare);
begin
  FCompare := Value;
end;

procedure TGBConnectionCriteriaOperator.SetDataType(const Value: TGBConnectionCriteriaFieldType);
begin
  FDataType := Value;
end;

procedure TGBConnectionCriteriaOperator.SetIsNullIfEmpty(const Value: Boolean);
begin
  FIsNullIfEmpty := Value;
end;

procedure TGBConnectionCriteriaOperator.SetValue(const Value: Variant);
begin
  FValue := Value;
end;

{ TGBConnectionCriteriaOperators }

function TGBConnectionCriteriaOperators.CreateOperator(
    const AColumnName: String;
    const AValue: Variant;
    const ACompare: TGBConnectionCriteriaCompare;
    const ADataType: TGBConnectionCriteriaFieldType): IGBConnectionCriteriaOperator;
begin
  Result := TGBConnectionCriteriaOperator.New;
  Result.ColumnName := AColumnName;
  Result.Compare := ACompare;
  Result.Value := AValue;
  Result.DataType := ADataType;
end;

function TGBConnectionCriteriaOperators.IsEqual(const AValue: String): String;
begin
  Result := CreateOperator(EmptyStr, AValue, fcEqual, dftString).AsString;
end;

function TGBConnectionCriteriaOperators.IsEqual(const AValue: Integer): String;
begin
  Result := CreateOperator(EmptyStr, AValue, fcEqual, dftInteger).AsString;
end;

function TGBConnectionCriteriaOperators.IsEqual(const AValue: Extended): String;
begin
  Result := CreateOperator(EmptyStr, AValue, fcEqual, dftFloat).AsString;
end;

function TGBConnectionCriteriaOperators.IsExists(const AValue: String): string;
begin
  Result := CreateOperator(EmptyStr, AValue, fcExists, dftString).AsString;
end;

function TGBConnectionCriteriaOperators.IsGreaterEqThan(const AValue: Integer): String;
begin
  Result := CreateOperator(EmptyStr, AValue, fcGreaterEqual, dftInteger).AsString;
end;

function TGBConnectionCriteriaOperators.IsGreaterEqThan(const AValue: Extended): String;
begin
  Result := CreateOperator(EmptyStr, AValue, fcGreaterEqual, dftFloat).AsString;
end;

function TGBConnectionCriteriaOperators.IsGreaterThan(const AValue: Integer): String;
begin
  Result := CreateOperator(EmptyStr, AValue, fcGreater, dftInteger).AsString;
end;

function TGBConnectionCriteriaOperators.IsGreaterThan(const AValue: Extended): String;
begin
  Result := CreateOperator(EmptyStr, AValue, fcGreater, dftFloat).AsString;
end;

function TGBConnectionCriteriaOperators.IsIn(const AValue: TArray<Double>): string;
begin
  Result := CreateOperator(EmptyStr, AValue, fcIn, dftArray).AsString;
end;

function TGBConnectionCriteriaOperators.IsIn(const AValue: String): string;
begin
  Result := CreateOperator(EmptyStr, AValue, fcIn, dftString).AsString;
end;

function TGBConnectionCriteriaOperators.IsIn(const AValue: TArray<String>): string;
begin
  Result := CreateOperator(EmptyStr, AValue, fcIn, dftArray).AsString;
end;

function TGBConnectionCriteriaOperators.IsLessEqThan(const AValue: Integer): String;
begin
  Result := CreateOperator(EmptyStr, AValue, fcLessEqual, dftInteger).AsString;
end;

function TGBConnectionCriteriaOperators.IsLessEqThan(const AValue: Extended): String;
begin
  Result := CreateOperator(EmptyStr, AValue, fcLessEqual, dftFloat).AsString;
end;

function TGBConnectionCriteriaOperators.IsLessThan(const AValue: Integer): String;
begin
  Result := CreateOperator(EmptyStr, AValue, fcLess, dftInteger).AsString;
end;

function TGBConnectionCriteriaOperators.IsLessThan(const AValue: Extended): String;
begin
  Result := CreateOperator(EmptyStr, AValue, fcLess, dftFloat).AsString;
end;

function TGBConnectionCriteriaOperators.IsLikeFull(const AValue: String): String;
begin
  Result := CreateOperator(EmptyStr, AValue, fcLikeFull, dftString).AsString;
end;

function TGBConnectionCriteriaOperators.IsLikeLeft(const AValue: String): String;
begin
  Result := CreateOperator(EmptyStr, AValue, fcLikeLeft, dftString).AsString;
end;

function TGBConnectionCriteriaOperators.IsLikeRight(const AValue: String): String;
begin
  Result := CreateOperator(EmptyStr, AValue, fcLikeRight, dftString).AsString;
end;

function TGBConnectionCriteriaOperators.IsNotEqual(const AValue: String): String;
begin
  Result := CreateOperator(EmptyStr, AValue, fcNotEqual, dftString).AsString;
end;

function TGBConnectionCriteriaOperators.IsNotEqual(const AValue: Integer): String;
begin
  Result := CreateOperator(EmptyStr, AValue, fcNotEqual, dftInteger).AsString;
end;

function TGBConnectionCriteriaOperators.IsNotEqual(const AValue: Extended): String;
begin
  Result := CreateOperator(EmptyStr, AValue, fcNotEqual, dftFloat).AsString;
end;

function TGBConnectionCriteriaOperators.IsNotExists(const AValue: String): string;
begin
  Result := CreateOperator(EmptyStr, AValue, fcNotExists, dftString).AsString;
end;

function TGBConnectionCriteriaOperators.IsNotIn(const AValue: TArray<Double>): string;
begin
  Result := CreateOperator(EmptyStr, AValue, fcNotIn, dftArray).AsString;
end;

function TGBConnectionCriteriaOperators.IsNotIn(const AValue: String): string;
begin
  Result := CreateOperator(EmptyStr, AValue, fcNotIn, dftString).AsString;
end;

function TGBConnectionCriteriaOperators.IsNotIn(const AValue: TArray<String>): string;
begin
  Result := CreateOperator(EmptyStr, AValue, fcNotIn, dftArray).AsString;
end;

function TGBConnectionCriteriaOperators.IsNotLikeFull(const AValue: String): String;
begin
  Result := CreateOperator(EmptyStr, AValue, fcNotLikeFull, dftString).AsString;
end;

function TGBConnectionCriteriaOperators.IsNotLikeLeft(const AValue: String): String;
begin
  Result := CreateOperator(EmptyStr, AValue, fcNotLikeLeft, dftString).AsString;
end;

function TGBConnectionCriteriaOperators.IsNotLikeRight(const AValue: String): String;
begin
  Result := CreateOperator(EmptyStr, AValue, fcNotLikeRight, dftString).AsString;
end;

function TGBConnectionCriteriaOperators.IsNotNull: String;
begin
  Result := CreateOperator(EmptyStr, Null, fcIsNotNull, dftUnknown).AsString;
end;

function TGBConnectionCriteriaOperators.IsNull: String;
begin
  Result := CreateOperator(EmptyStr, Null, fcIsNull, dftUnknown).AsString;
end;

class function TGBConnectionCriteriaOperators.New: IGBConnectionCriteriaOperators;
begin
  Result := Self.Create;
end;

end.
