unit GBConnection.Criteria.Utils;

interface

uses
  System.SysUtils;

type TGBConnectionCriteriaUtils = class

  private
    class function AddToList(const AList, ADelimiter, ANewElement: String): String;

  public
    class function Concat(const AElements: array of String; const ADelimiter: String = ' '): String;
end;

implementation

{ TGBConnectionCriteriaUtils }

class function TGBConnectionCriteriaUtils.AddToList(const AList, ADelimiter, ANewElement: String): String;
begin
  Result := AList;
  if Result <> '' then
    Result := Result + ADelimiter;
  Result := Result + ANewElement;
end;

class function TGBConnectionCriteriaUtils.Concat(const AElements: array of String; const ADelimiter: String): String;
var
  LValue: String;
begin
  Result := '';
  for LValue in AElements do
    if LValue <> '' then
      Result := AddToList(Result, ADelimiter, LValue);
end;

end.
