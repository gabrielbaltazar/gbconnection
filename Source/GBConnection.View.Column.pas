unit GBConnection.View.Column;

interface

uses
  GBConnection.View.Lista.Interfaces;

type TGBConnectionViewColumn<T: class, constructor> = class(TInterfacedObject, IGBConnectionViewColumn<T>)

  private
    [Weak] FParent: IGBConnectionViewListColumn<T>;

    FCaption   : string;
    FFieldName : string;
    FWidth     : Integer;

    function Caption  (Value: String) : IGBConnectionViewColumn<T>; overload;
    function FieldName(Value: String) : IGBConnectionViewColumn<T>; overload;
    function Width    (Value: Integer): IGBConnectionViewColumn<T>; overload;

    function Caption  : String;  overload;
    function FieldName: String;  overload;
    function Width    : Integer; overload;

    function &End: IGBConnectionViewListColumn<T>;

  public
    constructor create(Parent: IGBConnectionViewListColumn<T>);
    class function New(Parent: IGBConnectionViewListColumn<T>): IGBConnectionViewColumn<T>;
end;

implementation

{ TGBConnectionViewColumn<T> }

function TGBConnectionViewColumn<T>.Caption: String;
begin
  result := FCaption;
end;

function TGBConnectionViewColumn<T>.Caption(Value: String): IGBConnectionViewColumn<T>;
begin
  Result   := Self;
  FCaption := Value;
end;

constructor TGBConnectionViewColumn<T>.create(Parent: IGBConnectionViewListColumn<T>);
begin
  FParent := Parent;
end;

function TGBConnectionViewColumn<T>.&End: IGBConnectionViewListColumn<T>;
begin
  result := FParent;
end;

function TGBConnectionViewColumn<T>.FieldName(Value: String): IGBConnectionViewColumn<T>;
begin
  result := Self;
  FFieldName := Value;
end;

function TGBConnectionViewColumn<T>.FieldName: String;
begin
  result := FFieldName;
end;

class function TGBConnectionViewColumn<T>.New(Parent: IGBConnectionViewListColumn<T>): IGBConnectionViewColumn<T>;
begin
  result := Self.create(Parent);
end;

function TGBConnectionViewColumn<T>.Width(Value: Integer): IGBConnectionViewColumn<T>;
begin
  result := Self;
  FWidth := Value;
end;

function TGBConnectionViewColumn<T>.Width: Integer;
begin
  result := FWidth;
end;

end.
