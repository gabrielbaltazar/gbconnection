unit GBConnection.Data.Model.Types;

interface

type
  TDataModelDelphiType = (dtBlob,
                          dtBoolean,
                          dtDate,
                          dtDateTime,
                          dtFloat,
                          dtInteger,
                          dtString
                         );

  TDataModelDelphiTypeHelper = record helper for TDataModelDelphiType
  public
    function toString: String;
    function toFieldTypeString: string;
  end;

implementation

{ TDataModelDelphiTypeHelper }

function TDataModelDelphiTypeHelper.toFieldTypeString: string;
begin
  case Self of
    dtBlob: Result := 'ftBlob';
    dtBoolean: Result := 'ftBoolean';
    dtDate: Result := 'ftDate';
    dtDateTime: Result := 'ftDatetime';
    dtFloat: Result := 'ftFloat';
    dtInteger: Result := 'ftInteger';
    dtString: result := 'ftString';
  end;
end;

function TDataModelDelphiTypeHelper.toString: String;
begin
  case Self of
    dtBlob: Result := 'blob';
    dtBoolean: Result := 'boolean';
    dtDate: Result := 'date';
    dtDateTime: Result := 'datetime';
    dtFloat: Result := 'float';
    dtInteger: Result := 'integer';
    dtString: result := 'string';
  end;
end;

end.

