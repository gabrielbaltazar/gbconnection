unit GBConnection.Model.Firedac.DriverLink;

interface

uses
  GBConnection.Model.Interfaces,
  FireDAC.Phys,
  FireDAC.Phys.FB,
  FireDAC.Phys.MySQL,
  {$IFNDEF COMMUNITY}
  FireDAC.Phys.MSSQL,
  FireDAC.Phys.Oracle,
  {$ENDIF}
  FireDAC.Phys.PG,
  FireDAC.Phys.SQLite,
  System.SysUtils;

type TGBConnectionModelFiredacDriver = class

  private
    class function GetPostgresDriver: TFDPhysDriverLink;
    class function GetMySQLDriver: TFDPhysDriverLink;
    class function GetSQLiteDriver: TFDPhysDriverLink;
    class function GetFirebirdDriver: TFDPhysDriverLink;
    {$IFNDEF COMMUNITY}
    class function GetOracleDriver: TFDPhysDriverLink;
    class function GetMSSQLDriver: TFDPhysDriverLink;
    {$ENDIF}

  public
    class function GetDriver(Params: IGBConnectionParams<IGBConnection>): TFDPhysDriverLink;

end;

implementation

{ TGBConnectionModelFiredacDriver }

class function TGBConnectionModelFiredacDriver.GetDriver(Params: IGBConnectionParams<IGBConnection>): TFDPhysDriverLink;
begin
  Result := nil;
  case Params.Driver of
    gbSQLite: Result := GetSQLiteDriver;
    gbMySql: Result := GetMySQLDriver;
    gbPostgres: Result := GetPostgresDriver;
    gbFirebird: Result := GetFirebirdDriver;
    {$IFNDEF COMMUNITY}
    gbOracle: Result := GetOracleDriver;
    gbMSSQL: Result := GetMSSQLDriver;
    {$ENDIF}
  end;
end;

class function TGBConnectionModelFiredacDriver.GetFirebirdDriver: TFDPhysDriverLink;
begin
  Result := TFDPhysFBDriverLink.Create(nil);
  Result.VendorLib := 'fbclient.dll';
end;

{$IFNDEF COMMUNITY}
class function TGBConnectionModelFiredacDriver.GetMSSQLDriver: TFDPhysDriverLink;
begin
  Result := TFDPhysMSSQLDriverLink.Create(nil);
end;
{$ENDIF}

class function TGBConnectionModelFiredacDriver.GetMySQLDriver: TFDPhysDriverLink;
begin
  Result := TFDPhysMySQLDriverLink.create(nil);
end;

{$IFNDEF COMMUNITY}
class function TGBConnectionModelFiredacDriver.GetOracleDriver: TFDPhysDriverLink;
begin
  Result := TFDPhysOracleDriverLink.Create(nil);
end;
{$ENDIF}

class function TGBConnectionModelFiredacDriver.GetPostgresDriver: TFDPhysDriverLink;
begin
  Result := TFDPhysPgDriverLink.Create(nil);
  Result.VendorLib := '';
end;

class function TGBConnectionModelFiredacDriver.GetSQLiteDriver: TFDPhysDriverLink;
begin
  Result := TFDPhysSQLiteDriverLink.Create(nil);
  Result.VendorLib := 'sqlite3.dll';
end;

end.
