unit GBConnection.Data.Model.TableRow;

interface

uses
  GBConnection.Data.Model.Field,
  System.Generics.Collections;

type TGBConnectionDataModelTableRow = class
  private
    Ffields: TObjectList<TGBConnectionDataModelField>;

  public
    property fields: TObjectList<TGBConnectionDataModelField> read Ffields write Ffields;

    constructor Create;
    destructor Destroy; override;
end;

implementation

{ TGBConnectionDataModelRow }

constructor TGBConnectionDataModelTableRow.Create;
begin
  Ffields := TObjectList<TGBConnectionDataModelField>.Create;
end;

destructor TGBConnectionDataModelTableRow.Destroy;
begin
  Ffields.Free;
  inherited;
end;

end.
