unit GBConnection.View.Lookup;

interface

uses
  Data.DB,
  Datasnap.DBClient,
  GBConnection.Controller,
  GBConnection.Dao.Interfaces,
  GBConnection.Dao.ORMBr.DataSet,
  System.Rtti,
  System.Classes,
  System.SysUtils,
  System.Generics.Collections,
  ormbr.mapping.rttiutils,
  Vcl.DbCtrls;

type TGBConnectionViewLookup<T: class, constructor> = class
  private
    FComponent        : TComponent;
    FController       : IGBConnectionController;
    FORMBrDataSet     : IGBModelDaoDataSet<T>;
    FDataSource       : TDataSource;
    FParentDataSource : TDataSource;
    FProperties       : TDictionary<string, String>;
    FIndexes          : TStrings;

    function ValueSelected: Boolean;

  public
    function AddKeyField(LookupField, PropertyField: String): TGBConnectionViewLookup<T>;
    function AddIndex(Value: String): TGBConnectionViewLookup<T>;

    function Component(Value: TComponent): TGBConnectionViewLookup<T>;

    function DataSource      (Value: TDataSource): TGBConnectionViewLookup<T>; overload;
    function ParentDataSource(Value: TDataSource): TGBConnectionViewLookup<T>; overload;

    function Controller(Value: IGBConnectionController): TGBConnectionViewLookup<T>;
    function Open: TGBConnectionViewLookup<T>; overload;
    function Build: TGBConnectionViewLookup<T>;

    function SetValues   <M: class>(AModel: M): TGBConnectionViewLookup<T>;
    function LocateValues(AModel: TObject): TGBConnectionViewLookup<T>;

    constructor create;
    destructor Destroy; override;
end;

implementation

{ TGBConnectionViewLookup<T> }

function TGBConnectionViewLookup<T>.AddIndex(Value: String): TGBConnectionViewLookup<T>;
begin
  result := Self;
  if FIndexes.IndexOf(Value.ToLower) < 0 then
    FIndexes.Add(Value.ToLower);
end;

function TGBConnectionViewLookup<T>.AddKeyField(LookupField, PropertyField: String): TGBConnectionViewLookup<T>;
begin
  result := Self;
  if not FProperties.ContainsKey(PropertyField) then
    FProperties.Add(PropertyField, LookupField);
end;

function TGBConnectionViewLookup<T>.Build: TGBConnectionViewLookup<T>;
begin
  result := Self;
  FORMBrDataSet := TGBConnectionDAOORMBrDataSet<T>.create(FController.Connection)
                        .DataSource(FDataSource);
end;

function TGBConnectionViewLookup<T>.Component(Value: TComponent): TGBConnectionViewLookup<T>;
begin
  result := Self;
  FComponent := Value;
end;

function TGBConnectionViewLookup<T>.Controller(Value: IGBConnectionController): TGBConnectionViewLookup<T>;
begin
  result := Self;
  FController := Value;
end;

constructor TGBConnectionViewLookup<T>.create;
begin
  FProperties := TDictionary<string, String>.create;
  FIndexes    := TStringList.Create;
end;

function TGBConnectionViewLookup<T>.DataSource(Value: TDataSource): TGBConnectionViewLookup<T>;
begin
  result := Self;
  FDataSource := Value;
end;

destructor TGBConnectionViewLookup<T>.Destroy;
begin
  FProperties.Free;
  FIndexes.Free;
  inherited;
end;

function TGBConnectionViewLookup<T>.LocateValues(AModel: TObject): TGBConnectionViewLookup<T>;
var
  props  : string;
  key    : string;
  values : TArray<Variant>;
begin
  result := Self;
  for props in FProperties.Keys do
  begin
    SetLength(values, Length(values) + 1);

    if not key.IsEmpty then key := key + ';';

    key := key + FProperties.Items[props];
    values[Length(values) - 1] := TRttiSingleton.GetInstance
                                    .GetRttiType(AModel.classType)
                                    .GetProperty(props)
                                    .GetValue(TObject(AModel))
                                    .AsVariant;

    FParentDataSource.DataSet.FieldByName(props).ReadOnly := False;
  end;

  FDataSource.DataSet.Locate(key, values, []);
end;

function TGBConnectionViewLookup<T>.Open: TGBConnectionViewLookup<T>;
var
  i     : Integer;
  index : string;
begin
  result := Self;
  FORMBrDataSet.Open;

  for i := 0 to Pred(FIndexes.Count) do
  begin
    if i > 0 then
      index := index + ';';
    index := index + FIndexes[i];
  end;
  TClientDataSet(FDataSource.DataSet).IndexFieldNames := index;
end;

function TGBConnectionViewLookup<T>.ParentDataSource(Value: TDataSource): TGBConnectionViewLookup<T>;
begin
  result := Self;
  FParentDataSource := Value;
end;

function TGBConnectionViewLookup<T>.SetValues<M>(AModel: M): TGBConnectionViewLookup<T>;
var
  props: string;
  value: Variant;
begin
  result := Self;
  for props in FProperties.Keys do
  begin
    if not ValueSelected then
    begin
      TRttiSingleton.GetInstance
      .GetRttiType(AModel.classType)
      .GetProperty(props)
      .SetValue(TObject(AModel), TValue.Empty);
      exit;
    end;
    value := FDataSource.DataSet.FieldByName(FProperties.Items[props]).Value;
    TRttiSingleton.GetInstance
      .GetRttiType(AModel.classType)
      .GetProperty(props)
      .SetValue(TObject(AModel), TValue.FromVariant(value));
  end;
end;

function TGBConnectionViewLookup<T>.ValueSelected: Boolean;
begin
  result := False;

  if FComponent is TCustomDBLookupComboBox then
    result := not TCustomDBLookupComboBox(FComponent).Text.IsEmpty;

end;

end.
