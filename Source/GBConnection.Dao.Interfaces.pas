unit GBConnection.Dao.Interfaces;

interface

uses
  Data.DB,
  System.Classes,
  System.Generics.Collections,
  GBConnection.Model.Interfaces,
  GBConnection.Criteria.Interfaces,
  ormbr.container.dataset.interfaces;

type
  IGBModelDaoValidator<T: class, constructor> = interface;
  IGBModelDaoDataSet<T: class, constructor> = interface;
  IGBModelDao<T: class, constructor> = interface;

  IGBDAOConnection = interface
    ['{B8F7699B-37D8-4C58-BABB-83167E256AB6}']
    procedure StartTransaction;
    procedure Commit;
    procedure Rollback;
  end;

  IGBModelDaoDataSet<T: class, constructor> = interface(IGBDAOConnection)
    ['{A643BE82-78B6-42B2-AD83-176DE2EAA1C2}']
    function Container: IContainerDataSet<T>;

    function KeyNames: TArray<String>;
    function KeyValues: TArray<Variant>;

    function Open(const Values: array of variant): IGBModelDaoDataSet<T>; overload;
    function Open: IGBModelDaoDataSet<T>; overload;

    function ApplyUpdates: IGBModelDaoDataSet<T>;
    function Save: IGBModelDaoDataSet<T>;
    function Delete: IGBModelDaoDataSet<T>;

    function DataSet(Value: TDataSet): IGBModelDaoDataSet<T>; overload;
    function DataSource(Value: TDataSource): IGBModelDaoDataSet<T>;

    function This: T;
    function NewThis: T; overload;
    function NewThis(Value: T): IGBModelDaoDataSet<T>; overload;
  end;

  IGBModelDao<T: class, constructor> = interface(IGBDAOConnection)
    ['{EC0F2D3E-4C75-4E62-8242-37124DD387C5}']
    procedure QueryParams(AValue: TDictionary<String, String>);
    procedure SetPage(const AValue: Integer);
    procedure SetPageSize(const AValue: Integer);

    procedure Insert(Value: T);
    procedure Update(Value: T);
    procedure Delete(Value: T);
    procedure Modify(Value: T);

    procedure DeleteWhere(ACriteria: IGBConnectionCriteria); overload;
    procedure DeleteWhere(AWhere: String); overload;
    procedure DeleteWhere(AWhere: string; const Args: array of const); overload;

    function Validator: IGBModelDaoValidator<T>;

    function ListAll: TObjectList<T>;
    function ListWhere(AWhere: String): TObjectList<T>; overload;
    function ListWhere(AWhere: String; const Args: array of const): TObjectList<T>; overload;
    function ListWhere(Criteria: IGBConnectionCriteria): TObjectList<T>; overload;

    function Find(AId: string): T; overload;
    function Find(Key: array of variant): T; overload;
    function FindWhere(AWhere: String): T; overload;
    function FindWhere(AWhere: String; const Args: array of const): T; overload;
    function FindWhere(Criteria: IGBConnectionCriteria): T; overload;

    function GetPrimaryKey: TArray<String>;

    procedure ManagerTransaction(Value: Boolean); overload;
    function ManagerTransaction: Boolean; overload;
  end;

  IGBModelDaoValidator<T: class, constructor> = interface
    ['{159E918C-8B40-4CB3-8A01-528062A6FB6B}']
    function Exists(AWhere: String): Boolean; overload;
    function Exists(AWhere: String; const Args: array of const): Boolean; overload;
    function Exists(ACriteria: IGBConnectionCriteria): Boolean; overload;

    function AssertExists(AWhere: String): IGBModelDaoValidator<T>; overload;
    function AssertExists(AWhere: String; const Args: array of const): IGBModelDaoValidator<T>; overload;

    function AssertExists(ACriteria: IGBConnectionCriteria; Message: String): IGBModelDaoValidator<T>; overload;
    function AssertExists(ACriteria: IGBConnectionCriteria; Message: String; const Args: array of const): IGBModelDaoValidator<T>; overload;

    function AssertNotExists(AWhere: String): IGBModelDaoValidator<T>; overload;
    function AssertNotExists(AWhere: String; const Args: array of const): IGBModelDaoValidator<T>; overload;

    function AssertNotExists(ACriteria: IGBConnectionCriteria; Message: String): IGBModelDaoValidator<T>; overload;
    function AssertNotExists(ACriteria: IGBConnectionCriteria; Message: String; const Args: array of const): IGBModelDaoValidator<T>; overload;

    procedure RunValidate(Value: T; bInsert: Boolean = True);
  end;

  IGBFactoyModelDao<T: class, constructor> = interface
    ['{ECF80120-DA6E-45A7-9AE8-A4715234137E}']
    function CreateDaoDataSet(AConn: IGBConnection): IGBModelDaoDataSet<T>;
    function CreateDaoObject (AConn: IGBConnection): IGBModelDao<T>;
  end;

implementation

end.
