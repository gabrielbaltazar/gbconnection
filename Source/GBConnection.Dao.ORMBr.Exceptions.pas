unit GBConnection.Dao.ORMBr.Exceptions;

interface

uses
  System.SysUtils,
  System.RTTI,
  System.Generics.Collections,
  ormbr.mapping.register,
  ormbr.mapping.explorer,
  ormbr.mapping.classes,
  ormbr.rtti.helper;

type
  TGBModelKey = class
  private
    FKey: string;
    FValue: string;
  public
    property key: string read FKey write FKey;
    property value: string read FValue write FValue;
  end;

  TGBModelEntity = class
  private
    FEntity: string;
    Freference: TObjectList<TGBModelKey>;
  public
    property entity: string read FEntity write FEntity;
    property reference: TObjectList<TGBModelKey> read Freference write Freference;

    procedure AddReference(Key, Value: String);

    constructor Create; overload;
    constructor Create(AEntity: String); overload;
    destructor Destroy; override;
  end;

  EGBConnectionDAO = class(Exception)
  private
    FTable: TTableMapping;
    FColumns: TColumnMappingList;
    FPrimaryKey: TPrimaryKeyColumnsMapping;

  protected
    FClass: TClass;

    function Table: TTableMapping;
    function Columns: TColumnMappingList;
    function PrimaryKey: TPrimaryKeyColumnsMapping;
  public
    constructor Create(AClass: TClass); virtual;
  end;

  EGBDAOEntityNotFound = class(EGBConnectionDAO)
  public
    constructor Create(AClass: TClass); overload; override;
    constructor Create(AMessage: String); reintroduce; overload;
  end;

  EGBDAOEntityAlreadyExists = class(EGBConnectionDAO)
    private
      FModel: TObject;
      Fentity: TGBModelEntity;

      function GetParamValue(AInstance: TObject; AProperty: TRttiProperty): String;
    public
      property entity: TGBModelEntity read Fentity write Fentity;
      constructor Create(const Model: TObject); reintroduce; overload;
      constructor Create(AMessage: String); reintroduce; overload;
      destructor Destroy; override;
  end;

implementation

{ EGBConnectionDAO }

function EGBConnectionDAO.Columns: TColumnMappingList;
begin
  if not Assigned(FColumns) then
    FColumns := TMappingExplorer.GetInstance.GetMappingColumn(FClass);
  Result := FColumns;
end;

constructor EGBConnectionDAO.Create(AClass: TClass);
begin
  FClass := AClass;
end;

function EGBConnectionDAO.PrimaryKey: TPrimaryKeyColumnsMapping;
begin
  if not Assigned(FPrimaryKey) then
    FPrimaryKey := TMappingExplorer.GetInstance.GetMappingPrimaryKeyColumns(FClass);
  Result := FPrimaryKey;
end;

function EGBConnectionDAO.Table: TTableMapping;
begin
  if not Assigned(FTable) then
    FTable := TMappingExplorer.GetInstance.GetMappingTable(FClass);
  Result := FTable;
end;

{ EDAOEntityNotFound }

constructor EGBDAOEntityNotFound.Create(AClass: TClass);
begin
  inherited;
  Self.Message := Format('Entidade %s n�o encontrada.', [Table.Name]);
end;

{ EGBDAOEntityAlreadyExists<T> }

constructor EGBDAOEntityAlreadyExists.Create(const Model: TObject);
var
  I: Integer;
begin
  inherited Create(Model.classType);
  Fentity := TGBModelEntity.Create(Table.Name);
  FModel := Model;

  for I := 0 to Pred(PrimaryKey.Columns.Count) do
  begin
    Fentity.AddReference(PrimaryKey.Columns[I].ColumnName, GetParamValue(Model, PrimaryKey.Columns[I].ColumnProperty));
    Self.Message := Self.Message + ' ' +
                    PrimaryKey.Columns[I].ColumnName + ' = ' +
                    GetParamValue(Model, PrimaryKey.Columns[I].ColumnProperty);
  end;
end;

constructor EGBDAOEntityAlreadyExists.Create(AMessage: String);
begin
  Self.Message := AMessage;
end;

destructor EGBDAOEntityAlreadyExists.Destroy;
begin
  FModel.Free;
  Fentity.Free;
  inherited;
end;

function EGBDAOEntityAlreadyExists.GetParamValue(AInstance: TObject; AProperty: TRttiProperty): String;
begin
  Result := AProperty.GetNullableValue(AInstance).AsVariant;
end;

{ TGBModelIdentity }

procedure TGBModelEntity.AddReference(Key, Value: String);
var
  LModelKey: TGBModelKey;
begin
  LModelKey := TGBModelKey.Create;
  LModelKey.Key := Key;
  LModelKey.Value := Value;

  Freference.Add(LModelKey);
end;

constructor TGBModelEntity.Create;
begin
  Freference := TObjectList<TGBModelKey>.Create;
end;

constructor TGBModelEntity.Create(AEntity: String);
begin
  Create;
  FEntity := AEntity;
end;

destructor TGBModelEntity.Destroy;
begin
  Freference.Free;
  inherited;
end;

constructor EGBDAOEntityNotFound.Create(AMessage: String);
begin
  Self.Message := AMessage;
end;

end.
