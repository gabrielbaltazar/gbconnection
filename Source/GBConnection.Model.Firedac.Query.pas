unit GBConnection.Model.Firedac.Query;

interface

uses
  GBConnection.Model.Interfaces,
  Data.DB,
  System.Classes,
  System.SysUtils,
  FireDAC.Stan.Param,
  FireDAC.Comp.Client;

type TGBConnectionFiredacQuery = class(TInterfacedObject, IGBQuery)

  private
    [Weak]
    FConnection: IGBConnection;
    FDQuery: TFDQuery;

    procedure ShowMonitor(SQL: String);
  public
    function SQL(Value: String): IGBQuery; overload;
    function SQL(Value: string; const Args: array of const): IGBQuery; overload;

    function ParamAsInteger(Name: String; Value: Integer): IGBQuery;
    function ParamAsCurrency(Name: String; Value: Currency): IGBQuery;
    function ParamAsFloat(Name: String; Value: Double): IGBQuery;
    function ParamAsString(Name: String; Value: String): IGBQuery;
    function ParamAsDateTime(Name: String; Value: TDateTime): IGBQuery;
    function ParamAsDate(Name: String; Value: TDateTime): IGBQuery;
    function ParamAsTime(Name: String; Value: TDateTime): IGBQuery;
    function ParamAsBoolean(Name: String; Value: Boolean): IGBQuery;

    function Open: TDataSet;
    function ExecSQL: IGBQuery;
    function ExecSQLAndCommit: IGBQuery;

    constructor Create(AConnection: IGBConnection);
    class function New(AConnection: IGBConnection): IGBQuery;
    destructor Destroy; override;
end;

implementation

{ TGBConnectionFiredacQuery }

constructor TGBConnectionFiredacQuery.Create(AConnection: IGBConnection);
begin
  FConnection := AConnection;
  FDQuery := TFDQuery.Create(nil);
  FDQuery.Connection := TFDConnection( FConnection.Connection );
end;

function TGBConnectionFiredacQuery.SQL(Value: String): IGBQuery;
begin
  Result := Self;
  FDQuery.SQL.Add(Value);
end;

destructor TGBConnectionFiredacQuery.Destroy;
begin
  FDQuery.Free;
  inherited;
end;

function TGBConnectionFiredacQuery.ExecSQL: IGBQuery;
begin
  Result := Self;
  try
    ShowMonitor(FDQuery.SQL.Text);
    FDQuery.ExecSQL;
  finally
    FDQuery.SQL.Clear;
  end;
end;

function TGBConnectionFiredacQuery.ExecSQLAndCommit: IGBQuery;
begin
  Result := Self;
  try
    FConnection.StartTransaction;
    try
      ShowMonitor(FDQuery.SQL.Text);
      FDQuery.ExecSQL;
      FConnection.Commit;
    except
      FConnection.Rollback;
      raise;
    end;
  finally
    FDQuery.SQL.Clear;
  end;
end;

class function TGBConnectionFiredacQuery.New(AConnection: IGBConnection): IGBQuery;
begin
  Result := Self.create(AConnection);
end;

function TGBConnectionFiredacQuery.Open: TDataSet;
var
  LQuery : TFDQuery;
  I: Integer;
begin
  try
    LQuery := TFDQuery.Create(nil);
    try
      LQuery.Connection := TFDConnection(FConnection.Component);
      LQuery.SQL.Text := FDQuery.SQL.Text;
      for I := 0 to Pred(FDQuery.ParamCount) do
        LQuery.ParamByName(FDQuery.Params[I].Name).Value := FDQuery.Params[I].Value;
      ShowMonitor(FDQuery.SQL.Text);
      LQuery.Open;

      Result := LQuery;
    except
      LQuery.Free;
      raise;
    end;
  finally
    FDQuery.SQL.Clear;
  end;
end;

function TGBConnectionFiredacQuery.ParamAsBoolean(Name: String; Value: Boolean): IGBQuery;
begin
  Result := Self;
  FDQuery.ParamByName(Name).AsBoolean := Value;
end;

function TGBConnectionFiredacQuery.ParamAsCurrency(Name: String; Value: Currency): IGBQuery;
begin
  Result := Self;
  FDQuery.ParamByName(Name).AsCurrency := Value;
end;

function TGBConnectionFiredacQuery.ParamAsDate(Name: String; Value: TDateTime): IGBQuery;
begin
  Result := Self;
  FDQuery.ParamByName(Name).AsDate := Value;
end;

function TGBConnectionFiredacQuery.ParamAsDateTime(Name: String; Value: TDateTime): IGBQuery;
begin
  Result := Self;
  FDQuery.ParamByName(Name).AsDateTime := Value;
end;

function TGBConnectionFiredacQuery.ParamAsFloat(Name: String; Value: Double): IGBQuery;
begin
  Result := Self;
  FDQuery.ParamByName(Name).AsFloat := Value;
end;

function TGBConnectionFiredacQuery.ParamAsInteger(Name: String; Value: Integer): IGBQuery;
begin
  Result := Self;
  FDQuery.ParamByName(Name).AsInteger := Value;
end;

function TGBConnectionFiredacQuery.ParamAsString(Name, Value: String): IGBQuery;
begin
  Result := Self;
  FDQuery.ParamByName(Name).AsString := Value;
end;

function TGBConnectionFiredacQuery.ParamAsTime(Name: String; Value: TDateTime): IGBQuery;
begin
  Result := Self;
  FDQuery.ParamByName(Name).AsTime := Value;
end;

procedure TGBConnectionFiredacQuery.ShowMonitor(SQL: String);
begin
  if Assigned(FConnection.Monitor) then
    FConnection.Monitor.Command(SQL, nil);
end;

function TGBConnectionFiredacQuery.SQL(Value: string; const Args: array of const): IGBQuery;
begin
  Result := Self;
  SQL(Format(Value, Args));
end;

end.
