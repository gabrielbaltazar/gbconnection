unit GBConnection.Data.Model.Command.Table.Abstract;

interface

uses
  GBConnection.Data.Model.Command.Interfaces,
  System.SysUtils;

type TGBConnectionDataModelCommandTableAbstract = class(TInterfacedObject, IGBConnectionDataModelCommandTable)

  protected
    FSchema: string;

  public
    function TableColumns(TableName: string): string; virtual; abstract;

    constructor create(Schema: String);
    class function New(Schema: String): IGBConnectionDataModelCommandTable;
end;

implementation

{ TGBConnectionDataModelCommandTableAbstract }

constructor TGBConnectionDataModelCommandTableAbstract.create(Schema: String);
begin
  FSchema := Schema
end;

class function TGBConnectionDataModelCommandTableAbstract.New(Schema: String): IGBConnectionDataModelCommandTable;
begin
  result := Self.create(Schema);
end;

end.
