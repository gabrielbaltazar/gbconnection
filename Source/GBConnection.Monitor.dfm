object GBConnectionMonitor: TGBConnectionMonitor
  Left = 0
  Top = 0
  BorderStyle = bsNone
  ClientHeight = 404
  ClientWidth = 669
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Segoe UI'
  Font.Style = []
  OldCreateOrder = False
  Position = poScreenCenter
  PixelsPerInch = 96
  TextHeight = 13
  object mmoMonitor: TMemo
    Left = 0
    Top = 41
    Width = 669
    Height = 363
    Align = alClient
    BorderStyle = bsNone
    Color = 4732429
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWhite
    Font.Height = -13
    Font.Name = 'Sergoe UI'
    Font.Style = []
    ParentFont = False
    PopupMenu = pmLog
    ScrollBars = ssVertical
    TabOrder = 0
  end
  object pnlTop: TPanel
    Left = 0
    Top = 0
    Width = 669
    Height = 41
    Align = alTop
    Alignment = taLeftJustify
    BevelOuter = bvNone
    Color = 4140033
    ParentBackground = False
    TabOrder = 1
    OnMouseDown = pnlTopMouseDown
    object lbl1: TLabel
      AlignWithMargins = True
      Left = 20
      Top = 3
      Width = 74
      Height = 25
      Margins.Left = 20
      Align = alLeft
      Caption = 'Monitor'
      Color = clWhite
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -19
      Font.Name = 'Segoe UI'
      Font.Style = [fsBold]
      ParentColor = False
      ParentFont = False
      Layout = tlCenter
    end
    object lblClose: TLabel
      AlignWithMargins = True
      Left = 647
      Top = 3
      Width = 12
      Height = 25
      Cursor = crHandPoint
      Hint = 'Fechar'
      Margins.Right = 10
      Align = alRight
      Alignment = taCenter
      Caption = 'X'
      DragCursor = crHandPoint
      Font.Charset = DEFAULT_CHARSET
      Font.Color = clWhite
      Font.Height = -19
      Font.Name = 'Segoe UI'
      Font.Style = [fsBold]
      ParentFont = False
      ParentShowHint = False
      ShowHint = True
      Layout = tlCenter
      Visible = False
    end
  end
  object pmLog: TPopupMenu
    Left = 168
    Top = 280
    object Limpar1: TMenuItem
      Caption = 'Limpar'
      OnClick = Limpar1Click
    end
  end
end
