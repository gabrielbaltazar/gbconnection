unit GBConnection.Monitor.LogFile;

interface

uses
  GBConnection.Model.Interfaces,
  ormbr.factory.interfaces,
  Data.DB,
  Winapi.ShellAPI,
  System.Variants,
  System.SysUtils,
  System.Classes;

type TGBConnectionMonitorLogFile = class(TInterfacedObject, IGBMonitorConnection, ICommandMonitor)

  private
    FLogFile: TStrings;
    FFileName: string;

  public
    procedure SendMessage(AMessage: String);
    procedure Command(const ASQL: string; AParams: TParams);
    procedure Show;

    constructor create(AFileName: String);
    destructor  Destroy; override;
end;

implementation

uses
  Winapi.Windows,
  System.TypInfo;

{ TGBConnectionMonitorLogFile }

procedure TGBConnectionMonitorLogFile.Command(const ASQL: string; AParams: TParams);
var
  iFor       : Integer;
  AsValue    : string;
  textParams : string;
begin
  //SendMessage('');
  SendMessage(ASQL);
  if AParams <> nil then
  begin
    textParams := EmptyStr;
    for iFor := 0 to AParams.Count -1 do
    begin
      if AParams.Items[iFor].Value = System.Variants.Null then
        AsValue := 'NULL'
      else
      if AParams.Items[iFor].DataType = ftDateTime then
        AsValue := '"' + DateTimeToStr(AParams.Items[iFor].Value) + '"'
      else
      if AParams.Items[iFor].DataType = ftDate then
        AsValue := '"' + DateToStr(AParams.Items[iFor].Value) + '"'
      else
        AsValue := '"' + VarToStr(AParams.Items[iFor].Value) + '"';

      textParams := textParams + AParams.Items[iFor].Name + ' = ' + AsValue + ' (' +
                    GetEnumName(TypeInfo(TFieldType), Ord(AParams.Items[iFor].DataType)) + ')' + sLineBreak;
    end;
    SendMessage(textParams);
  end;
end;

constructor TGBConnectionMonitorLogFile.create(AFileName: String);
begin
  FLogFile := TStringList.Create;
  FFileName:= AFileName;
  FLogFile.SaveToFile(AFileName);
end;

destructor TGBConnectionMonitorLogFile.Destroy;
begin
  FLogFile.Free;
  inherited;
end;

procedure TGBConnectionMonitorLogFile.SendMessage(AMessage: String);
begin
  if not AMessage.Trim.IsEmpty then
    FLogFile.Add('-- ' + FormatDateTime('dd/MM/yyyy hh:mm:ss', now));
  FLogFile.Add(AMessage);
  FLogFile.SaveToFile(FFileName);
end;

procedure TGBConnectionMonitorLogFile.Show;
begin
  ShellExecute(HInstance, 'open', PChar(FFileName), '', '', SW_SHOWNORMAL);
end;

end.
