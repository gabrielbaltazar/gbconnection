unit GBConnection.View.Lista;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Data.DB,
  Vcl.DBGrids,
  GBConnection.Model.Interfaces,
  GBConnection.Dao.Interfaces,
  GBConnection.Dao.ORMBr.DataSet,
  GBConnection.Dao.ORMBr,
  GBConnection.Controller,
  GBConnection.View.Cadastro,
  GBConnection.View.Cadastro.Crud;

//type TGBViewFormCadastro = class of TFGBConnectionViewCadastro;

type
  TFGBConnectionViewLista = class(TForm)
    DataSource: TDataSource;
  protected
    FCrud     : IInterface;
    FOnNew    : TNotifyEvent;
    FOnDelete : TNotifyEvent;
    FOnView   : TNotifyEvent;
    { Private declarations }
  public
    function GridComponent: TDBGrid; virtual; abstract;

    procedure SetOnNew    (Value: TNotifyEvent);
    procedure SetOnView   (Value: TNotifyEvent);
    procedure SetOnDelete (Value: TNotifyEvent);

    destructor Destroy; override;
    { Public declarations }
  end;

implementation

{$R *.dfm}

{ TFGBConnectionViewLista }

destructor TFGBConnectionViewLista.Destroy;
begin
//  FCrud.Free;
  inherited;
end;

procedure TFGBConnectionViewLista.SetOnDelete(Value: TNotifyEvent);
begin
  FOnDelete := Value;
end;

procedure TFGBConnectionViewLista.SetOnNew(Value: TNotifyEvent);
begin
  FOnNew := Value;
end;

procedure TFGBConnectionViewLista.SetOnView(Value: TNotifyEvent);
begin
  FOnView := Value;
end;

end.
