unit GBConnection.Data.Dao.Factory;

interface

uses
  GBConnection.Model.Interfaces,
  GBConnection.Data.Dao.Interfaces,
  GBConnection.Data.Dao.Table,
  GBConnection.Data.Dao.TableRow;

type TGBConnectionDataDaoFactory = class(TInterfacedObject, IGBConnectionDataDaoFactory)

  private
    [Weak]
    FConnection: IGBConnection;

  public
    function createDaoTable   : IGBConnectionDataDaoTable;
    function createDaoTableRow: IGBConnectionDataDaoTableRow;

    constructor create(Connection: IGBConnection);
    class function New(Connection: IGBConnection): IGBConnectionDataDaoFactory;
end;

implementation

{ TGBConnectionDataDaoFactory }

constructor TGBConnectionDataDaoFactory.create(Connection: IGBConnection);
begin
  FConnection := Connection;
end;

function TGBConnectionDataDaoFactory.createDaoTable: IGBConnectionDataDaoTable;
begin
  result := TGBConnectionDataDaoTable.New(FConnection);
end;

function TGBConnectionDataDaoFactory.createDaoTableRow: IGBConnectionDataDaoTableRow;
begin
  result := TGBConnectionDataDaoTableRow.New(FConnection);
end;

class function TGBConnectionDataDaoFactory.New(Connection: IGBConnection): IGBConnectionDataDaoFactory;
begin
  Result := Self.create(Connection);
end;

end.
