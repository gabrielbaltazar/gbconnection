unit GBConnection.Zeos.Connection;

interface

uses
  GBConnection.Model.Interfaces,
  GBConnection.Model.Params,
  ZCompatibility,
  ZConnection,
  ZDatasetUtils,
  Data.DB,
  System.SysUtils,
  System.Classes;

type TGBConnectionZeos = class(TInterfacedObject, IGBConnection)

  protected
//    [weak]
    FMonitor: IGBMonitorConnection;
    FConnection: TZConnection;
    FParams: IGBConnectionParams<IGBConnection>;

    procedure Setup; virtual;
    function  GetProtocol: string;
  public
    function Connection: TCustomConnection;
    function Component: TComponent;

    function Connect: IGBConnection;
    function Disconnect: IGBConnection;
    function StartTransaction: IGBConnection;
    function Commit: IGBConnection;
    function Rollback: IGBConnection;

    function InTransaction: Boolean;
    function Params: IGBConnectionParams<IGBConnection>;

    function Monitor(Value: IGBMonitorConnection): IGBConnection; overload;
    function Monitor: IGBMonitorConnection; overload;

    constructor Create;
    class function New: IGBConnection;
    destructor Destroy; override;
end;

implementation

{ TGBConnectionZeos }

function TGBConnectionZeos.Commit: IGBConnection;
begin
  Result := Self;
  FConnection.Commit;
end;

function TGBConnectionZeos.Component: TComponent;
begin
  Result := FConnection;
end;

function TGBConnectionZeos.Connect: IGBConnection;
begin
  Result := Self;
  if not FConnection.Connected then
  begin
    Setup;
    FConnection.Connected := True;
  end;
end;

function TGBConnectionZeos.Connection: TCustomConnection;
begin
  raise ENotSupportedException.Create('Fun��o n�o suportada para o Driver Zeos.');
end;

constructor TGBConnectionZeos.Create;
begin
  FParams := TGBConnectionParams<IGBConnection>.New(Self);
  FConnection := TZConnection.Create(nil);
  FConnection.Name := 'teste';
end;

destructor TGBConnectionZeos.Destroy;
begin
  FConnection.Free;
  inherited;
end;

function TGBConnectionZeos.Disconnect: IGBConnection;
begin
  Result := Self;
  FConnection.Connected := False;
end;

function TGBConnectionZeos.GetProtocol: string;
begin
  case FParams.Driver of
    gbMySql: Result := 'mysql';
    gbOracle: Result := 'oracle';
    gbSQLite: Result := 'sqlite';
    gbMSSQL: Result := 'mssql';
    gbPostgres: Result := 'postgresql';
  else
    raise ENotImplemented.Create('Driver n�o implementado.');
  end;
end;

function TGBConnectionZeos.InTransaction: Boolean;
begin
  Result := FConnection.InTransaction;
end;

function TGBConnectionZeos.Monitor: IGBMonitorConnection;
begin
  Result := FMonitor;
end;

function TGBConnectionZeos.Monitor(Value: IGBMonitorConnection): IGBConnection;
begin
  FMonitor := Value;
  Result := Self;
end;

class function TGBConnectionZeos.New: IGBConnection;
begin
  Result := Self.Create;
end;

function TGBConnectionZeos.Params: IGBConnectionParams<IGBConnection>;
begin
  Result := FParams;
end;

function TGBConnectionZeos.Rollback: IGBConnection;
begin
  Result := Self;
  FConnection.Rollback;
end;

procedure TGBConnectionZeos.Setup;
begin
  if not Assigned(FParams) then
    raise EArgumentNilException.Create('Par�metros de Conex�o n�o informados.');

  FConnection.AutoCommit := False;
  FConnection.AutoEncodeStrings := True;
  FConnection.Protocol := GetProtocol;
  FConnection.ControlsCodePage := TZControlsCodePage.cCP_UTF16;
  FConnection.HostName := FParams.Server;
  FConnection.Port := FParams.Port;
  FConnection.Database := FParams.Database;
  FConnection.User := FParams.UserName;
  FConnection.Password := FParams.Password;

  if FParams.Driver = gbMSSQL then
  begin
    FConnection.LibraryLocation := ExtractFilePath(GetModuleName(HInstance)) + 'ntwdblib.dll';
  end;
//    FConnection.Protocol := 'ado';
//    FConnection.HostName          := EmptyStr;
//    FConnection.Database          := EmptyStr;
//    FConnection.User              := EmptyStr;
//    FConnection.Password          := EmptyStr;
//    FConnection.Database :=
//    Format('Provider=SQLNCLI11.1;UID=%s;PWD=%s;Server=%s;Database=%s;MARS_Connection=yes',
//                                                [FParams.UserName,
//                                                 FParams.Password,
//                                                 FParams.Server,
//                                                 FParams.Database]);
//
//    Format('Provider=SQLOLEDB.1;Password=%s;Persist Security Info=False;User ID=%s;' +
//           'Initial Catalog=%s;Data Source=%s', [FParams.Password,
//                                                 FParams.UserName,
//                                                 FParams.Database,
//                                                 FParams.Server]);
//  end;
end;

function TGBConnectionZeos.StartTransaction: IGBConnection;
begin
  Result := Self;
  FConnection.StartTransaction;
end;

end.
