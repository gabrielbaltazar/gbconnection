unit GBConnection.Model.FiredacConnection;

interface

uses
  GBConnection.Model.Interfaces,
  GBConnection.Model.Params,
  GBConnection.Model.Firedac.DriverLink,
  Data.DB,
  FireDAC.DApt,
  {$IFDEF MSWINDOWS}
  FireDAC.Comp.UI,
  FireDAC.VCLUI.Wait,
  FireDAC.UI.Intf,
  {$ENDIF}
  {$IFDEF LINUX}
  FireDAC.ConsoleUI.Wait,
  {$ENDIF}
  FireDAC.Stan.Intf,
  FireDAC.Stan.Option,
  FireDAC.Stan.Error,
  FireDAC.Phys.Intf,
  FireDAC.Stan.Def,
  FireDAC.Stan.Pool,
  FireDAC.Stan.Async,
  FireDAC.Phys,
  FireDAC.Comp.Client,
  System.Classes,
  System.SysUtils;

type TGBConnectionFiredac = class(TInterfacedObject, IGBConnection)
  private
    FConnection: TFDConnection;
    FDriver: TFDPhysDriverLink;
    FParams: IGBConnectionParams<IGBConnection>;
    FMonitor: IGBMonitorConnection;

    procedure Setup;
    procedure CreateDriver;

    function GetDriverId: String;
  public
    function Connection: TCustomConnection;
    function Component: TComponent;
    function Params: IGBConnectionParams<IGBConnection>;

    function Connect: IGBConnection;
    function Disconnect: IGBConnection;
    function StartTransaction: IGBConnection;
    function Commit: IGBConnection;
    function Rollback: IGBConnection;
    function InTransaction: Boolean;

    function Monitor(Value: IGBMonitorConnection): IGBConnection; overload;
    function Monitor: IGBMonitorConnection; overload;

    constructor Create;
    destructor Destroy; override;
    class function New: IGBConnection;
end;

implementation

{ TGBConnectionFiredac }

function TGBConnectionFiredac.Commit: IGBConnection;
begin
  Result := Self;
  FConnection.Commit;
end;

function TGBConnectionFiredac.Component: TComponent;
begin
  Result := FConnection;
end;

function TGBConnectionFiredac.Connect: IGBConnection;
begin
  Result := Self;
  if not FConnection.Connected then
  begin
    Setup;
    FConnection.Connected := True;
  end;
end;

function TGBConnectionFiredac.Connection: TCustomConnection;
begin
  Result := FConnection;
end;

constructor TGBConnectionFiredac.Create;
begin
  FConnection := TFDConnection.Create(nil);
  FParams := TGBConnectionParams<IGBConnection>.New(Self);
end;

procedure TGBConnectionFiredac.CreateDriver;
begin
  FreeAndNil(FDriver);
  FDriver := TGBConnectionModelFiredacDriver.GetDriver(FParams);

  if not Assigned(FDriver) then
    raise Exception.Create('N�o foi poss�vel criar o Driver de Conex�o.');
end;

destructor TGBConnectionFiredac.Destroy;
begin
  FConnection.Free;
  FDriver.Free;
  inherited;
end;

function TGBConnectionFiredac.Disconnect: IGBConnection;
begin
  Result := Self;
  FConnection.Connected := False;
end;

function TGBConnectionFiredac.GetDriverId: String;
begin
  case FParams.Driver of
    gbOracle: Result := 'Ora';
    gbMySql: Result := 'MySQL';
    gbSQLite: Result := 'SQLite';
    gbMSSQL: Result := 'MSSQL';
    gbPostgres: Result := 'PG';
  else
    raise Exception.Create('Driver Firedac n�o implementado.');
  end;
end;

function TGBConnectionFiredac.InTransaction: Boolean;
begin
  Result := FConnection.InTransaction;
end;

function TGBConnectionFiredac.Monitor: IGBMonitorConnection;
begin
  Result := FMonitor;
end;

function TGBConnectionFiredac.Monitor(Value: IGBMonitorConnection): IGBConnection;
begin
  Result   := Self;
  FMonitor := Value;
end;

class function TGBConnectionFiredac.New: IGBConnection;
begin
  Result := Self.Create;
end;

function TGBConnectionFiredac.Params: IGBConnectionParams<IGBConnection>;
begin
  Result := FParams;
end;

function TGBConnectionFiredac.Rollback: IGBConnection;
begin
  result := Self;
  FConnection.Rollback;
end;

procedure TGBConnectionFiredac.Setup;
begin
  if not Assigned(FParams) then
    raise EArgumentNilException.Create('Par�metros de Conex�o n�o informados.');

  FConnection.TxOptions.AutoCommit := FParams.AutoCommit;
  FConnection.Params.Database := FParams.Database;
  FConnection.Params.UserName := FParams.UserName;
  FConnection.Params.Password := FParams.Password;
  FConnection.Params.DriverID := GetDriverId;
  FConnection.FetchOptions.Mode := fmAll;

  if FParams.Port > 0 then
    FConnection.Params.Add('Port=' + FParams.Port.ToString);

  if not FParams.Server.IsEmpty then
    FConnection.Params.Add('Server=' + FParams.Server);

  CreateDriver;
end;

function TGBConnectionFiredac.StartTransaction: IGBConnection;
begin
  Result := Self;
  FConnection.StartTransaction;
end;

end.
