unit GBConnection.Model.Factory;

interface

uses
  {$IFDEF ZEOS}
    GBConnection.Zeos.Connection, GBConnection.Zeos.Query,
  {$ENDIF}
  {$IFDEF FIREDAC}
    GBConnection.Model.FiredacConnection, GBConnection.Model.Firedac.Query,
  {$ENDIF}
  GBConnection.Model.Interfaces,
  GBConnection.Model.Params;

type TGBFactoryConnection = class(TInterfacedObject, IGBFactoryConnection)

  public
    function CreateConnection: IGBConnection;
    function CreateQuery(AConnection: IGBConnection): IGBQuery;

    class function New: IGBFactoryConnection;
end;

implementation

{ TGBFactoryConnection }

function TGBFactoryConnection.CreateConnection: IGBConnection;
begin
  {$IFDEF ZEOS}
    Exit( TGBConnectionZeos.New );
  {$ENDIF}

  {$IFDEF FIREDAC}
    Exit( TGBConnectionFiredac.New );
  {$ENDIF}
end;

function TGBFactoryConnection.CreateQuery(AConnection: IGBConnection): IGBQuery;
begin
  {$IFDEF ZEOS}
    Exit( TGBConnectionQueryZeos.New(AConnection));
  {$ENDIF}

  {$IFDEF FIREDAC}
    Exit( TGBConnectionFiredacQuery.New(AConnection) );
  {$ENDIF}
end;

class function TGBFactoryConnection.New: IGBFactoryConnection;
begin
  Result := Self.Create;
end;

end.
