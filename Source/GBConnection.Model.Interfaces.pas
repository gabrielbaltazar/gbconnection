unit GBConnection.Model.Interfaces;

interface

uses
  Data.DB,
  System.Classes,
  System.SysUtils;

type
  TGBDriverConnection = (gbOracle, gbMSSQL, gbPostgres, gbMySql, gbSQLite, gbFirebird);

  IGBConnection = interface;
  IGBQuery = interface;
  IGBMonitorConnection = interface;

  IGBConnectionParams<I: IInterface> = interface
    ['{FEE9970A-A0DB-4F17-B02B-86B5AE2409C6}']
    function Database(Value: string): IGBConnectionParams<I>; overload;
    function UserName(Value: string): IGBConnectionParams<I>; overload;
    function Password(Value: string): IGBConnectionParams<I>; overload;
    function Server(Value: string): IGBConnectionParams<I>; overload;
    function Schema(Value: String): IGBConnectionParams<I>; overload;
    function Port(Value: Integer): IGBConnectionParams<I>; overload;
    function AutoCommit(Value: Boolean): IGBConnectionParams<I>; overload;
    function Driver(Value: TGBDriverConnection): IGBConnectionParams<I>; overload;

    function Database: string; overload;
    function UserName: string; overload;
    function Password: string; overload;
    function Server: string; overload;
    function Schema: string; overload;
    function Port: Integer; overload;
    function AutoCommit: Boolean; overload;
    function Driver: TGBDriverConnection; overload;

    function &Begin: IGBConnectionParams<I>;
    function &End: I;
  end;

  IGBConnection = interface
    ['{ACB64F86-00E2-4D12-A88F-562772EB21E3}']
    function Connection: TCustomConnection;
    function Component: TComponent;
    function Params: IGBConnectionParams<IGBConnection>;

    function Connect: IGBConnection;
    function Disconnect: IGBConnection;
    function StartTransaction: IGBConnection;
    function Commit: IGBConnection;
    function Rollback: IGBConnection;
    function InTransaction: Boolean;

    function Monitor(Value: IGBMonitorConnection): IGBConnection; overload;
    function Monitor: IGBMonitorConnection; overload;
  end;

  IGBQuery = interface
    ['{A27DB16D-CB06-43A7-B2D5-53349E893A5B}']
    function SQL(Value: String): IGBQuery; overload;
    function SQL(Value: string; const Args: array of const): IGBQuery; overload;

    function ParamAsInteger(Name: String; Value: Integer): IGBQuery;
    function ParamAsCurrency(Name: String; Value: Currency): IGBQuery;
    function ParamAsFloat(Name: String; Value: Double): IGBQuery;
    function ParamAsString(Name: String; Value: String): IGBQuery;
    function ParamAsDateTime(Name: String; Value: TDateTime): IGBQuery;
    function ParamAsDate(Name: String; Value: TDateTime): IGBQuery;
    function ParamAsTime(Name: String; Value: TDateTime): IGBQuery;
    function ParamAsBoolean(Name: String; Value: Boolean): IGBQuery;

    function Open: TDataSet;
    function ExecSQL: IGBQuery;
    function ExecSQLAndCommit: IGBQuery;
  end;

  IGBMonitorConnection = interface
    ['{5C3E1DBD-C69B-4F2B-B585-A97C6851605F}']
    procedure Command(const ASQL: string; AParams: TParams);
    procedure Show;
  end;

  IGBFactoryConnection = interface
    ['{8580A941-72C5-4F5C-8E2A-9C4AAEA46CE4}']
    function CreateConnection: IGBConnection;
    function CreateQuery(AConnection: IGBConnection): IGBQuery;
  end;

{$IFDEF MSWINDOWS}
procedure UseDefaultMonitor(Connection: IGBConnection);
procedure UseMonitorLogFile(Connection: IGBConnection); overload;
procedure UseMonitorLogFile(Connection: IGBConnection; AFileName: String); overload;
{$ENDIF}

function FactoryConnection: IGBFactoryConnection;

implementation

uses
{$IFDEF MSWINDOWS}
  GBConnection.Monitor,
  GBConnection.Monitor.LogFile,
{$ENDIF}
  GBConnection.Model.Factory;

function FactoryConnection: IGBFactoryConnection;
begin
  result := TGBFactoryConnection.New;
end;

{$IFDEF MSWINDOWS}
procedure UseDefaultMonitor(Connection: IGBConnection);
var
  monitor: IGBMonitorConnection;
begin
  monitor := TGBConnectionMonitorImpl.create;
  monitor.Show;
  Connection.Monitor(monitor);
end;

procedure UseMonitorLogFile(Connection: IGBConnection);
var
  fileName: string;
begin
  fileName := GetModuleName(HInstance);
  fileName := ChangeFileExt(fileName, '.trace');

  UseMonitorLogFile(Connection, fileName);
end;

procedure UseMonitorLogFile(Connection: IGBConnection; AFileName: String);
var
  monitor : IGBMonitorConnection;
begin
  monitor := TGBConnectionMonitorLogFile.create(AFileName);
  monitor.Show;
  Connection.Monitor(monitor);
end;
{$ENDIF}

end.
