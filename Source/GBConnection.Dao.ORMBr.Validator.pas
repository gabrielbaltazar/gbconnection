unit GBConnection.Dao.ORMBr.Validator;

interface

uses
  GBConnection.Dao.Interfaces,
  GBConnection.Criteria.Interfaces,
  ormbr.bind,
  ormbr.factory.interfaces,
  ormbr.objects.manager,
  ormbr.session.objectset,
  ormbr.mapping.rttiutils,
  ormbr.mapping.attributes,
  ormbr.mapping.explorer,
  ormbr.mapping.classes,
  ormbr.command.factory,
  ormbr.rtti.helper,
  ormbr.objects.helper,
  GBConnection.Dao.ORMBr.Exceptions,
  System.SysUtils;

type TGBModelDAOValidator<T: class, constructor> = class(TInterfacedObject, IGBModelDaoValidator<T>)

  private
    FConnection: IDBConnection;

    FSession: TSessionObjectSet<T>;
    FManager: TObjectManager<T>;
    FDML: TDMLCommandFactory;

    function Session: TSessionObjectSet<T>; overload;
    function Manager: TObjectManager<T>; overload;

    procedure ValidateAssertExist(AObject: TObject; Column: TColumnMapping; bInsert: boolean);
    procedure ValidateAssertNotExist(AObject: TObject; Column: TColumnMapping; bInsert: boolean);

    function GetResultSet(TableName: string; Columns, Values: TArray<String>): IDBResultSet;
  public
    function Exists(AWhere: String): Boolean; overload;
    function Exists(AWhere: String; const Args: array of const): Boolean; overload;
    function Exists(ACriteria: IGBConnectionCriteria): Boolean; overload;

    function AssertExists(AWhere: String): IGBModelDaoValidator<T>; overload;
    function AssertExists(AWhere: String; const Args: array of const): IGBModelDaoValidator<T>; overload;
    function AssertExists(ACriteria: IGBConnectionCriteria; Message: String): IGBModelDaoValidator<T>; overload;
    function AssertExists(ACriteria: IGBConnectionCriteria; Message: String; const Args: array of const): IGBModelDaoValidator<T>; overload;

    function AssertNotExists(AWhere: String): IGBModelDaoValidator<T>; overload;
    function AssertNotExists(AWhere: String; const Args: array of const): IGBModelDaoValidator<T>; overload;
    function AssertNotExists(ACriteria: IGBConnectionCriteria; Message: String): IGBModelDaoValidator<T>; overload;
    function AssertNotExists(ACriteria: IGBConnectionCriteria; Message: String; const Args: array of const): IGBModelDaoValidator<T>; overload;

    procedure RunValidate(Value: T; bInsert: Boolean = True);

    constructor Create(AConnection: IDBConnection);
    destructor Destroy; override;
    class function New(AConnection: IDBConnection): IGBModelDaoValidator<T>;
end;

implementation

{ TGBModelDAOValidator<T> }

function TGBModelDAOValidator<T>.AssertExists(AWhere: String): IGBModelDaoValidator<T>;
begin
  Result := Self;
  AssertExists(AWhere, []);
end;

function TGBModelDAOValidator<T>.AssertExists(ACriteria: IGBConnectionCriteria; Message: String; const Args: array of const): IGBModelDaoValidator<T>;
begin
  Result := self;
  AssertExists(ACriteria, Format(Message, Args));
end;

function TGBModelDAOValidator<T>.AssertExists(ACriteria: IGBConnectionCriteria; Message: String): IGBModelDaoValidator<T>;
begin
  try
    Result := self;
    AssertExists(ACriteria.AsString);
  except
    on e: EGBDAOEntityNotFound do
    begin
      e.Message := Message;
      raise;
    end;
  end;
end;

function TGBModelDAOValidator<T>.AssertNotExists(ACriteria: IGBConnectionCriteria; Message: String; const Args: array of const): IGBModelDaoValidator<T>;
begin
  Result := Self;
  AssertNotExists(ACriteria, Format(Message, Args));
end;

function TGBModelDAOValidator<T>.AssertNotExists(ACriteria: IGBConnectionCriteria; Message: String): IGBModelDaoValidator<T>;
begin
  try
    Result := Self;
    AssertNotExists(ACriteria.AsString);
  except
    on e: EGBDAOEntityAlreadyExists do
    begin
      e.Message := Message;
      raise;
    end;
  end;
end;

function TGBModelDAOValidator<T>.AssertExists(AWhere: String; const Args: array of const): IGBModelDaoValidator<T>;
begin
  Result := Self;
  if not Exists(AWhere, Args) then
    raise EGBDAOEntityNotFound.create(T);
end;

function TGBModelDAOValidator<T>.AssertNotExists(AWhere: String; const Args: array of const): IGBModelDaoValidator<T>;
var
  LResultSet: IDBResultSet;
  LSQL: String;
  LModel: T;
begin
  LModel := nil;
  Result := Self;
  LSQL := Format(AWhere, Args);
  LResultSet := Manager.SelectInternal(Manager.SelectInternalWhere(LSQL, ''));
  try
    if LResultSet.RecordCount > 0 then
    begin
      LModel := T.create;
      try
        TBind.Instance.SetFieldToProperty(LResultSet, TObject(LModel));
      except
        LModel.Free;
        raise;
      end;
    end;
  finally
    LResultSet.Close;
  end;

  if Assigned(LModel) then
    raise EGBDAOEntityAlreadyExists.Create(LModel);
end;

function TGBModelDAOValidator<T>.AssertNotExists(AWhere: String): IGBModelDaoValidator<T>;
begin
  Result := AssertNotExists(AWhere, []);
end;

constructor TGBModelDAOValidator<T>.Create(AConnection: IDBConnection);
begin
  FConnection := AConnection;
end;

destructor TGBModelDAOValidator<T>.Destroy;
begin
  FManager.Free;
  FSession.Free;
  inherited;
end;

function TGBModelDAOValidator<T>.Exists(AWhere: String): Boolean;
begin
  Result := Exists(AWhere, []);
end;

function TGBModelDAOValidator<T>.Exists(AWhere: String; const Args: array of const): Boolean;
var
  LResultSet: IDBResultSet;
  LSQL: String;
begin
  Result := False;
  LSQL := Format(AWhere, Args);
  LResultSet := Manager.SelectInternal(Manager.SelectInternalWhere(LSQL, ''));
  try
    Result := LResultSet.RecordCount > 0;
  finally
    LResultSet.Close;
  end;
end;

class function TGBModelDAOValidator<T>.New(AConnection: IDBConnection): IGBModelDaoValidator<T>;
begin
  Result := Self.Create(AConnection);
end;

procedure TGBModelDAOValidator<T>.RunValidate(Value: T; bInsert: Boolean);
var
  LColumn: TColumnMapping;
  LColumns: TColumnMappingList;
  LAttribute: TCustomAttribute;
begin
  TRttiSingleton.GetInstance.RunValidade(Value);

  FDML := TDMLCommandFactory.Create(Value, FConnection, FConnection.GetDriverName);
  try
    LColumns := TMappingExplorer.GetInstance.GetMappingColumn(Value.ClassType);
    for LColumn in LColumns do
    begin
      LAttribute := LColumn.ColumnProperty.GetExistConstraint;
      if LAttribute <> nil then
        ValidateAssertExist(Value, LColumn, bInsert);

      LAttribute := LColumn.ColumnProperty.GetNotExistConstraint;
      if LAttribute <> nil then
        ValidateAssertNotExist(Value, LColumn, bInsert);
    end;
  finally
    FDML.Free;
  end;
end;

function TGBModelDAOValidator<T>.Session: TSessionObjectSet<T>;
begin
  if not Assigned(FSession) then
    FSession := TSessionObjectSet<T>.Create(FConnection);
  Result := FSession;
end;

procedure TGBModelDAOValidator<T>.ValidateAssertExist(AObject: TObject; Column: TColumnMapping; bInsert: boolean);
var
  LAttribute: Exist;
  I: Integer;
  LResultSet: IDBResultSet;
  LColumns: TArray<String>;
  LValues: TArray<String>;
begin
  LAttribute := Column.ColumnProperty.GetExistConstraint;

  if (bInsert) and (not LAttribute.Insert) then
    Exit;
  if (not bInsert) and (not LAttribute.Update) then
    Exit;
  if Column.ColumnProperty.IsEmptyValue(AObject) then
    Exit;

  SetLength(LColumns, 1);
  SetLength(LValues, 1);
  LColumns[0] := Column.ColumnName;
  LValues[0] := AObject.GetProperty(Column.ColumnName).GetVariantValue(AObject);
  if Length(LAttribute.ColumnsNameRef) > 0 then
    LColumns[0] := LAttribute.ColumnsNameRef[0];

  for I := 1 to Pred(Length(LAttribute.ColumnsNameRef)) do
  begin
    SetLength(LColumns, I + 1);
    SetLength(LValues, I + 1);

    LColumns[I] := LAttribute.ColumnsNameRef[I];
    LValues[I] := AObject.GetProperty(LAttribute.ColumnsName[I]).GetVariantValue(AObject);
  end;

  LResultSet := GetResultSet(LAttribute.TableName, LColumns, LValues);
  if LResultSet.RecordCount = 0 then
    raise EGBDAOEntityNotFound.Create(LAttribute.ValidateMessage(LValues));
end;

procedure TGBModelDAOValidator<T>.ValidateAssertNotExist(AObject: TObject; Column: TColumnMapping; bInsert: boolean);
var
  LAttribute: NotExist;
  I: Integer;
  LResultSet: IDBResultSet;
  LColumns: TArray<String>;
  LValues: TArray<String>;
begin
  LAttribute := Column.ColumnProperty.GetNotExistConstraint;
  if (bInsert) and (not LAttribute.Insert) then Exit;
  if (not bInsert) and (not LAttribute.Update) then Exit;

  if Column.ColumnProperty.IsEmptyValue(AObject) then
    Exit;

  SetLength(LColumns, 1);
  SetLength(LValues, 1);
  LColumns[0] := Column.ColumnName;
  LValues[0] := AObject.GetProperty(Column.ColumnName).GetVariantValue(AObject);
  if Length(LAttribute.ColumnsNameRef) > 0 then
    LColumns[0] := LAttribute.ColumnsNameRef[0];

  for I := 1 to Pred(Length(LAttribute.ColumnsNameRef)) do
  begin
    SetLength(LColumns, I + 1);
    SetLength(LValues, I + 1);

    LColumns[I] := LAttribute.ColumnsNameRef[I];
    LValues[I] := AObject.GetProperty(LAttribute.ColumnsName[I]).GetVariantValue(AObject);
  end;

  LResultSet := GetResultSet(LAttribute.TableName, LColumns, LValues);
  if LResultSet.RecordCount > 0 then
    raise EGBDAOEntityAlreadyExists.Create(LAttribute.ValidateMessage(LValues));
end;

function TGBModelDAOValidator<T>.Exists(ACriteria: IGBConnectionCriteria): Boolean;
begin
  Result := Exists(ACriteria.AsString);
end;

function TGBModelDAOValidator<T>.GetResultSet(TableName: string; Columns, Values: TArray<String>): IDBResultSet;
var
  I: Integer;
  LClass: TClass;
  LCriteria: IGBConnectionCriteria;
  LSQL: string;
begin
  LCriteria := GBConnection.Criteria.Interfaces.createCriteria;
  LClass := TMappingExplorer.GetInstance.Repository.FindEntityByTableName(TableName);
  if not Assigned(LClass) then
    raise Exception.CreateFmt('Classe referente a tabela %s n�o encontrada.', [TableName]);

  LCriteria.Where(Columns[0]).Equal(Values[0]);
  for I := 1 to Pred(Length(Columns)) do
  begin
    LCriteria
      .&And(Columns[I]).Equal(Values[I]);
  end;

  LSQL := FDML.GeneratorSelectWhere(LClass, LCriteria.AsString, '', 1);
  Result := Manager.SelectInternal(LSQL);
end;

function TGBModelDAOValidator<T>.Manager: TObjectManager<T>;
begin
  if not Assigned(FManager) then
    FManager := TObjectManager<T>.Create(Session, FConnection, 1);
  Result := FManager;
end;

end.
