unit GBConnection.Data.Model.Field;

interface

uses
  Data.DB,
  System.Generics.Collections,
  System.SysUtils;

type TGBConnectionDataModelField = class
  private
    FfieldName: string;
    FfieldValue: string;
    FfieldType: string;
    Frequired: Boolean;

  public
    property fieldName: string read FfieldName write FfieldName;
    property fieldValue: string read FfieldValue write FfieldValue;
    property fieldType: string read FfieldType write FfieldType;
    property required: Boolean read Frequired write Frequired;

    function GetFieldType: TFieldType;

    class function GetValue(AFieldName: String; ATableRow: TObjectList<TGBConnectionDataModelField>): string;
end;


implementation

{ TGBConnectionDataModelField }

function TGBConnectionDataModelField.GetFieldType: TFieldType;
begin
  Result := ftString;

  if fieldType.ToLower.Equals('ftstring') then Exit(ftString);
  if fieldType.ToLower.Equals('ftinteger') then Exit(ftInteger);
  if fieldType.ToLower.Equals('ftfloat') then Exit(ftFloat);
  if fieldType.ToLower.Equals('ftdate') then Exit(ftDate);
  if fieldType.ToLower.Equals('ftdatetime') then Exit(ftDateTime);
end;

class function TGBConnectionDataModelField.GetValue(AFieldName: String; ATableRow: TObjectList<TGBConnectionDataModelField>): string;
var
  I: Integer;
begin
  for I := 0 to Pred(ATableRow.Count) do
  begin
    if ATableRow[I].fieldName.ToLower.Equals(AFieldName.ToLower) then
      Exit(ATableRow[I].FfieldValue);
  end;

  raise Exception.CreateFmt('Campo %s n�o encontrado', [AFieldName]);
end;

end.
