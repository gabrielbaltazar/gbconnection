unit GBConnection.Controller;

interface

uses
  GBConnection.Model.Interfaces;

type
  IGBConnectionController = interface
    ['{E5BE9373-ACF3-497D-8CC0-E7C3DBF4B650}']
    function Connection: IGBConnection; overload;
    function Connection(Value: IGBConnection): IGBConnectionController; overload;
  end;

  TGBConnectionController = class(TInterfacedObject, IGBConnectionController)
    private
      FConnection: IGBConnection;

    protected
      function Connection: IGBConnection; overload;
      function Connection(Value: IGBConnection): IGBConnectionController; overload;
  end;

implementation

{ TGBConnectionController }

function TGBConnectionController.Connection(Value: IGBConnection): IGBConnectionController;
begin
  Result := Self;
  FConnection := Value;
end;

function TGBConnectionController.Connection: IGBConnection;
begin
  result := FConnection;
end;

end.
