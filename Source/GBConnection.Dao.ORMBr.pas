unit GBConnection.Dao.ORMBr;

interface

uses
  System.SysUtils,
  System.Classes,
  System.Variants,
  System.Generics.Collections,
  Data.DB,
  GBConnection.Dao.Types,
  GBConnection.Dao.Interfaces,
  GBConnection.Dao.ORMBr.Interceptors,
  GBConnection.Dao.ORMBr.Connection,
  GBConnection.Dao.ORMBr.Validator,
  GBConnection.Criteria,
  GBConnection.Criteria.Interfaces,
  GBConnection.Model.Interfaces,
  GBConnection.Model.Factory,
  ormbr.core.config,
  ormbr.factory.interfaces,
  ormbr.container.objectset,
  ormbr.mapping.classes,
  ormbr.mapping.explorer,
  ormbr.mapping.rttiutils,
  ormbr.container.objectset.interfaces;

type TGBModelDaoORMBr<T: class, constructor> = class(TGBConnectionDaoORMBrConnection<T>, IGBModelDao<T>)

  private
    FValidator: IGBModelDaoValidator<T>;
    FInterceptor: TGBConnectionDaoInterceptors<T>;
    FCriteria: IGBConnectionCriteria;

    function GetOffSet: Integer;
    function ContainsColumn(AColumn: String): Boolean;
    function IgnoreColumn(AColumn: String): Boolean;

  protected
    FQuery: IGBQuery;
    FORMBrContainer: IContainerObjectSet<T>;
    FPage: Integer;
    FPageSize: Integer;
    FQueryParams: TDictionary<String, String>;

    procedure QueryParams(AValue: TDictionary<String, String>);
    procedure SetPage(const AValue: Integer);
    procedure SetPageSize(const AValue: Integer);
    function UsePaginate: Boolean;
    function GetOrderBy: String;
    function ApplyQueryParams(ASql: String): String;

    procedure ValidateObject(Value: T; bInsert: Boolean);

    procedure ValidateInsert(Value: T); virtual;
    procedure ValidateUpdate(Value: T); virtual;
    procedure ValidateDelete(Value: T); virtual;

    function Criteria: IGBConnectionCriteria;
    function GenericDao<M: class, constructor>: IGBModelDao<M>;

  public
    function Validator: IGBModelDaoValidator<T>;

    procedure Insert(Value: T);
    procedure Update(Value: T);
    procedure Delete(Value: T);
    procedure Modify(Value: T);

    procedure DeleteWhere(ACriteria: IGBConnectionCriteria); overload;
    procedure DeleteWhere(AWhere: String); overload;
    procedure DeleteWhere(AWhere: string; const Args: array of const); overload;

    function ListAll: TObjectList<T>;
    function ListWhere(AWhere: String): TObjectList<T>; overload;
    function ListWhere(AWhere: String; const Args: array of const): TObjectList<T>; overload;
    function ListWhere(ACriteria: IGBConnectionCriteria): TObjectList<T>; overload; virtual;

    function Find(Key: array of variant): T; overload;
    function Find(Id: String): T; overload; virtual;
    function FindWhere(AWhere: String): T; overload;
    function FindWhere(AWhere: String; const Args: array of const): T; overload;
    function FindWhere(ACriteria: IGBConnectionCriteria): T; overload;

    constructor Create(AConnection: IGBConnection); override;
    class function NewGenericDAO(AConnection: IGBConnection): IGBModelDao<T>; virtual;
    destructor Destroy; override;
end;

implementation

uses
  GBConnection.Dao.ORMBr.Factory;

{ TGBModelDaoORMBr<T> }

function TGBModelDaoORMBr<T>.ApplyQueryParams(ASql: String): String;
var
  I: Integer;
  LCriteria: IGBConnectionCriteria;
  LHasWhere: Boolean;
  LField: String;
  LValue: String;
  LColumn: TColumnMapping;
begin
  Result := ASql;
  if (not Assigned(FQueryParams)) or (FQueryParams.Count <= 0) then
    Exit;

  LHasWhere := not Result.Trim.IsEmpty;
  LCriteria := CreateCriteria;

  for I := 0 to Pred(FQueryParams.Keys.Count) do
  begin
    LField := FQueryParams.Keys.ToArray[I];
    if IgnoreColumn(LField) then
      Continue;

    LValue := FQueryParams.Items[LField];
    if LValue.Trim = EmptyStr then
      Continue;

    LColumn := GetColumn(LField);
    if LColumn.FieldType = ftString then
    begin
      LField := 'lower(' + LField + ')';
      LValue := LValue.ToLower;
    end;

    if LHasWhere then
      LCriteria.&And(LField).Equal(LValue)
    else
    begin
      LCriteria.Where(LField).Equal(LValue);
      LHasWhere := True;
    end;
  end;

  Result := Result + LCriteria.AsString;

  if FQueryParams.ContainsKey('search') then
  begin
    LHasWhere := not Result.Trim.IsEmpty;
    if not LHasWhere then
      Result := '1 = 1';
    LValue := FQueryParams.Items['search'].ToLower;
    for LColumn in GetColumns do
    begin
      if not (LColumn.FieldType in [ftString]) then
        Continue;

      LField := 'lower(' + LColumn.ColumnName + ')';
      Criteria.&Or(LField).LikeFull(LValue);
    end;

    Result := '(' + Result + ') and (1 = 2 ' + Criteria.AsString + ')';
  end;

end;

function TGBModelDaoORMBr<T>.ContainsColumn(AColumn: String): Boolean;
var
  I: Integer;
begin
  Result := False;
  for I := 0 to Pred(GetColumns.Count) do
  begin
    if GetColumns[I].ColumnName.ToLower = AColumn.ToLower then
      Exit(True);
  end;
end;

constructor TGBModelDaoORMBr<T>.Create(AConnection: IGBConnection);
begin
  inherited;
  FManagerTransaction := True;
  FInterceptor := TGBConnectionDaoInterceptors<T>.Create(Self);
  FQuery := TGBFactoryConnection.New.createQuery(FConnection);
  FORMBrContainer := TContainerObjectSet<T>.Create(FORMBrConnection);
  FPage := -1;
  FPageSize := -1;

  if Assigned(FConnection.Monitor) then
    FORMBrConnection.SetCommandMonitor(ICommandMonitor(FConnection.Monitor));
end;

function TGBModelDaoORMBr<T>.Criteria: IGBConnectionCriteria;
begin
  if not Assigned(FCriteria) then
    FCriteria := TGBConnectionCriteria.New;
  Result := FCriteria;
end;

procedure TGBModelDaoORMBr<T>.Delete(Value: T);
begin
  FORMBrContainer.Delete(Value);
end;

procedure TGBModelDaoORMBr<T>.DeleteWhere(ACriteria: IGBConnectionCriteria);
begin
  DeleteWhere(ACriteria.AsString);
end;

procedure TGBModelDaoORMBr<T>.DeleteWhere(AWhere: String);
begin
  FQuery
    .SQL('delete from %s ', [Self.MappingTable.Name])
    .SQL('where %s', [AWhere])
    .ExecSQL;
end;

procedure TGBModelDaoORMBr<T>.DeleteWhere(AWhere: string; const Args: array of const);
begin
  DeleteWhere(Format(AWhere, Args));
end;

destructor TGBModelDaoORMBr<T>.Destroy;
begin
  FInterceptor.Free;
  inherited;
end;

function TGBModelDaoORMBr<T>.Find(Id: String): T;
begin
  Result := FindWhere(Format('%s = %s', [MappingPrimaryKey.Columns[0].ColumnName, Id.QuotedString]));
end;

function TGBModelDaoORMBr<T>.Find(Key: array of variant): T;
var
  LSQL: string;
  I: Integer;
begin
  for I := 0 to Pred(MappingPrimaryKey.Columns.Count) do
  begin
    if not LSQL.IsEmpty then
      LSQL := LSQL + ' and ';

    LSQL := LSQL + Format('%s = %s', [MappingPrimaryKey.Columns[I].ColumnName, varToStr( key[I]).QuotedString]);
  end;

  Result := FindWhere(LSQL);
end;

function TGBModelDaoORMBr<T>.FindWhere(AWhere: String): T;
var
  LList: TObjectList<T>;
begin
  Result := nil;
  LList := FORMBrContainer.FindWhere(AWhere);
  try
    if LList.Count > 0 then
      Result := LList.Extract(LList.First);
  finally
    LList.Free;
  end;
end;

function TGBModelDaoORMBr<T>.FindWhere(AWhere: String; const Args: array of const): T;
begin
  Result := FindWhere( Format(AWhere, Args));
end;

function TGBModelDaoORMBr<T>.GenericDao<M>: IGBModelDao<M>;
begin
  Result := TGBModelDaoORMBr<M>.NewGenericDAO(Self.FConnection);
end;

function TGBModelDaoORMBr<T>.GetOffSet: Integer;
begin
  Result := 0;
  if (FPage > 0) and (FPageSize > 0) then
  begin
    if FPage > 1 then
      Result := (FPage - 1) * FPageSize;
  end;
end;

function TGBModelDaoORMBr<T>.GetOrderBy: String;
var
  LKey: String;
begin
  Result := EmptyStr;
  if not Assigned(FQueryParams) then
    Exit;

  for LKey in FQueryParams.Keys do
  begin
    if LKey.ToLower = 'orderby' then
      Result := FQueryParams.Items[LKey];
  end;
end;

function TGBModelDaoORMBr<T>.IgnoreColumn(AColumn: String): Boolean;
var
  LField: String;
begin
  LField := AColumn.ToLower;
  Result := (LField = 'page') or
            (LField = 'pagesize') or
            (LField = 'search') or
            (LField = 'orderby');

  if not Result then
    Result := not ContainsColumn(LField);
end;

procedure TGBModelDaoORMBr<T>.Insert(Value: T);
begin
  FORMBrContainer.Insert(Value);
end;

function TGBModelDaoORMBr<T>.ListAll: TObjectList<T>;
var
  LOrderBy: String;
  LWhere: String;
begin
  LOrderBy := GetOrderBy;
  LWhere := ApplyQueryParams(EmptyStr);
  if LWhere.Trim.IsEmpty then
    LWhere := '1 = 1';

  if UsePaginate then
    Result := FORMBrContainer.NextPacket(LWhere, LOrderBy, FPageSize, GetOffSet)
  else
    Result := FORMBrContainer.FindWhere(LWhere, LOrderBy);
end;

function TGBModelDaoORMBr<T>.ListWhere(ACriteria: IGBConnectionCriteria): TObjectList<T>;
var
  LSql: string;
begin
  if Assigned(ACriteria) then
  begin
    LSql := ACriteria.AsString;
    Exit(Self.ListWhere(LSql));
  end;

  Result := Self.ListAll;
end;

function TGBModelDaoORMBr<T>.ListWhere(AWhere: String): TObjectList<T>;
var
  LOrderBy: String;
  LWhere: String;
begin
  LOrderBy := GetOrderBy;
  LWhere := ApplyQueryParams(AWhere);
  if LWhere.Trim.IsEmpty then
    LWhere := '1 = 1';

  if UsePaginate then
    Result := FORMBrContainer.NextPacket(LWhere, LOrderBy, FPageSize, GetOffSet)
  else
    Result := FORMBrContainer.FindWhere(LWhere, LOrderBy);
end;

function TGBModelDaoORMBr<T>.ListWhere(AWhere: String; const Args: array of const): TObjectList<T>;
begin
  Result := Self.ListWhere(Format( AWhere, Args));
end;

procedure TGBModelDaoORMBr<T>.Modify(Value: T);
begin
  FORMBrContainer.Modify(Value);
end;

class function TGBModelDaoORMBr<T>.NewGenericDAO(AConnection: IGBConnection): IGBModelDao<T>;
begin
  Result := Self.create(AConnection);
end;

procedure TGBModelDaoORMBr<T>.QueryParams(AValue: TDictionary<String, String>);
begin
  FQueryParams := AValue;
end;

procedure TGBModelDaoORMBr<T>.SetPage(const AValue: Integer);
begin
  FPage := AValue;
end;

procedure TGBModelDaoORMBr<T>.SetPageSize(const AValue: Integer);
begin
  FPageSize := AValue;
end;

procedure TGBModelDaoORMBr<T>.Update(Value: T);
begin
  if not TORMBrConfig.Instance.UseModifiedFields then
    Modify(Value);
  FORMBrContainer.Update(Value);
end;

function TGBModelDaoORMBr<T>.UsePaginate: Boolean;
begin
  Result := (FPage > 0) and (FPageSize > 0);
end;

procedure TGBModelDaoORMBr<T>.ValidateDelete(Value: T);
begin
  if not Assigned(Value) then
    raise EArgumentNilException.CreateFmt('Value should not be nil', []);
end;

procedure TGBModelDaoORMBr<T>.ValidateInsert(Value: T);
begin
  ValidateObject(Value, True);
end;

procedure TGBModelDaoORMBr<T>.ValidateObject(Value: T; bInsert: Boolean);
begin
  if not Assigned(Value) then
    raise EArgumentNilException.CreateFmt('Value should not be nil', []);
  Validator.RunValidate(Value, bInsert);
end;

procedure TGBModelDaoORMBr<T>.ValidateUpdate(Value: T);
begin
  ValidateObject(Value, False);
end;

function TGBModelDaoORMBr<T>.Validator: IGBModelDaoValidator<T>;
begin
  if not Assigned(FValidator) then
    FValidator := TGBModelDAOValidator<T>.New(Self.FORMBrConnection);
  Result := Self.FValidator;
end;

function TGBModelDaoORMBr<T>.findWhere(ACriteria: IGBConnectionCriteria): T;
var
  LSQL: string;
begin
  if Assigned(ACriteria) then
  begin
    LSQL := ACriteria.AsString;
    Exit(Self.FindWhere(LSQL));
  end;

  Result := Self.FindWhere('1 = 1');
end;

end.
