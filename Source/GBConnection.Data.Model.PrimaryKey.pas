unit GBConnection.Data.Model.PrimaryKey;

interface

type TGBConnectionDataModelPrimaryKey = class
  private
    Fchave: String;
    Fvalor: string;

  public
    property chave: String read Fchave write Fchave;
    property valor: string read Fvalor write Fvalor;
end;

implementation

end.

