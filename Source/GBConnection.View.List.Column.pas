unit GBConnection.View.List.Column;

interface

uses
  GBConnection.View.Lista.Interfaces,
  GBConnection.View.Column,
  System.SysUtils,
  System.Generics.Collections;

type TGBConnectionViewListColumn<T: class, constructor> = class(TInterfacedObject, IGBConnectionViewListColumn<T>)

  private
    [Weak] FParent: IGBConnectionViewLista<T>;

    FColumns: TList<IGBConnectionViewColumn<T>>;

    function ActiveColumn: IGBConnectionViewColumn<T>;

    function Add    (Value: String) : IGBConnectionViewListColumn<T>;
    function Caption(Value: String) : IGBConnectionViewListColumn<T>;
    function Width  (Value: Integer): IGBConnectionViewListColumn<T>;

    function Count: Integer;
    function Items(Index: Integer): IGBConnectionViewColumn<T>;
    function Contains(Value: String): Boolean;
    function Get(Value: String): IGBConnectionViewColumn<T>;

    function &End: IGBConnectionViewLista<T>;

  public
    constructor create(Parent: IGBConnectionViewLista<T>);
    class function New(Parent: IGBConnectionViewLista<T>): IGBConnectionViewListColumn<T>;
    destructor Destroy; override;
end;

implementation

{ TGBConnectionViewListColumn<T> }

function TGBConnectionViewListColumn<T>.ActiveColumn: IGBConnectionViewColumn<T>;
begin
  result := FColumns.Last;
end;

function TGBConnectionViewListColumn<T>.Add(Value: String): IGBConnectionViewListColumn<T>;
begin
  Result := Self;
  FColumns.Add(TGBConnectionViewColumn<T>.New(Self));
  ActiveColumn.FieldName(Value);
end;

function TGBConnectionViewListColumn<T>.Caption(Value: String): IGBConnectionViewListColumn<T>;
begin
  result := Self;
  ActiveColumn.Caption(Value);
end;

function TGBConnectionViewListColumn<T>.Contains(Value: String): Boolean;
var
  i: Integer;
begin
  result := False;
  for i := 0 to Pred(FColumns.Count) do
  begin
    if FColumns.Items[i].FieldName.ToLower.Equals(Value.ToLower) then
      Exit(True);
  end;
end;

function TGBConnectionViewListColumn<T>.Count: Integer;
begin
  result := FColumns.Count;
end;

constructor TGBConnectionViewListColumn<T>.create(Parent: IGBConnectionViewLista<T>);
begin
  FParent  := Parent;
  FColumns := TList<IGBConnectionViewColumn<T>>.create;
end;

destructor TGBConnectionViewListColumn<T>.Destroy;
begin
  FColumns.Free;
  inherited;
end;

function TGBConnectionViewListColumn<T>.&End: IGBConnectionViewLista<T>;
begin
  result := FParent;
end;

function TGBConnectionViewListColumn<T>.Get(Value: String): IGBConnectionViewColumn<T>;
var
  i : Integer;
begin
  result := nil;

  for i := 0 to Pred(Count) do
  begin
    if Items(i).FieldName.ToLower.Equals(Value.ToLower) then
      Exit(Items(i));
  end;
end;

function TGBConnectionViewListColumn<T>.Items(Index: Integer): IGBConnectionViewColumn<T>;
begin
  Result := FColumns.Items[Index];
end;

class function TGBConnectionViewListColumn<T>.New(Parent: IGBConnectionViewLista<T>): IGBConnectionViewListColumn<T>;
begin
  result := Self.create(Parent);
end;

function TGBConnectionViewListColumn<T>.Width(Value: Integer): IGBConnectionViewListColumn<T>;
begin
  result := Self;
  ActiveColumn.Width(Value);
end;

end.
