unit GBConnection.Dao.ORMBr.DataSet;

interface

uses
  Data.DB,
  GBConnection.Model.Interfaces,
  GBConnection.Dao.Interfaces,
  GBConnection.Dao.ORMBr.Connection,
  ormbr.container.dataset.interfaces,
  ormbr.container.clientdataset,
  ormbr.factory.interfaces,
  Datasnap.DBClient,
  System.SysUtils,
  System.StrUtils,
  System.Variants,
  System.Classes;

type TGBConnectionDAOORMBrDataSet<T: class, constructor> = class(TGBConnectionDaoORMBrConnection<T>, IGBModelDaoDataSet<T>)

  protected
    FClientDataSet: TClientDataSet;
    FORMBrContainer: IContainerDataSet<T>;
    FNewThis: T;

    function Container: IContainerDataSet<T>;

  public
    function KeyNames: TArray<String>;
    function KeyValues: TArray<Variant>;

    function Open(const Values: array of variant): IGBModelDaoDataSet<T>; overload;
    function Open: IGBModelDaoDataSet<T>; overload;

    function ApplyUpdates: IGBModelDaoDataSet<T>;
    function Save: IGBModelDaoDataSet<T>; virtual;
    function Delete: IGBModelDaoDataSet<T>;

    function DataSet(Value: TDataSet): IGBModelDaoDataSet<T>; overload;
    function DataSource(Value: TDataSource): IGBModelDaoDataSet<T>;

    function This: T;
    function NewThis: T; overload;
    function NewThis(Value: T): IGBModelDaoDataSet<T>; overload;

    constructor Create(AConnection: IGBConnection); override;
    destructor Destroy; override;
end;

implementation

{ TGBConnectionDAOORMBrDataSet<T> }

function TGBConnectionDAOORMBrDataSet<T>.ApplyUpdates: IGBModelDaoDataSet<T>;
begin
  Result := Self;
  FORMBrContainer.ApplyUpdates(0);
end;

function TGBConnectionDAOORMBrDataSet<T>.Container: IContainerDataSet<T>;
begin
  Result := FORMBrContainer;
end;

constructor TGBConnectionDAOORMBrDataSet<T>.Create(AConnection: IGBConnection);
begin
  inherited;
  FClientDataSet := TClientDataSet.Create(nil);
  FORMBrContainer := TContainerClientDataSet<T>.Create(FORMBrConnection, FClientDataSet);
end;

function TGBConnectionDAOORMBrDataSet<T>.DataSet(Value: TDataSet): IGBModelDaoDataSet<T>;
begin
  Result := Self;
end;

function TGBConnectionDAOORMBrDataSet<T>.DataSource(Value: TDataSource): IGBModelDaoDataSet<T>;
begin
  Result := Self;
  Value.DataSet := FClientDataSet;
end;

function TGBConnectionDAOORMBrDataSet<T>.Delete: IGBModelDaoDataSet<T>;
begin
  Result := Self;
  FORMBrContainer.Delete;
end;

destructor TGBConnectionDAOORMBrDataSet<T>.Destroy;
begin
  FClientDataSet.Free;
  inherited;
end;

function TGBConnectionDAOORMBrDataSet<T>.NewThis: T;
begin
  if not Assigned(FNewThis) then
    FNewThis := FORMBrContainer.Current;
  Result := FNewThis;
end;

function TGBConnectionDAOORMBrDataSet<T>.Open(const Values: array of variant): IGBModelDaoDataSet<T>;
var
  I: Integer;
  LSQL: string;
begin
  for I := 0 to Pred(MappingPrimaryKey.Columns.Count) do
  begin
    LSQL := IfThen(I = 0, '%s.%s = %s', ' and %s.%s = %s');
    LSQL := Format(LSQL,
        [MappingTable.Name,
         MappingPrimaryKey.Columns[I].ColumnName,
         VartoStr(Values[I]).QuotedString]);
  end;

  FORMBrContainer.OpenWhere(LSQL);
end;

function TGBConnectionDAOORMBrDataSet<T>.Open: IGBModelDaoDataSet<T>;
begin
  Result := Self;
  FORMBrContainer.Open;
end;

function TGBConnectionDAOORMBrDataSet<T>.KeyNames: TArray<String>;
var
  I: Integer;
begin
  SetLength(Result, Self.FPrimaryKey.Columns.Count);
  for I := 0 to Pred(Self.FPrimaryKey.Columns.Count) do
    Result[I] := FPrimaryKey.Columns[I].ColumnName;
end;

function TGBConnectionDAOORMBrDataSet<T>.KeyValues: TArray<Variant>;
var
  I: Integer;
  LNames: TArray<String>;
begin
  LNames := KeyNames;
  SetLength(Result, Length(LNames));

  for I := 0 to Pred(Length(LNames)) do
    Result[I] := Self.FClientDataSet.FieldByName(LNames[I]).Value;
end;

function TGBConnectionDAOORMBrDataSet<T>.NewThis(Value: T): IGBModelDaoDataSet<T>;
begin
  Result := Self;
  FNewThis := Value;
end;

function TGBConnectionDAOORMBrDataSet<T>.Save: IGBModelDaoDataSet<T>;
begin
  Result := Self;
  if not (FClientDataSet.State in dsEditModes) then
    FClientDataSet.Insert;

  FClientDataSet.Post;
  FORMBrContainer.Save(NewThis);
end;

function TGBConnectionDAOORMBrDataSet<T>.This: T;
begin
  Result := FORMBrContainer.Current;
end;

end.
