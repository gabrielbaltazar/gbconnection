unit GBConnection.Dao.ORMBr.Zeos;

interface

uses
  GBConnection.Model.Interfaces,
  GBconnection.Dao.ORMBr.Interfaces,
  ormbr.factory.interfaces,
  ormbr.factory.zeos,
  System.SysUtils;

type TGBConnectionDaoORMBrZeos = class(TInterfacedObject, IGBConnectionDaoORMBr)

  protected
    function GetDriverName(Connection: IGBConnection): TDriverName;
    function GetConnection(Connection: IGBConnection): IDBConnection;

  public
    class function New: IGBConnectionDaoORMBr;
end;

implementation

{ TGBConnectionDaoORMBrZeos }

function TGBConnectionDaoORMBrZeos.GetConnection(Connection: IGBConnection): IDBConnection;
begin
  Result := TFactoryZeos.Create(Connection.Component, GetDriverName(Connection));
end;

function TGBConnectionDaoORMBrZeos.GetDriverName(Connection: IGBConnection): TDriverName;
begin
  case Connection.Params.Driver of
    gbOracle: result := TDriverName.dnOracle;
    gbMSSQL: result := TDriverName.dnMSSQL;
    gbMySql: result := TDriverName.dnMySQL;
    gbSQLite: result := TDriverName.dnSQLite;
    gbPostgres: result := TDriverName.dnPostgreSQL;
  else
    raise ENotImplemented.Create('Driver n�o implementado.');
  end;
end;

class function TGBConnectionDaoORMBrZeos.New: IGBConnectionDaoORMBr;
begin
  Result := Self.Create;
end;

end.
