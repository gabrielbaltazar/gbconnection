unit GBConnection.Dao.ORMBr.Factory;

interface

uses
  {$IFDEF ZEOS}
  GBConnection.Dao.ORMBr.Zeos,
  {$ENDIF}
  {$IFDEF FIREDAC}
  GBConnection.Dao.ORMBr.Firedac,
  {$ENDIF}
  GBconnection.Dao.ORMBr.Interfaces;

type TGBConnectionDaoORMBrFactory = class(TInterfacedObject, IGBConnectionDaoORMBrFactory)

  public
    function CreateConnectionORMBr: IGBConnectionDaoORMBr;

    class function New: IGBConnectionDaoORMBrFactory;
end;

implementation

{ TGBConnectionDaoORMBrFactory }

function TGBConnectionDaoORMBrFactory.createConnectionORMBr: IGBConnectionDaoORMBr;
begin
  {$IFDEF FIREDAC}
    Exit( TGBConnectionDaoORMBrFiredac.New );
  {$ENDIF}

  {$IFDEF ZEOS}
    Exit( TGBConnectionDaoORMBrZeos.New );
  {$ENDIF}
end;

class function TGBConnectionDaoORMBrFactory.New: IGBConnectionDaoORMBrFactory;
begin
  Result := Self.Create;
end;

end.
