unit GBConnection.View.Lista.Crud;

interface

uses
  Vcl.Controls,
  Data.DB,
  System.Classes,
  System.SysUtils,
  System.StrUtils,
  GBConnection.Model.Interfaces,
  GBConnection.Dao.Interfaces,
  GBConnection.Dao.ORMBr.DataSet,
  GBConnection.Dao.ORMBr,
  GBConnection.Controller,
  GBConnection.View.Lista.Interfaces,
  GBConnection.View.Lista,
  GBConnection.View.Cadastro,
  GBConnection.View.Cadastro.Crud,
  GBConnection.View.List.Column;

type
  TGBConnectionViewLista<T: class, constructor> = class(TInterfacedObject, IGBConnectionViewLista<T>)
    private
      [Weak] FController: IGBConnectionController;

      FormCadastro : TGBViewFormCadastro;
      FormLista    : TFGBConnectionViewLista;
      FORMBrDataSet: IGBModelDaoDataSet<T>;
      FModelDAO    : IGBModelDao<T>;
      FColumns     : IGBConnectionViewListColumn<T>;

      procedure Refresh(Value: TArray<Variant>);

      procedure SetupGrid;

      function KeysValues: TArray<Variant>;

      procedure OnNew   (Sender: TObject);
      procedure OnDelete(Sender: TObject);
      procedure OnView  (Sender: TObject);

      procedure SetEvents;

      function Controller (Value: IGBConnectionController): IGBConnectionViewLista<T>;
      function DAO        (Value: IGBModelDao<T>)         : IGBConnectionViewLista<T>;
      function Cadastro   (Value: TGBViewFormCadastro)    : IGBConnectionViewLista<T>;

      function Open  : IGBConnectionViewLista<T>; overload;
      function Build : IGBConnectionViewLista<T>;

      function Columns: IGBConnectionViewListColumn<T>;

    public
      constructor create(AForm: TFGBConnectionViewLista); virtual;
      class function New(AForm: TFGBConnectionViewLista): IGBConnectionViewLista<T>;
  end;

implementation

{ TGBConnectionViewLista<T> }

function TGBConnectionViewLista<T>.Build: IGBConnectionViewLista<T>;
begin
  Result        := Self;
  FORMBrDataSet := TGBConnectionDAOORMBrDataSet<T>.create(FController.Connection);
  FORMBrDataSet.DataSource(FormLista.DataSource);

  SetEvents;
end;

function TGBConnectionViewLista<T>.Cadastro(Value: TGBViewFormCadastro): IGBConnectionViewLista<T>;
begin
  result := Self;
  Self.FormCadastro := Value;
end;

function TGBConnectionViewLista<T>.Columns: IGBConnectionViewListColumn<T>;
begin
  result := FColumns;
end;

function TGBConnectionViewLista<T>.Controller(Value: IGBConnectionController): IGBConnectionViewLista<T>;
begin
  Result      := Self;
  FController := Value;
end;

constructor TGBConnectionViewLista<T>.create(AForm: TFGBConnectionViewLista);
begin
  FormLista := AForm;
  FColumns  := TGBConnectionViewListColumn<T>.new(Self);
end;

function TGBConnectionViewLista<T>.DAO(Value: IGBModelDao<T>): IGBConnectionViewLista<T>;
begin
  result := Self;
  FModelDAO := Value;
end;

function TGBConnectionViewLista<T>.KeysValues: TArray<Variant>;
begin
  result := FORMBrDataSet.KeyValues;
end;

class function TGBConnectionViewLista<T>.New(AForm: TFGBConnectionViewLista): IGBConnectionViewLista<T>;
begin
  result := Self.create(AForm);
end;

procedure TGBConnectionViewLista<T>.OnDelete(Sender: TObject);
var
  model : T;
begin
  model := FORMBrDataSet.NewThis;
  try
    FORMBrDataSet.Delete;
    FModelDAO.delete(model);
  finally
    FORMBrDataSet.NewThis(nil);
  end;
end;

procedure TGBConnectionViewLista<T>.OnNew(Sender: TObject);
var
  form : TFGBConnectionViewCadastro;
begin
  form := FormCadastro.Create(nil);
  try
    form.ShowModal;
    if form.ModalResult = mrOk then
      Self.Refresh(form.Keys);
  finally
    form.Free;
  end;
end;

procedure TGBConnectionViewLista<T>.OnView(Sender: TObject);
var
  form: TFGBConnectionViewCadastro;
begin
  form := FormCadastro.Create(nil, KeysValues);
  try
    form.ShowModal;
    if form.ModalResult = mrOk then
      Refresh(form.Keys);
  finally
    form.Free;
  end;
end;

function TGBConnectionViewLista<T>.Open: IGBConnectionViewLista<T>;
begin
  result := Self;
  FORMBrDataSet.Open;

  SetupGrid;
end;

procedure TGBConnectionViewLista<T>.Refresh(Value: TArray<Variant>);
var
  keys      : TArray<String>;
  keyFields : string;
  i         : Integer;
begin
  keys := FORMBrDataSet.KeyNames;
  try
    for i := 0 to Length(keys) - 1 do
      keyFields := ';' + keys[i];

    keyFields := keyFields.Substring(1);
    Self.Open;
    Self.FormLista.DataSource.DataSet.Locate(keyFields, Value, []);
  finally
  end;
end;

procedure TGBConnectionViewLista<T>.SetEvents;
begin
  Self.FormLista.SetOnNew(OnNew);
  Self.FormLista.SetOnDelete(OnDelete);
  Self.FormLista.SetOnView(OnView);
end;

procedure TGBConnectionViewLista<T>.SetupGrid;
var
  i : Integer;
begin
  if FColumns.Count <= 0 then
    Exit;

  for i := 0 to Pred(Self.FormLista.GridComponent.Columns.Count) do
  begin
    Self.FormLista.GridComponent.Columns.Items[i].Visible := FColumns.Contains(Self.FormLista.GridComponent.Columns.Items[i].FieldName);

    if Self.FormLista.GridComponent.Columns.Items[i].Visible then
    begin
      if not FColumns.Get(Self.FormLista.GridComponent.Columns.Items[i].FieldName).Caption.IsEmpty then
        Self.FormLista.GridComponent.Columns.Items[i].Title.Caption := FColumns.Get(Self.FormLista.GridComponent.Columns.Items[i].FieldName).Caption;

      if not FColumns.Get(Self.FormLista.GridComponent.Columns.Items[i].FieldName).Width = 0 then
        Self.FormLista.GridComponent.Columns.Items[i].Width := FColumns.Get(Self.FormLista.GridComponent.Columns.Items[i].FieldName).Width;
    end;
  end;
end;

end.
