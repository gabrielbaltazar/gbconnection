unit GBConnection.Model.Params;

interface

uses
  GBConnection.Model.Interfaces;

Type
  TGBConnectionParams<I: IInterface> = class(TInterfacedObject, IGBConnectionParams<I>)
  private
    [WeakAttribute]
    FParent: I;

    FDatabase: String;
    FUserName: String;
    FPassword: String;
    FServer: String;
    FSchema: string;
    FPort: Integer;
    FAutoCommit: Boolean;
    FDriver: TGBDriverConnection;

  public
    function Database(Value: string): IGBConnectionParams<I>; overload;
    function UserName(Value: string): IGBConnectionParams<I>; overload;
    function Password(Value: string): IGBConnectionParams<I>; overload;
    function Server(Value: string): IGBConnectionParams<I>; overload;
    function Schema(Value: String): IGBConnectionParams<I>; overload;
    function Port(Value: Integer): IGBConnectionParams<I>; overload;
    function AutoCommit(Value: Boolean): IGBConnectionParams<I>; overload;
    function Driver(Value: TGBDriverConnection): IGBConnectionParams<I>; overload;

    function Database: string; overload;
    function UserName: string; overload;
    function Password: string; overload;
    function Server: string; overload;
    function Schema: string; overload;
    function Port: Integer; overload;
    function AutoCommit: Boolean; overload;
    function Driver: TGBDriverConnection; overload;

    function &Begin: IGBConnectionParams<I>;
    function &End: I;

    constructor Create(AInterface: I);
    class function New(AInterface: I): IGBConnectionParams<I>;
    destructor Destroy; override;
  end;

implementation

{ TGBConnectionParams }

function TGBConnectionParams<I>.Database(Value: String): IGBConnectionParams<I>;
begin
  Result := Self;
  FDatabase := Value;
end;

class function TGBConnectionParams<I>.New(AInterface: I): IGBConnectionParams<I>;
begin
  Result := Self.Create(AInterface);
end;

function TGBConnectionParams<I>.Password(Value: String): IGBConnectionParams<I>;
begin
  Result := Self;
  FPassword := Value;
end;

function TGBConnectionParams<I>.Port(Value: Integer): IGBConnectionParams<I>;
begin
  Result := Self;
  FPort := Value;
end;

function TGBConnectionParams<I>.Server(Value: String): IGBConnectionParams<I>;
begin
  Result := Self;
  FServer := Value;
end;

function TGBConnectionParams<I>.UserName(Value: String): IGBConnectionParams<I>;
begin
  Result := Self;
  FUserName := Value;
end;

function TGBConnectionParams<I>.AutoCommit: Boolean;
begin
  Result := FAutoCommit;
end;

function TGBConnectionParams<I>.AutoCommit(Value: Boolean): IGBConnectionParams<I>;
begin
  Result := Self;
  FAutoCommit := Value;
end;

function TGBConnectionParams<I>.&Begin: IGBConnectionParams<I>;
begin
  Result := Self;
end;

function TGBConnectionParams<I>.&End: I;
begin
  Result := FParent;
end;

constructor TGBConnectionParams<I>.create(AInterface: I);
begin
  FParent := AInterface;
  FAutoCommit := True;
end;

function TGBConnectionParams<I>.Database: string;
begin
  Result := FDatabase;
end;

destructor TGBConnectionParams<I>.Destroy;
begin

  inherited;
end;

function TGBConnectionParams<I>.Driver: TGBDriverConnection;
begin
  Result := FDriver;
end;

function TGBConnectionParams<I>.Driver(Value: TGBDriverConnection): IGBConnectionParams<I>;
begin
  Result  := Self;
  FDriver := Value;
end;

function TGBConnectionParams<I>.Password: string;
begin
  Result := FPassword;
end;

function TGBConnectionParams<I>.Port: Integer;
begin
  Result := FPort;
end;

function TGBConnectionParams<I>.Schema: string;
begin
  Result := FSchema;
end;

function TGBConnectionParams<I>.Schema(Value: String): IGBConnectionParams<I>;
begin
  Result := Self;
  FSchema := Value;
end;

function TGBConnectionParams<I>.Server: string;
begin
  Result := FServer;
end;

function TGBConnectionParams<I>.UserName: string;
begin
  Result := FUserName;
end;

end.
