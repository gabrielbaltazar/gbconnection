unit GBConnection.Criteria;

interface

uses
  GBConnection.Criteria.Interfaces,
  GBConnection.Criteria.Operators,
  GBConnection.Criteria.Types,
  System.Generics.Collections,
  System.SysUtils,
  System.Variants;

type TGBConnectionCriteria = class(TInterfacedObject, IGBConnectionCriteria)

  private
    FDateFormat: string;
    FDateTimeFormat: string;
    FOperators: TList<IGBConnectionCriteriaOperator>;

    function GetDateTimeValue(AValue: TDateTime; bUseHour: Boolean = False): String;

    function ActiveOperator: IGBConnectionCriteriaOperator;
    function AddOperator(AValue: String): TGBConnectionCriteria;

    function UpdateOperator(AValue: Variant;
                            ACompare: TGBConnectionCriteriaCompare;
                            AType: TGBConnectionCriteriaFieldType): TGBConnectionCriteria;
  public
    function DateFormat(Value: String): IGBConnectionCriteria;
    function DateTimeFormat(Value: String): IGBConnectionCriteria;

    function Where(const Value: string): IGBConnectionCriteria;
    function &And(const Value: string): IGBConnectionCriteria;
    function &Or(const Value: string): IGBConnectionCriteria;

    function IsNullIfEmpty: IGBConnectionCriteria;

    function Equal(const Value: String): IGBConnectionCriteria; overload;
    function Equal(const Value: Extended): IGBConnectionCriteria overload;
    function Equal(const Value: Integer): IGBConnectionCriteria; overload;
    function EqualDt(const Value: TDateTime; bUseHour: Boolean = False): IGBConnectionCriteria; overload;

    function NotEqual(const Value: String): IGBConnectionCriteria; overload;
    function NotEqual(const Value: Extended): IGBConnectionCriteria; overload;
    function NotEqual(const Value: Integer): IGBConnectionCriteria; overload;
    function NotEqualDt(const Value: TDateTime; bUseHour: Boolean = False): IGBConnectionCriteria; overload;

    function IsNull: IGBConnectionCriteria;
    function IsNotNull: IGBConnectionCriteria;

    function GreaterThan(const Value: Extended): IGBConnectionCriteria; overload;
    function GreaterThan(const Value: Integer): IGBConnectionCriteria; overload;
    function GreaterThan(const Value: String): IGBConnectionCriteria; overload;
    function GreaterThanDt(const Value: TDateTime; bUseHour: Boolean = False): IGBConnectionCriteria;
    function GreaterEqThan(const Value: Extended): IGBConnectionCriteria; overload;
    function GreaterEqThan(const Value: Integer): IGBConnectionCriteria; overload;
    function GreaterEqThan(const Value: String): IGBConnectionCriteria; overload;
    function GreaterEqThanDt(const Value: TDateTime; bUseHour: Boolean = False): IGBConnectionCriteria; overload;

    function LessThan(const Value: Extended): IGBConnectionCriteria; overload;
    function LessThan(const Value: Integer): IGBConnectionCriteria; overload;
    function LessThan(const Value: String): IGBConnectionCriteria; overload;
    function LessThanDt(const Value: TDateTime; bUseHour: Boolean = False): IGBConnectionCriteria; overload;
    function LessEqThan(const Value: Extended): IGBConnectionCriteria; overload;
    function LessEqThan(const Value: Integer): IGBConnectionCriteria; overload;
    function LessEqThan(const Value: String): IGBConnectionCriteria; overload;
    function LessEqThanDt(const Value: TDateTime; bUseHour: Boolean = False): IGBConnectionCriteria; overload;

    function IsIn(const Value: TArray<Double>): IGBConnectionCriteria; overload;
    function IsIn(const Value: TArray<String>): IGBConnectionCriteria; overload;
    function IsIn(const Value: String): IGBConnectionCriteria; overload;

    function IsNotIn(const Value: TArray<Double>): IGBConnectionCriteria; overload;
    function IsNotIn(const Value: TArray<String>): IGBConnectionCriteria; overload;
    function IsNotIn(const Value: String): IGBConnectionCriteria; overload;

    function LikeFull(const Value: String): IGBConnectionCriteria;
    function LikeLeft(const Value: String): IGBConnectionCriteria;
    function LikeRight(const Value: String): IGBConnectionCriteria;
    function NotLikeFull(const Value: String): IGBConnectionCriteria;
    function NotLikeLeft(const Value: String): IGBConnectionCriteria;
    function NotLikeRight(const Value: String): IGBConnectionCriteria;

    function AsString(bClear: boolean = True): string;
    function Count: Integer;

    constructor Create;
    destructor  Destroy; override;
    class function New: IGBConnectionCriteria;
end;

implementation

{ TGBConnectionCriteria }

function TGBConnectionCriteria.ActiveOperator: IGBConnectionCriteriaOperator;
begin
  Result := FOperators.Last;
end;

function TGBConnectionCriteria.AddOperator(AValue: String): TGBConnectionCriteria;
var
  LOperators: IGBConnectionCriteriaOperator;
begin
  Result := Self;
  LOperators := TGBConnectionCriteriaOperator.New;
  LOperators.ColumnName := AValue;
  LOperators.IsNullIfEmpty := False;

  FOperators.Add(LOperators);
end;

function TGBConnectionCriteria.&And(const Value: string): IGBConnectionCriteria;
begin
  Result := Self;
  AddOperator('and ' + Value);
end;

function TGBConnectionCriteria.Equal(const Value: Extended): IGBConnectionCriteria;
begin
  Result := Self;
  UpdateOperator(Value, fcEqual, dftFloat);
end;

function TGBConnectionCriteria.Equal(const Value: String): IGBConnectionCriteria;
begin
  Result := Self;
  UpdateOperator(Value, fcEqual, dftString);
end;

function TGBConnectionCriteria.Equal(const Value: Integer): IGBConnectionCriteria;
begin
  Result := Self;
  UpdateOperator(Value, fcEqual, dftInteger);
end;

function TGBConnectionCriteria.EqualDt(const Value: TDateTime; bUseHour: Boolean): IGBConnectionCriteria;
begin
  Result := Self;
  UpdateOperator(GetDateTimeValue(Value, bUseHour), fcEqual, dftString);
end;

function TGBConnectionCriteria.AsString(bClear: boolean = True): string;
var
  LOper : IGBConnectionCriteriaOperator;
begin
  Result := EmptyStr;
  for LOper in FOperators do
    Result := Result + ' ' + LOper.AsString;

  if bClear then
    FOperators.Clear;
end;

function TGBConnectionCriteria.Count: Integer;
begin
  Result := FOperators.Count;
end;

constructor TGBConnectionCriteria.Create;
begin
  FOperators := TList<IGBConnectionCriteriaOperator>.Create;
  FDateFormat := 'dd/MM/yyyy';
  FDateTimeFormat := 'dd/MM/yyyy hh:mm:ss';
end;

function TGBConnectionCriteria.DateFormat(Value: String): IGBConnectionCriteria;
begin
  Result := Self;
  FDateFormat := Value;
end;

function TGBConnectionCriteria.DateTimeFormat(Value: String): IGBConnectionCriteria;
begin
  Result := Self;
  FDateTimeFormat := Value;
end;

destructor TGBConnectionCriteria.Destroy;
begin
  FOperators.Free;
  inherited;
end;

function TGBConnectionCriteria.GreaterEqThan(const Value: Integer): IGBConnectionCriteria;
begin
  Result := Self;
  UpdateOperator(Value, fcGreaterEqual, dftInteger);
end;

function TGBConnectionCriteria.GetDateTimeValue(AValue: TDateTime; bUseHour: Boolean): String;
begin
  Result := FormatDateTime(FDateFormat, AValue);
  if bUseHour then
    Result := FormatDateTime(FDateTimeFormat, AValue);
end;

function TGBConnectionCriteria.GreaterEqThan(const Value: String): IGBConnectionCriteria;
begin
  Result := Self;
  UpdateOperator(Value, fcGreaterEqual, dftString);
end;

function TGBConnectionCriteria.GreaterEqThanDt(const Value: TDateTime; bUseHour: Boolean = False): IGBConnectionCriteria;
begin
  Result := Self;
  UpdateOperator(GetDateTimeValue(Value, bUseHour), fcGreaterEqual, dftString);
end;

function TGBConnectionCriteria.GreaterEqThan(const Value: Extended): IGBConnectionCriteria;
begin
  Result := Self;
  UpdateOperator(Value, fcGreaterEqual, dftFloat);
end;

function TGBConnectionCriteria.GreaterThan(const Value: Integer): IGBConnectionCriteria;
begin
  Result := Self;
  UpdateOperator(Value, fcGreater, dftInteger);
end;

function TGBConnectionCriteria.GreaterThan(const Value: String): IGBConnectionCriteria;
begin
  Result := Self;
  UpdateOperator(Value, fcGreater, dftString);
end;

function TGBConnectionCriteria.GreaterThanDt(const Value: TDateTime; bUseHour: Boolean = False): IGBConnectionCriteria;
begin
  Result := Self;
  UpdateOperator(GetDateTimeValue(Value, bUseHour), fcGreater, dftString);
end;

function TGBConnectionCriteria.IsIn(const Value: String): IGBConnectionCriteria;
begin
  Result := Self;
  UpdateOperator(Value, fcIn, dftString);
end;

function TGBConnectionCriteria.LikeFull(const Value: String): IGBConnectionCriteria;
begin
  Result := Self;
  UpdateOperator(Value, fcLikeFull, dftString);
end;

function TGBConnectionCriteria.LikeLeft(const Value: String): IGBConnectionCriteria;
begin
  Result := Self;
  UpdateOperator(Value, fcLikeLeft, dftString);
end;

function TGBConnectionCriteria.LikeRight(const Value: String): IGBConnectionCriteria;
begin
  Result := Self;
  UpdateOperator(Value, fcLikeRight, dftString);
end;

function TGBConnectionCriteria.IsIn(const Value: TArray<String>): IGBConnectionCriteria;
begin
  Result := Self;
  UpdateOperator(Value, fcIn, dftArray);
end;

function TGBConnectionCriteria.IsIn(const Value: TArray<Double>): IGBConnectionCriteria;
begin
  Result := Self;
  UpdateOperator(Value, fcIn, dftArray);
end;

function TGBConnectionCriteria.IsNotIn(const Value: String): IGBConnectionCriteria;
begin
  Result := Self;
  UpdateOperator(Value, fcNotIn, dftString);
end;

function TGBConnectionCriteria.NotLikeFull(const Value: String): IGBConnectionCriteria;
begin
  Result := Self;
  UpdateOperator(Value, fcNotLikeFull, dftString);
end;

function TGBConnectionCriteria.NotLikeLeft(const Value: String): IGBConnectionCriteria;
begin
  result := Self;
  UpdateOperator(Value, fcNotLikeLeft, dftString);
end;

function TGBConnectionCriteria.NotLikeRight(const Value: String): IGBConnectionCriteria;
begin
  result := Self;
  UpdateOperator(Value, fcNotLikeRight, dftString);
end;

function TGBConnectionCriteria.IsNotIn(const Value: TArray<String>): IGBConnectionCriteria;
begin
  Result := Self;
  UpdateOperator(Value, fcNotIn, dftArray);
end;

function TGBConnectionCriteria.IsNotIn(const Value: TArray<Double>): IGBConnectionCriteria;
begin
  Result := Self;
  UpdateOperator(Value, fcNotIn, dftArray);
end;

function TGBConnectionCriteria.IsNotNull: IGBConnectionCriteria;
begin
  Result := Self;
  UpdateOperator(null, fcIsNotNull, dftString);
end;

function TGBConnectionCriteria.IsNull: IGBConnectionCriteria;
begin
  Result := Self;
  UpdateOperator(null, fcIsNull, dftString);
end;

function TGBConnectionCriteria.IsNullIfEmpty: IGBConnectionCriteria;
begin
  Result := Self;
  ActiveOperator.IsNullIfEmpty := True;
end;

function TGBConnectionCriteria.LessEqThan(const Value: Extended): IGBConnectionCriteria;
begin
  Result := Self;
  UpdateOperator(Value, fcLessEqual, dftFloat);
end;

function TGBConnectionCriteria.LessEqThan(const Value: Integer): IGBConnectionCriteria;
begin
  Result := Self;
  UpdateOperator(Value, fcLessEqual, dftInteger);
end;

function TGBConnectionCriteria.LessEqThan(const Value: String): IGBConnectionCriteria;
begin
  Result := Self;
  UpdateOperator(Value, fcLessEqual, dftString);
end;

function TGBConnectionCriteria.LessEqThanDt(const Value: TDateTime; bUseHour: Boolean = False): IGBConnectionCriteria;
begin
  Result := Self;
  UpdateOperator(GetDateTimeValue(Value, bUseHour), fcLessEqual, dftString);
end;

function TGBConnectionCriteria.LessThan(const Value: Extended): IGBConnectionCriteria;
begin
  Result := Self;
  UpdateOperator(Value, fcLess, dftFloat);
end;

function TGBConnectionCriteria.LessThan(const Value: Integer): IGBConnectionCriteria;
begin
  Result := Self;
  UpdateOperator(Value, fcLess, dftInteger);
end;

function TGBConnectionCriteria.LessThan(const Value: String): IGBConnectionCriteria;
begin
  Result := Self;
  UpdateOperator(Value, fcLess, dftString);
end;

function TGBConnectionCriteria.LessThanDt(const Value: TDateTime; bUseHour: Boolean = False): IGBConnectionCriteria;
begin
  Result := Self;
  UpdateOperator(GetDateTimeValue(Value, bUseHour), fcLess, dftString);
end;

function TGBConnectionCriteria.GreaterThan(const Value: Extended): IGBConnectionCriteria;
begin
  Result := Self;
  UpdateOperator(Value, fcGreater, dftFloat);
end;

function TGBConnectionCriteria.NotEqual(const Value: String): IGBConnectionCriteria;
begin
  Result := Self;
  UpdateOperator(Value, fcNotEqual, dftString);
end;

function TGBConnectionCriteria.NotEqual(const Value: Extended): IGBConnectionCriteria;
begin
  Result := Self;
  UpdateOperator(Value, fcNotEqual, dftFloat);
end;

class function TGBConnectionCriteria.New: IGBConnectionCriteria;
begin
  Result := Self.Create;
end;

function TGBConnectionCriteria.NotEqual(const Value: Integer): IGBConnectionCriteria;
begin
  Result := Self;
  UpdateOperator(Value, fcNotEqual, dftInteger);
end;

function TGBConnectionCriteria.&Or(const Value: string): IGBConnectionCriteria;
begin
  Result := Self;
  AddOperator('or ' + value);
end;

function TGBConnectionCriteria.UpdateOperator(AValue: Variant; ACompare: TGBConnectionCriteriaCompare; AType: TGBConnectionCriteriaFieldType): TGBConnectionCriteria;
begin
  Result := Self;
  ActiveOperator.Compare  := ACompare;
  ActiveOperator.Value    := AValue;
  ActiveOperator.DataType := AType;
end;

function TGBConnectionCriteria.Where(const Value: string): IGBConnectionCriteria;
begin
  Result := Self;
  AddOperator(Value);
end;

function TGBConnectionCriteria.NotEqualDt(const Value: TDateTime; bUseHour: Boolean = False): IGBConnectionCriteria;
begin
  Result := Self;
  UpdateOperator(GetDateTimeValue(Value, bUseHour), fcNotEqual, dftString);
end;

end.
