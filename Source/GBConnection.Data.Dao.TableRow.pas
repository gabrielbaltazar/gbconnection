unit GBConnection.Data.Dao.TableRow;

interface

uses
  GBConnection.Model.Interfaces,
  GBConnection.Model.Factory,
  GBConnection.Data.Model.Types,
  GBConnection.Data.Model.TableColumn,
  GBConnection.Data.Model.Table,
  GBConnection.Data.Model.TableRow,
  GBConnection.Data.Model.PrimaryKey,
  GBConnection.Data.Model.Field,
  GBConnection.Data.Dao.Interfaces,
  Data.DB,
  System.Classes,
  System.SysUtils,
  System.Variants,
  System.Generics.Collections;

type TGBConnectionDataDaoTableRow = class(TInterfacedObject, IGBConnectionDataDaoTableRow)

  protected
    [Weak]
    FConnection: IGBConnection;
    FQuery     : IGBQuery;

    function DataSetToListTableRow(DataSet: TDataSet; ATable: TGBConnectionDataModelTable): TObjectList<TGBConnectionDataModelTableRow>;
    function DataSetToField       (DataSet: TDataSet; ATable: TGBConnectionDataModelTable): TObjectList<TGBConnectionDataModelField>;

    function obtemChavePrimaria(AQuery: TStrings): TObjectList<TGBConnectionDataModelPrimaryKey>;

    function getTable(TableName: String): TGBConnectionDataModelTable;
    function GetValue(Value: Variant; ADelphiType: TDataModelDelphiType): String;
  public
    function LoadFromTable(TableName: string; Query: TStrings): TObjectList<TGBConnectionDataModelTableRow>;

    constructor create(Connection: IGBConnection);
    class function New(Connection: IGBConnection): IGBConnectionDataDaoTableRow;
end;

implementation

uses
  GBConnection.Data.Dao.Factory;

{ TGBConnectionDataDaoTableRow }

constructor TGBConnectionDataDaoTableRow.create(Connection: IGBConnection);
begin
  FConnection := Connection;
  FQuery      := TGBFactoryConnection.New.createQuery(FConnection);
end;

function TGBConnectionDataDaoTableRow.DataSetToField(DataSet: TDataSet; ATable: TGBConnectionDataModelTable): TObjectList<TGBConnectionDataModelField>;
var
  column : TGBConnectionDataModelTableColumn;
  i      : Integer;
  value  : Variant;
begin
  Result := TObjectList<TGBConnectionDataModelField>.create;
  try
    if DataSet.RecordCount > 0 then
    begin
      for i := 0 to Pred(DataSet.FieldCount) do
      begin
        result.Add( TGBConnectionDataModelField.Create );

        column := ATable.GetColumn(DataSet.Fields[i].FieldName);
        value  := DataSet.Fields[i].Value;

        Result.Last.fieldName  := DataSet.Fields[i].FieldName;
        Result.Last.fieldType  := column.DelphiType.toFieldTypeString;
        Result.Last.required   := column.required;
        Result.Last.fieldValue := GetValue(value, column.DelphiType);
      end;
    end;
  except
    Result.Free;
    raise;
  end;
end;

function TGBConnectionDataDaoTableRow.DataSetToListTableRow(DataSet: TDataSet; ATable: TGBConnectionDataModelTable): TObjectList<TGBConnectionDataModelTableRow>;
begin
  result := TObjectList<TGBConnectionDataModelTableRow>.create;

  DataSet.First;
  while not DataSet.Eof do
  begin
    Result.Add(TGBConnectionDataModelTableRow.Create);
    Result.Last.fields.Free;
    Result.Last.fields := DataSetToField(DataSet, ATable);
    DataSet.Next;
  end;
end;

function TGBConnectionDataDaoTableRow.getTable(TableName: String): TGBConnectionDataModelTable;
begin
  result := TGBConnectionDataDaoFactory.New(FConnection)
              .createDaoTable
                .GetTable(TableName);

  if not Assigned(Result) then
    raise Exception.CreateFmt('Tabela %s n�o encontrada', [TableName]);
end;

function TGBConnectionDataDaoTableRow.GetValue(Value: Variant; ADelphiType: TDataModelDelphiType): String;
var
  data: TDateTime;
begin
  Result := EmptyStr;
  if not VarIsNull(Value) then
  begin
    if ADelphiType in [dtDate, dtDateTime] then
    begin
      data   := Value;
      result := FormatDateTime('yyyy-MM-dd', data) + 'T' + FormatDateTime('hh:mm:ss', data);
    end
    else
      result := VarToStr(Value);
  end;
end;

function TGBConnectionDataDaoTableRow.LoadFromTable(TableName: string; Query: TStrings): TObjectList<TGBConnectionDataModelTableRow>;
var
  LTable: TGBConnectionDataModelTable;
  LPrimaryKey: TObjectList<TGBConnectionDataModelPrimaryKey>;
  LDataSet: TDataSet;
begin
  LTable := getTable(TableName);
  try
    LPrimaryKey := obtemChavePrimaria(Query);
    try
      LDataSet := FQuery.SQL(LTable.GenerateSelect(LPrimaryKey)).Open;
      try
        result := DataSetToListTableRow(LDataSet, LTable);
      finally
        LDataSet.Free;
      end;
    finally
      LPrimaryKey.Free;
    end;
  finally
    LTable.Free;
  end;
end;

class function TGBConnectionDataDaoTableRow.New(Connection: IGBConnection): IGBConnectionDataDaoTableRow;
begin
  result := Self.create(Connection);
end;

function TGBConnectionDataDaoTableRow.obtemChavePrimaria(AQuery: TStrings): TObjectList<TGBConnectionDataModelPrimaryKey>;
var
  i: Integer;
begin
  result := TObjectList<TGBConnectionDataModelPrimaryKey>.create;
  try
    for i := 0 to Pred(AQuery.Count) do
    begin
      result.Add(TGBConnectionDataModelPrimaryKey.Create);
      Result.Last.chave := AQuery.Names[i];
      Result.Last.valor := AQuery.ValueFromIndex[i];
    end;
  except
    on E: Exception do
    begin
      result.Free;
      raise;
    end;
  end;
end;

end.
