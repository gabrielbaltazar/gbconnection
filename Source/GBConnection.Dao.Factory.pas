unit GBConnection.Dao.Factory;

interface

uses
  GBConnection.Dao.Interfaces,
  GBConnection.Dao.ORMBr.DataSet,
  GBConnection.Dao.ORMBr,
  GBConnection.Model.Interfaces;

type TGBFactoryModelDao<T: class, constructor> = class(TInterfacedObject, IGBFactoyModelDao<T>)

  public
    function CreateDaoDataSet(AConn: IGBConnection): IGBModelDaoDataSet<T>;
    function CreateDaoObject (AConn: IGBConnection): IGBModelDao<T>;

    class function New: IGBFactoyModelDao<T>;
end;

implementation

{ TGBFactoryModelDao<T> }

function TGBFactoryModelDao<T>.CreateDaoDataSet(AConn: IGBConnection): IGBModelDaoDataSet<T>;
begin
  Result := TGBConnectionDAOORMBrDataSet<T>.create(AConn);
end;

function TGBFactoryModelDao<T>.CreateDaoObject(AConn: IGBConnection): IGBModelDao<T>;
begin
  Result := TGBModelDaoORMBr<T>.create(AConn);
end;

class function TGBFactoryModelDao<T>.New: IGBFactoyModelDao<T>;
begin
  Result := Self.Create;
end;

end.
