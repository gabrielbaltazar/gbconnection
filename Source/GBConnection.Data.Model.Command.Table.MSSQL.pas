unit GBConnection.Data.Model.Command.Table.MSSQL;

interface

uses
  GBConnection.Data.Model.Command.Interfaces,
  GBConnection.Data.Model.Command.Table.Abstract,
  System.SysUtils,
  System.Classes;

type TGBConnectionDataModelCommandTableMSSQL = class(TGBConnectionDataModelCommandTableAbstract,
                                                      IGBConnectionDataModelCommandTable)

  public
    function TableColumns(TableName: string): string; override;

end;

implementation

{ TGBConnectionDataModelCommandTableMSSQL }

function TGBConnectionDataModelCommandTableMSSQL.TableColumns(TableName: string): string;
begin
  with TStringList.Create do
  begin
    Add('SELECT                                                                                 ');
    Add('  INFORMATION_SCHEMA.COLUMNS.COLUMN_NAME              AS COLUMN_NAME                   ');
    Add(' ,INFORMATION_SCHEMA.COLUMNS.ORDINAL_POSITION         AS column_position               ');
    Add(' ,INFORMATION_SCHEMA.COLUMNS.CHARACTER_MAXIMUM_LENGTH AS column_size                   ');

    Add(' ,CASE                                                                                 ');
    Add('     WHEN INFORMATION_SCHEMA.COLUMNS.DATA_TYPE = ''numeric''                           ');
    Add('     THEN INFORMATION_SCHEMA.COLUMNS.NUMERIC_PRECISION                                 ');
    Add('     ELSE INFORMATION_SCHEMA.COLUMNS.CHARACTER_OCTET_LENGTH                            ');
    Add(' END AS column_precision                                                               ');

    Add(' ,INFORMATION_SCHEMA.COLUMNS.NUMERIC_SCALE      AS column_scale                        ');
    Add(' ,INFORMATION_SCHEMA.COLUMNS.IS_NULLABLE        AS column_nullable                     ');
    Add(' ,INFORMATION_SCHEMA.COLUMNS.DATA_TYPE          AS column_typename                     ');
    Add(' ,INFORMATION_SCHEMA.COLUMNS.CHARACTER_SET_NAME AS column_charset                      ');
    Add(' ,'' '' AS column_default                      ');
    Add(' ,(SELECT COL.COLUMN_NAME                    ');
    Add('   from INFORMATION_SCHEMA.TABLE_CONSTRAINTS TAB                  ');
    Add('   JOIN INFORMATION_SCHEMA.CONSTRAINT_COLUMN_USAGE COL ON TAB.CONSTRAINT_NAME = COL.CONSTRAINT_NAME ');
    Add('   where UPPER( TAB.CONSTRAINT_TYPE ) = ''PRIMARY KEY'' AND UPPER( TAB.TABLE_NAME ) = :TABLE_NAME AND ');
    Add('   upper (COL.COLUMN_NAME) =  UPPER(INFORMATION_SCHEMA.COLUMNS.COLUMN_NAME)) AS PK ');
    Add('FROM                                                                                   ');
    Add('    INFORMATION_SCHEMA.COLUMNS                                                         ');
    Add('WHERE                                                                                  ');
    Add('     UPPER( INFORMATION_SCHEMA.COLUMNS.TABLE_NAME ) = UPPER( :TABLE_NAME )             ');

    Add('ORDER BY INFORMATION_SCHEMA.COLUMNS.ORDINAL_POSITION                                   ');

    Result := Text.Replace(':TABLE_NAME', TableName.QuotedString);
    Free;
  end;
end;

end.

