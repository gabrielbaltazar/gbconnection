unit GBConnection.Data.Model.Command.Factory;

interface

uses
  GBConnection.Data.Model.Command.Interfaces,
  GBConnection.Data.Model.Command.Table.Oracle,
  GBConnection.Data.Model.Command.Table.MSSQL,
  GBConnection.Model.Interfaces;

type TGBConnectionDataModelCommandFactory = class(TInterfacedObject, IGBConnectionDataModelCommandFactory)

  private
    [Weak]
    FConnection: IGBConnection;

  public
    function createCommandTable: IGBConnectionDataModelCommandTable;

    constructor create(Connection: IGBConnection);
    class function New(Connection: IGBConnection): IGBConnectionDataModelCommandFactory;
end;

implementation

{ TGBConnectionDataModelCommandFactory }

constructor TGBConnectionDataModelCommandFactory.create(Connection: IGBConnection);
begin
  FConnection := Connection;
end;

function TGBConnectionDataModelCommandFactory.createCommandTable: IGBConnectionDataModelCommandTable;
begin
  case FConnection.Params.Driver of
    gbOracle : result := TGBConnectionDataModelCommandTableOracle.New(FConnection.Params.Schema);
    gbMSSQL  : result := TGBConnectionDataModelCommandTableMSSQL.New(FConnection.Params.Schema);
  end;
end;

class function TGBConnectionDataModelCommandFactory.New(Connection: IGBConnection): IGBConnectionDataModelCommandFactory;
begin
  result := Self.create(Connection);
end;

end.
