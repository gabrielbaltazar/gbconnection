unit GBConnection.Data.Model.Table;

interface

uses
  System.Generics.Collections,
  GBConnection.Data.Model.TableColumn,
  GBConnection.Data.Model.PrimaryKey,
  System.SysUtils,
  System.StrUtils,
  System.JSON;

type TGBConnectionDataModelTable = class
  private
    Fname: String;
    Fcolumns: TObjectList<TGBConnectionDataModelTableColumn>;

  public
    property name: String read Fname write Fname;
    property columns: TObjectList<TGBConnectionDataModelTableColumn> read Fcolumns write Fcolumns;

    function GetColumn(AColumnName: String): TGBConnectionDataModelTableColumn;
    function ColumnExists(AColumnName: String): Boolean;
    function ColumnCount: Integer;

    function ScriptAddColumn(AColumn: TGBConnectionDataModelTableColumn): String;
    function GenerateSelect(AChavePrimaria: TObjectList<TGBConnectionDataModelPrimaryKey>): string;

    function ToJsonObject: TJSONObject;

    constructor Create;
    destructor Destroy; override;
end;

implementation

{ TGBConnectionDataModelTable }

function TGBConnectionDataModelTable.ColumnCount: Integer;
begin
  Result := 0;
  if Assigned(Fcolumns) then
    Result := Fcolumns.Count;
end;

function TGBConnectionDataModelTable.ColumnExists(AColumnName: String): Boolean;
begin
  Result := GetColumn(AColumnName) <> nil;
end;

constructor TGBConnectionDataModelTable.Create;
begin
  Fcolumns := TObjectList<TGBConnectionDataModelTableColumn>.Create;
end;

destructor TGBConnectionDataModelTable.Destroy;
begin
  Fcolumns.Free;
  inherited;
end;

function TGBConnectionDataModelTable.GenerateSelect(AChavePrimaria: TObjectList<TGBConnectionDataModelPrimaryKey>): string;
var
  I : Integer;
begin
  for I := 0 to Pred(Self.columns.Count) do
    Result := Result + IfThen((I = 0), name + '.' + columns[I].name, ', ' + name + '.' + columns[I].name);

  Result := 'select ' + Result + ' from ' + name;

  for I := 0 to Pred(AChavePrimaria.Count) do
    Result := Result +
                IfThen(I = 0,
                       Format(' where %s = %s', [AChavePrimaria[I].chave, AChavePrimaria[I].valor.QuotedString]),
                       Format(' and %s = %s',   [AChavePrimaria[I].chave, AChavePrimaria[I].valor.QuotedString]));
end;

function TGBConnectionDataModelTable.GetColumn(AColumnName: String): TGBConnectionDataModelTableColumn;
var
  I : Integer;
begin
  Result := nil;
  for I := 0 to Pred(ColumnCount) do
  begin
    if Fcolumns[I].name.ToLower.Equals(AColumnName.ToLower) then
      Exit(FColumns[I]);
  end;
end;

function TGBConnectionDataModelTable.ScriptAddColumn(AColumn: TGBConnectionDataModelTableColumn): String;
var
  LTable: string;
  LColumnName: string;
  LNullable: string;
begin
  Result := 'ALTER TABLE %s ADD %s %s %s';
  LTable := Self.name;
  LColumnName := AColumn.name;
  LNullable := IfThen(AColumn.required, 'NOT NULL', 'NULL');
end;

function TGBConnectionDataModelTable.ToJsonObject: TJSONObject;
var
  LColumns: TJSONArray;
  I: Integer;
begin
  Result := TJSONObject.Create;
  LColumns := TJSONArray.Create;
  try
    Result.AddPair('name', Self.name);
    for I := 0 to Pred(Fcolumns.Count) do
      LColumns.AddElement(Fcolumns[I].ToJsonObject);

    Result.AddPair('columns', LColumns);
  except
    Result.Free;
    LColumns.Free;
  end;
end;

end.

