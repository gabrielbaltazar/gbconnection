unit GBConnection.Data.Model.TableColumn;

interface

uses
  System.SysUtils,
  System.StrUtils,
  System.JSON,
  GBConnection.Data.Model.Types,
  GBConnection.Model.Interfaces;

type TGBConnectionDataModelTableColumn = class
  private
    Fname: String;
    FcolumnType: string;
    Flength: Integer;
    Frequired: Boolean;
    FprimaryKey: Boolean;
    Fprecision: Double;
    Fscale: Double;
    FdefaultValue: String;

    function GetNullableScript: String;
    function GetColumnTypeScript: string;

  public
    property name: String read Fname write Fname;
    property columnType: string read FcolumnType write FcolumnType;
    property length: Integer read Flength write Flength;
    property required: Boolean read Frequired write Frequired;
    property precision: Double read Fprecision write Fprecision;
    property scale: Double read Fscale write Fscale;
    property defaultValue: String read FdefaultValue write FdefaultValue;
    property primaryKey: Boolean read FprimaryKey write FprimaryKey;

    function ScriptAddColumn(ATableName: string): string;
    function DelphiType: TDataModelDelphiType; virtual; abstract;

    function ToJsonObject: TJSONObject;

    class function New(ADriver: TGBDriverConnection): TGBConnectionDataModelTableColumn;
end;

implementation

uses
  GBConnection.Data.Model.TableColumn.Oracle,
  GBConnection.Data.Model.TableColumn.MSSQL;

{ TGBConnectionDataModelTableColumn }

function TGBConnectionDataModelTableColumn.GetColumnTypeScript: string;
begin
  Result := columnType;
  if DelphiType = dtString then
    Exit( Result + '(' + Length.ToString + ')' );

  if DelphiType = dtFloat then
  begin
    if Fprecision > 0 then
    begin
      Result := Result + '(' + FPrecision.ToString;
      if Fscale > 0 then
        Result := Result + ', ' + FScale.ToString;;
      Result := Result + ')';
    end;
  end;
end;

function TGBConnectionDataModelTableColumn.GetNullableScript: String;
begin
  Result := IfThen(Frequired, 'NOT NULL', 'NULL');
end;

class function TGBConnectionDataModelTableColumn.New(ADriver: TGBDriverConnection): TGBConnectionDataModelTableColumn;
begin
  case ADriver of
    gbOracle: Result := TGBConnectionDataModelTableColumnOracle.Create;
    gbMSSQL: Result := TGBConnectionDataModelTableColumnMSSQL.Create;
  else
    raise Exception.CreateFmt('Tipo BD Inv�lido.', []);
  end;
end;

function TGBConnectionDataModelTableColumn.ScriptAddColumn(ATableName: string): string;
begin
  Result := Format( 'ALTER TABLE %s ADD %s %s %s',
              [ATableName,
               Self.name,
               Self.GetColumnTypeScript,
               Self.GetNullableScript,
               Self.defaultValue]);
end;

function TGBConnectionDataModelTableColumn.ToJsonObject: TJSONObject;
begin
  Result := TJSONObject.Create;
  Result.AddPair('name', Self.name)
        .AddPair('columnType', Self.columnType)
        .AddPair('length', TJSONNumber.Create(Self.length))
        .AddPair('precision', TJSONNumber.Create(Self.precision))
        .AddPair('scale', TJSONNumber.Create(Self.scale))
        .AddPair('defaultValue', Self.defaultValue)
        .AddPair('delphiType', Self.DelphiType.toString)
        .AddPair('required', TJSONBool.Create(Self.required))
        .AddPair('primaryKey', TJSONBool.Create(Self.primaryKey));
end;

end.

