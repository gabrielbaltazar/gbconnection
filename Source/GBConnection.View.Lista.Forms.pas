unit GBConnection.View.Lista.Forms;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, GBConnection.View.Lista, Data.DB,
  GBConnection.View.Lista.Interfaces,
  GBConnection.View.Lista.Crud;

type
  TFGBConnectionViewListaForms = class(TFGBConnectionViewLista)
  private

  protected
    function Crud<T: class, constructor>: IGBConnectionViewLista<T>;
    { Private declarations }
  public
    { Public declarations }
  end;

implementation

{$R *.dfm}

{ TFGBConnectionViewListaForms }

function TFGBConnectionViewListaForms.Crud<T>: IGBConnectionViewLista<T>;
begin
  if not Assigned(FCrud) then
    FCrud :=  TGBConnectionViewLista<T>.New(Self);

  result := IGBConnectionViewLista<T>(FCrud);
end;

end.
