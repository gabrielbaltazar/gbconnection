unit GBConnection.View.Cadastro;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  GBConnection.Model.Interfaces,
  GBConnection.Dao.Interfaces,
  GBConnection.Dao.ORMBr.DataSet,
  GBConnection.Controller,
  GBConnection.View.Lookup,
  System.Generics.Collections,
  Data.DB;

type
  TGBDataSetState = (dssEdit, dssInsert);

  TOnFind = reference to procedure(AKeys: TArray<Variant>);

  TFGBConnectionViewCadastro = class(TForm)
    DataSource: TDataSource;
    procedure FormShow (Sender: TObject);
    procedure FormClose(Sender: TObject; var Action: TCloseAction);
  private
    FState      : TGBDataSetState;
    FController : IGBConnectionController;
    FKeys       : TArray<Variant>;
    FLookups    : TObjectDictionary<string, TObject>;

    procedure destroyLookup;
  protected
    FCrud       : TObject;
    FOnGravar   : TNotifyEvent;
    FOnCancelar : TNotifyEvent;
    FOnFind     : TOnFind;

    procedure OnInsert; virtual;
    procedure OnEdit; virtual;

    function Lookup<T: class, constructor>(AName: String): TGBConnectionViewLookup<T>;

    { Private declarations }
  public
    function GetState: TGBDataSetState;
    function GetLookups: TObjectDictionary<string, TObject>;

    procedure SetCrud(Value: TObject);
    procedure SetKeysValues(Value: TArray<Variant>);

    procedure SetController(Value: IGBConnectionController);

    procedure SetOnGravar  (Value: TNotifyEvent);
    procedure SetOnCancelar(Value: TNotifyEvent);
    procedure SetOnFind    (Value: TOnFind);

    function Keys: TArray<Variant>;

    constructor Create(AOwner: TComponent); overload; override;
    constructor Create(AOwner: TComponent; AKeys: TArray<Variant>); reintroduce; overload;
    destructor Destroy; override;
    { Public declarations }
  end;

implementation

{$R *.dfm}

{ TFGBConnectionViewCadastro }

procedure TFGBConnectionViewCadastro.OnEdit;
begin
  FState := dssEdit;
end;

constructor TFGBConnectionViewCadastro.Create(AOwner: TComponent; AKeys: TArray<Variant>);
begin
  create(AOwner);
  FKeys := AKeys;
end;

destructor TFGBConnectionViewCadastro.Destroy;
begin
  FCrud.Free;
  destroyLookup;
  inherited;
end;

procedure TFGBConnectionViewCadastro.destroyLookup;
var
  key: string;
begin
  if Assigned(FLookups) then
  begin
    for key in FLookups.Keys do
      FLookups.Items[Key].Free;

    FLookups.Free;
  end;
end;

function TFGBConnectionViewCadastro.Lookup<T>(AName: String): TGBConnectionViewLookup<T>;
begin
  if not Assigned(FLookups) then
    FLookups := TObjectDictionary<string, TObject>.Create;

  if not FLookups.ContainsKey(AName) then
  begin
    FLookups.Add(AName, TGBConnectionViewLookup<T>.create
                            .Controller(FController)
                            .ParentDataSource(DataSource));
  end;
  result := TGBConnectionViewLookup<T>( FLookups.Items[AName] );
end;

procedure TFGBConnectionViewCadastro.OnInsert;
begin
  DataSource.DataSet.Insert;
  FState := dssInsert;
end;

function TFGBConnectionViewCadastro.Keys: TArray<Variant>;
begin
  result := FKeys;
end;

procedure TFGBConnectionViewCadastro.SetController(Value: IGBConnectionController);
begin
  FController := Value;
end;

procedure TFGBConnectionViewCadastro.SetCrud(Value: TObject);
begin
  FCrud := Value;
end;

procedure TFGBConnectionViewCadastro.SetKeysValues(Value: TArray<Variant>);
begin
  FKeys := Value;
end;

procedure TFGBConnectionViewCadastro.SetOnCancelar(Value: TNotifyEvent);
begin
  FOnCancelar := Value;
end;

procedure TFGBConnectionViewCadastro.SetOnFind(Value: TOnFind);
begin
  FOnFind := Value;
end;

procedure TFGBConnectionViewCadastro.SetOnGravar(Value: TNotifyEvent);
begin
  FOnGravar := Value;
end;

constructor TFGBConnectionViewCadastro.Create(AOwner: TComponent);
begin
  inherited;
end;

procedure TFGBConnectionViewCadastro.FormClose(Sender: TObject; var Action: TCloseAction);
begin
  Action := caFree;
end;

procedure TFGBConnectionViewCadastro.FormShow(Sender: TObject);
begin
  if Length(FKeys) > 0 then
  begin
    FOnFind(FKeys);
    OnEdit;
  end
  else
    OnInsert;
end;

function TFGBConnectionViewCadastro.GetLookups: TObjectDictionary<string, TObject>;
begin
  result := FLookups;
end;

function TFGBConnectionViewCadastro.GetState: TGBDataSetState;
begin
  Result := FState;
end;

end.
