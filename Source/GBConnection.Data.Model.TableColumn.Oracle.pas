unit GBConnection.Data.Model.TableColumn.Oracle;

interface

uses
  GBConnection.Data.Model.Types,
  GBConnection.Data.Model.TableColumn,
  System.SysUtils;

type TGBConnectionDataModelTableColumnOracle = class sealed(TGBConnectionDataModelTableColumn)

  public
    function DelphiType: TDataModelDelphiType; override;

end;

implementation

{ TGBConnectionDataModelTableColumnOracle }

function TGBConnectionDataModelTableColumnOracle.DelphiType: TDataModelDelphiType;
var
  LDbType: string;
begin
  LDbType := Self.ColumnType.ToUpper;

  if LDbType.Equals('BFILE') then Exit(dtString);
  if LDbType.Equals('BINARY_DOUBLE') then Exit(dtFloat);
  if LDbType.Equals('BINARY_FLOAT') then Exit(dtFloat);
  if LDbType.Equals('BLOB') then Exit(dtBlob);
  if LDbType.Equals('BOOLEAN') then Exit(dtBoolean);
  if LDbType.Equals('CHAR') then Exit(dtString);
  if LDbType.Equals('CLOB') then Exit(dtBlob);
  if LDbType.Equals('DATE') then Exit(dtDateTime);
  if LDbType.Equals('FLOAT') then Exit(dtFloat);
  if LDbType.Equals('LONG') then Exit(dtString);
  if LDbType.Equals('LONG RAW') then Exit(dtBlob);
  if LDbType.Equals('NCHAR') then Exit(dtString);
  if LDbType.Equals('NUMBER') then Exit(dtFloat);
  if LDbType.Equals('NVARCHAR') then Exit(dtString);
  if LDbType.Equals('NVARCHAR2') then Exit(dtString);
  if LDbType.Equals('NCLOB') then Exit(dtBlob);
  if LDbType.Equals('RAW') then Exit(dtBlob);
  if LDbType.Equals('VARCHAR2') then Exit(dtString);
  if LDbType.Equals('TIMESTAMP') then Exit(dtDateTime);

  raise Exception.CreateFmt('Tipo %s n�o mapeado para a coluna %s', [LDbType, Name]);
end;

end.

