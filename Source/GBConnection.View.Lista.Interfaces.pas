unit GBConnection.View.Lista.Interfaces;

interface

uses
  GBConnection.Controller,
  GBConnection.Dao.Interfaces,
  GBConnection.View.Cadastro;

type
  TGBViewFormCadastro = class of TFGBConnectionViewCadastro;
  IGBConnectionViewListColumn<T: class, constructor> = interface;

  IGBConnectionViewLista<T: class, constructor> = interface
    ['{318D99AD-EC9A-4C59-890E-4C9ED1808F38}']
    function Open: IGBConnectionViewLista<T>; overload;
    function Controller(Value: IGBConnectionController): IGBConnectionViewLista<T>;
    function DAO(Value: IGBModelDao<T>): IGBConnectionViewLista<T>;
    function Cadastro(Value: TGBViewFormCadastro): IGBConnectionViewLista<T>;
    function Build: IGBConnectionViewLista<T>;

    function Columns: IGBConnectionViewListColumn<T>;
  end;

  IGBConnectionViewColumn<T: class, constructor> = interface
    ['{58002A75-ECD6-4747-A55E-8B7060B5D313}']
    function Caption  (Value: String) : IGBConnectionViewColumn<T>; overload;
    function FieldName(Value: String) : IGBConnectionViewColumn<T>; overload;
    function Width    (Value: Integer): IGBConnectionViewColumn<T>; overload;

    function Caption  : String;  overload;
    function FieldName: String;  overload;
    function Width    : Integer; overload;

    function &End: IGBConnectionViewListColumn<T>;
  end;

  IGBConnectionViewListColumn<T: class, constructor> = interface
    ['{59357901-BE6F-4FF7-8B9C-8FA5202CDC36}']
    function Add    (Value: String) : IGBConnectionViewListColumn<T>;
    function Caption(Value: String) : IGBConnectionViewListColumn<T>;
    function Width  (Value: Integer): IGBConnectionViewListColumn<T>;

    function Count: Integer;
    function Items(Index: Integer): IGBConnectionViewColumn<T>;
    function Contains(Value: String): Boolean;

    function Get(Value: String): IGBConnectionViewColumn<T>;

    function &End: IGBConnectionViewLista<T>;
  end;

implementation

end.
