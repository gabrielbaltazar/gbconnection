unit GBConnection.Criteria.Interfaces;

interface

uses
  GBConnection.Criteria.Types;

type
  IGBConnectionCriteria = interface
    ['{4722DC1A-ED3B-4D5E-8E9E-D2AA3A1B908D}']
    function DateFormat(Value: String): IGBConnectionCriteria;
    function DateTimeFormat(Value: String): IGBConnectionCriteria;

    function Where(const Value: String): IGBConnectionCriteria;
    function &And(const Value: string): IGBConnectionCriteria;
    function &Or(const Value: string): IGBConnectionCriteria;

    function Equal(const Value: String): IGBConnectionCriteria; overload;
    function Equal(const Value: Extended): IGBConnectionCriteria overload;
    function Equal(const Value: Integer): IGBConnectionCriteria; overload;
    function EqualDt(const Value: TDateTime; bUseHour: Boolean = False): IGBConnectionCriteria; overload;

    function NotEqual(const Value: String): IGBConnectionCriteria; overload;
    function NotEqual(const Value: Extended): IGBConnectionCriteria; overload;
    function NotEqual(const Value: Integer): IGBConnectionCriteria; overload;
    function NotEqualDt(const Value: TDateTime; bUseHour: Boolean = False): IGBConnectionCriteria; overload;

    function IsNullIfEmpty: IGBConnectionCriteria;
    function IsNull: IGBConnectionCriteria;
    function IsNotNull: IGBConnectionCriteria;

    function GreaterThan(const Value: Extended): IGBConnectionCriteria; overload;
    function GreaterThan(const Value: Integer): IGBConnectionCriteria; overload;
    function GreaterThan(const Value: String): IGBConnectionCriteria; overload;
    function GreaterThanDt(const Value: TDateTime; bUseHour: Boolean = False): IGBConnectionCriteria;
    function GreaterEqThan(const Value: Extended): IGBConnectionCriteria; overload;
    function GreaterEqThan(const Value: Integer): IGBConnectionCriteria; overload;
    function GreaterEqThan(const Value: String): IGBConnectionCriteria; overload;
    function GreaterEqThanDt(const Value: TDateTime; bUseHour: Boolean = False): IGBConnectionCriteria; overload;

    function LessThan(const Value: Extended): IGBConnectionCriteria; overload;
    function LessThan(const Value: Integer): IGBConnectionCriteria; overload;
    function LessThan(const Value: String): IGBConnectionCriteria; overload;
    function LessThanDt(const Value: TDateTime; bUseHour: Boolean = False): IGBConnectionCriteria; overload;
    function LessEqThan(const Value: Extended): IGBConnectionCriteria; overload;
    function LessEqThan(const Value: Integer): IGBConnectionCriteria; overload;
    function LessEqThan(const Value: String): IGBConnectionCriteria; overload;
    function LessEqThanDt(const Value: TDateTime; bUseHour: Boolean = False): IGBConnectionCriteria; overload;

    function IsIn(const Value: TArray<Double>): IGBConnectionCriteria; overload;
    function IsIn(const Value: TArray<String>): IGBConnectionCriteria; overload;
    function IsIn(const Value: String): IGBConnectionCriteria; overload;

    function IsNotIn(const Value: TArray<Double>): IGBConnectionCriteria; overload;
    function IsNotIn(const Value: TArray<String>): IGBConnectionCriteria; overload;
    function IsNotIn(const Value: String): IGBConnectionCriteria; overload;

    function LikeFull(const Value: String): IGBConnectionCriteria;
    function LikeLeft(const Value: String): IGBConnectionCriteria;
    function LikeRight(const Value: String): IGBConnectionCriteria;
    function NotLikeFull(const Value: String): IGBConnectionCriteria;
    function NotLikeLeft(const Value: String): IGBConnectionCriteria;
    function NotLikeRight(const Value: String): IGBConnectionCriteria;

    function AsString(bClear: boolean = True): string;
    function Count: Integer;
  end;

  IGBConnectionCriteriaOperator = interface
    ['{8EF88808-EDCB-47EA-B20C-FDF7E44B2D1A}']
    function GetColumnName: string;
    function GetCompare: TGBConnectionCriteriaCompare;
    function GetValue: Variant;
    function GetDataType: TGBConnectionCriteriaFieldType;
    function GetIsNullIfEmpty: Boolean;

    procedure SetColumnName(const Value: String);
    procedure SetIsNullIfEmpty(const Value: Boolean);
    procedure SetCompare(const Value: TGBConnectionCriteriaCompare);
    procedure SetValue(const Value: Variant);
    procedure SetDataType(const Value: TGBConnectionCriteriaFieldType);

    property ColumnName: string read GetcolumnName write SetcolumnName;
    property Compare: TGBConnectionCriteriaCompare read Getcompare write Setcompare;
    property Value: Variant read Getvalue write Setvalue;
    property DataType: TGBConnectionCriteriaFieldType read GetdataType write SetdataType;
    property IsNullIfEmpty: Boolean read GetIsNullIfEmpty write SetIsNullIfEmpty;

    function AsString: string;
  end;

  IGBConnectionCriteriaOperators = interface
    ['{7F855D42-FB26-4F21-BCBE-93BC407ED15B}']
    function IsEqual(const AValue: Extended): String; overload;
    function IsEqual(const AValue: Integer): String; overload;
    function IsEqual(const AValue: String): String; overload;

    function IsNotEqual(const AValue: Extended): String; overload;
    function IsNotEqual(const AValue: Integer): String; overload;
    function IsNotEqual(const AValue: String): String; overload;

    function IsGreaterThan(const AValue: Extended): String; overload;
    function IsGreaterThan(const AValue: Integer): String; overload;

    function IsGreaterEqThan(const AValue: Extended): String; overload;
    function IsGreaterEqThan(const AValue: Integer): String; overload;

    function IsLessThan(const AValue: Extended): String; overload;
    function IsLessThan(const AValue: Integer): String; overload;

    function IsLessEqThan(const AValue: Extended): String; overload;
    function IsLessEqThan(const AValue: Integer): String; overload;

    function IsNull: String;
    function IsNotNull: String;

    function IsLikeFull(const AValue: String): String;
    function IsLikeLeft(const AValue: String): String;
    function IsLikeRight(const AValue: String): String;
    function IsNotLikeFull(const AValue: String): String;
    function IsNotLikeLeft(const AValue: String): String;
    function IsNotLikeRight(const AValue: String): String;

    function IsIn(const AValue: TArray<Double>): string; overload;
    function IsIn(const AValue: TArray<String>): string; overload;
    function IsIn(const AValue: String): string; overload;

    function IsNotIn(const AValue: TArray<Double>): string; overload;
    function IsNotIn(const AValue: TArray<String>): string; overload;
    function IsNotIn(const AValue: String): string; overload;

    function IsExists(const AValue: String): string; overload;
    function IsNotExists(const AValue: String): string; overload;
  end;

function CreateCriteria: IGBConnectionCriteria;

implementation

uses
  GBConnection.Criteria;

function CreateCriteria: IGBConnectionCriteria;
begin
  result := TGBConnectionCriteria.New;
end;

end.
