unit GBConnection.View.Cadastro.Crud;

interface

uses
  Data.DB,
  System.SysUtils,
  System.Classes,
  System.Generics.Collections,
  Vcl.Forms,
  GBConnection.View.Cadastro,
  GBConnection.View.Lookup,
  GBConnection.Controller,
  GBConnection.Dao.Interfaces,
  GBConnection.Dao.ORMBr.DataSet;

type TGBConnectionViewCadastro<T: class, constructor> = class

  private
    [Weak]
    FController: IGBConnectionController;

    FormCadastro : TFGBConnectionViewCadastro;
    FORMBrDataSet: IGBModelDaoDataSet<T>;
    FDAO         : IGBModelDao<T>;
    FThis        : T;

    procedure SetEvents;

    procedure LookupSetValues;
    
    procedure OnGravar  (Sender: TObject);
    procedure OnCancelar(Sender: TObject);
    procedure OnFind    (AKeys: TArray<Variant>);

    function Save: TGBConnectionViewCadastro<T>;

  public
    function This: T;

    procedure LookupLocateValues;
    function Open(const Values: array of variant): TGBConnectionViewCadastro<T>; overload;
    function Controller(Value: IGBConnectionController): TGBConnectionViewCadastro<T>;
    function DAO(Value: IGBModelDao<T>): TGBConnectionViewCadastro<T>;
    function Build: TGBConnectionViewCadastro<T>;

    constructor create(AForm: TFGBConnectionViewCadastro); virtual;

end;

implementation

{ TGBConnectionViewCadastro<T> }

function TGBConnectionViewCadastro<T>.Build: TGBConnectionViewCadastro<T>;
begin
  Result        := Self;
  FORMBrDataSet := TGBConnectionDAOORMBrDataSet<T>.create(FController.Connection);
  FORMBrDataSet.DataSource(FormCadastro.DataSource);

  FormCadastro.setController(FController);
  SetEvents;
end;

function TGBConnectionViewCadastro<T>.Controller(Value: IGBConnectionController): TGBConnectionViewCadastro<T>;
begin
  Result      := Self;
  FController := Value;
end;

constructor TGBConnectionViewCadastro<T>.create(AForm: TFGBConnectionViewCadastro);
begin
  FormCadastro := AForm;
  FormCadastro.SetCrud(Self);
end;

function TGBConnectionViewCadastro<T>.DAO(Value: IGBModelDao<T>): TGBConnectionViewCadastro<T>;
begin
  result := Self;
  FDAO   := Value;
end;

procedure TGBConnectionViewCadastro<T>.OnCancelar(Sender: TObject);
begin
  FormCadastro.Close;
end;

procedure TGBConnectionViewCadastro<T>.OnFind(AKeys: TArray<Variant>);
begin
  FORMBrDataSet.Open(AKeys);
  FDAO.modify(FORMBrDataSet.This);

  Self.FormCadastro.DataSource.DataSet.Edit;
  LookupLocateValues;
end;

procedure TGBConnectionViewCadastro<T>.OnGravar(Sender: TObject);
begin
  Self.Save;
end;

function TGBConnectionViewCadastro<T>.Open(const Values: array of variant): TGBConnectionViewCadastro<T>;
begin
  result := Self;
  FORMBrDataSet.Open(Values);
end;

function TGBConnectionViewCadastro<T>.Save: TGBConnectionViewCadastro<T>;
var
  model: T;
begin
  result := Self;
  try
    FORMBrDataSet.Save;
    LookupSetValues;
    model := This;

    case FormCadastro.GetState of
      dssEdit   : FDAO.update(model);
      dssInsert : FDAO.insert(model);
    end;

    FormCadastro.SetKeysValues(FORMBrDataSet.KeyValues);
  finally
    FThis := nil;
    FORMBrDataSet.NewThis(nil);
  end;
end;

procedure TGBConnectionViewCadastro<T>.SetEvents;
begin
  FormCadastro.SetOnGravar(OnGravar);
  FormCadastro.SetOnCancelar(OnCancelar);
  FormCadastro.SetOnFind(OnFind);
end;

procedure TGBConnectionViewCadastro<T>.LookupLocateValues;
var
  lookups: TObjectDictionary<String, TObject>;
  model  : T;
  key    : string;
  look   : TObject;
begin
  try
    lookups := FormCadastro.GetLookups;
    if not Assigned(lookups) then
      Exit;

    model := This;

    for key in lookups.Keys do
    begin
      look := lookups.Items[key];
      TGBConnectionViewLookup<TObject>(look).LocateValues(model);
    end;
  finally
    FThis := nil;
  end;
end;

procedure TGBConnectionViewCadastro<T>.LookupSetValues;
var
  lookups: TObjectDictionary<String, TObject>;
  model  : T;
  key    : string;
  look   : TObject;
begin
  lookups := FormCadastro.GetLookups;
  if not Assigned(lookups) then
    Exit;
      
  model := This;

  for key in lookups.Keys do
  begin
    look := (lookups.Items[key]);
    TGBConnectionViewLookup<TObject>(look).SetValues<T>(model);
  end;
end;

function TGBConnectionViewCadastro<T>.This: T;
begin
  if not Assigned(FThis) then
    FThis := FORMBrDataSet.This;
  result := FThis;
end;

end.
