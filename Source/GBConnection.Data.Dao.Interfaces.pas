unit GBConnection.Data.Dao.Interfaces;

interface

uses
  GBConnection.Data.Model.Field,
  GBConnection.Data.Model.Table,
  GBConnection.Data.Model.TableRow,
  System.Classes,
  System.SysUtils,
  System.Generics.Collections;

type
  IGBConnectionDataDaoTable = interface
    ['{B46377A0-9567-4F83-8720-8202BEA7BB81}']
    function GetTable(TableName: String): TGBConnectionDataModelTable;
  end;

  IGBConnectionDataDaoTableRow = interface
    ['{E9643C51-BB9B-429E-A5BE-70ADCA38C3DD}']
    function LoadFromTable(TableName: String; Query: TStrings): TObjectList<TGBConnectionDataModelTableRow>;
  end;

  IGBConnectionDataDaoFactory = interface
    ['{108B5069-AD75-4059-9C9A-19C6F5B86380}']
    function createDaoTable   : IGBConnectionDataDaoTable;
    function createDaoTableRow: IGBConnectionDataDaoTableRow;
  end;

implementation

end.
