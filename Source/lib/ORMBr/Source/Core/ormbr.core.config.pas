unit ormbr.core.config;

interface

uses
  System.SysUtils; //

type
  TORMBrConfig = class
  private
    class var FInstance: TORMBrConfig;

    FUseModifiedFields: Boolean;
    FUseBooleanAsInteger: Boolean;

    constructor createPrivate;

  protected
    constructor Create;

  public
    function UseModifiedFields(AValue: Boolean): TORMBrConfig; overload;
    function UseModifiedFields: Boolean; overload;

    function UseBooleanAsInteger(AValue: Boolean): TORMBrConfig; overload;
    function UseBooleanAsInteger: Boolean; overload;

    class function Instance: TORMBrConfig;
    class destructor UnInitialize;
  end;

implementation

{ TORMBrConfig }

constructor TORMBrConfig.Create;
begin
  raise Exception.CreateFmt('Use GetInstance...', []);
end;

constructor TORMBrConfig.createPrivate;
begin
  FUseModifiedFields := True;
  FUseBooleanAsInteger := True;
end;

class function TORMBrConfig.Instance: TORMBrConfig;
begin
  if not Assigned(FInstance) then
    FInstance := createPrivate;
  result := FInstance;
end;

function TORMBrConfig.UseBooleanAsInteger(AValue: Boolean): TORMBrConfig;
begin
  result := Self;
  FUseBooleanAsInteger := AValue;
end;

class destructor TORMBrConfig.UnInitialize;
begin
  if Assigned(FInstance) then
    FreeAndNil(FInstance);
end;

function TORMBrConfig.UseBooleanAsInteger: Boolean;
begin
  result := FUseBooleanAsInteger;
end;

function TORMBrConfig.UseModifiedFields(AValue: Boolean): TORMBrConfig;
begin
  result := Self;
  FUseModifiedFields := AValue;
end;

function TORMBrConfig.UseModifiedFields: Boolean;
begin
  result := FUseModifiedFields;
end;

end.
