unit GBConnection.Data.Dao.Table;

interface

uses
  GBConnection.Model.Interfaces,
  GBConnection.Model.Factory,
  GBConnection.Data.Model.Table,
  GBConnection.Data.Dao.Interfaces,
  GBConnection.Data.Model.TableColumn,
  GBConnection.Data.Model.Command.Factory,
  Data.DB,
  System.SysUtils;

type TGBConnectionDataDaoTable = class(TInterfacedObject, IGBConnectionDataDaoTable)

  protected
    [Weak]
    FConnection: IGBConnection;
    FQuery     : IGBQuery;

    function  DataSetToColumn (ADataSet: TDataSet): TGBConnectionDataModelTableColumn;
    procedure AddTableColumn  (ADataSet: TDataSet; ATable: TGBConnectionDataModelTable);
  public
    function GetTable(TableName: string): TGBConnectionDataModelTable;

    constructor create(Connection: IGBConnection);
    class function New(Connection: IGBConnection): IGBConnectionDataDaoTable;
end;

implementation

{ TGBConnectionDataDaoTable }

procedure TGBConnectionDataDaoTable.AddTableColumn(ADataSet: TDataSet; ATable: TGBConnectionDataModelTable);
begin
  ADataSet.First;
  while not ADataSet.Eof do
  begin
    ATable.columns.Add(DataSetToColumn(ADataSet));
    ADataSet.Next;
  end;
end;

constructor TGBConnectionDataDaoTable.create(Connection: IGBConnection);
begin
  FConnection := Connection;
  FQuery      := TGBFactoryConnection.New.createQuery(FConnection);
end;

function TGBConnectionDataDaoTable.DataSetToColumn(ADataSet: TDataSet): TGBConnectionDataModelTableColumn;
begin
  result := TGBConnectionDataModelTableColumn.New(FConnection.Params.Driver);
  result.Name         := ADataSet.FieldByName('column_name').AsString;
  result.ColumnType   := ADataSet.FieldByName('column_typename').AsString;
  result.Length       := ADataSet.FieldByName('column_size').AsInteger;
  result.Required     := ADataSet.FieldByName('column_nullable').AsString = 'N';
  result.Precision    := ADataSet.FieldByName('column_precision').AsFloat;
  result.Scale        := ADataSet.FieldByName('column_scale').AsFloat;
  result.DefaultValue := ADataSet.FieldByName('column_default').AsString;
  Result.primaryKey   := not ADataSet.FieldByName('PK').AsString.IsEmpty;
end;

function TGBConnectionDataDaoTable.GetTable(TableName: string): TGBConnectionDataModelTable;
var
  LSql : string;
  LDataSet: TDataSet;
begin
  result := nil;
  LSql := TGBConnectionDataModelCommandFactory.New(FConnection)
            .createCommandTable
              .TableColumns(TableName);

  LDataSet := FQuery.SQL(LSql).Open;
  try
    if LDataSet.RecordCount > 0 then
    begin
      result := TGBConnectionDataModelTable.Create;
      Result.name := TableName;

      AddTableColumn(LDataSet, Result);
    end;
  finally
    LDataSet.Open;
  end;
end;

class function TGBConnectionDataDaoTable.New(Connection: IGBConnection): IGBConnectionDataDaoTable;
begin
  result := Self.create(Connection);
end;

end.
