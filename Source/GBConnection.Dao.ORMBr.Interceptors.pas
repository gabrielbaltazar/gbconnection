unit GBConnection.Dao.ORMBr.Interceptors;

interface

uses
  System.Rtti,
  System.Classes,
  System.SysUtils,
  System.TypInfo,
  GBConnection.Dao.Interfaces,
  GBConnection.Dao.Types,
  ormbr.mapping.rttiutils;

type TGBConnectionDaoInterceptors<T: class, constructor> = class

  private
    [Weak]
    FDAO: IGBModelDAO<T>;
    FVMI: TVirtualMethodInterceptor;

    procedure SetupVMI;
    function IsTransactionMethod(Method: TRttiMethod): Boolean;

    procedure OnBeforeTransaction(Method: TRttiMethod);
    procedure OnAfterTransaction(Method: TRttiMethod);
    procedure OnExceptionTransaction(Method: TRttiMethod);

    procedure OnBefore(Instance: TObject;
                       Method: TRttiMethod;
                       const Args: TArray<TValue>;
                       out DoInvoke: Boolean;
                       out Result: TValue);

    procedure OnAfter (Instance: TObject;
                       Method: TRttiMethod;
                       const Args: TArray<TValue>;
                       var Result: TValue);

    procedure OnException(Instance: TObject;
                          Method: TRttiMethod;
                          const Args: TArray<TValue>;
                          out RaiseException: Boolean;
                          TheException: Exception;
                          out Result: TValue );
  public
    constructor Create(Dao: IGBModelDAO<T>);
    destructor Destroy; override;

end;

implementation

{ TGBConnectionDaoInterceptors<T> }

constructor TGBConnectionDaoInterceptors<T>.Create(Dao: IGBModelDAO<T>);
begin
  FDAO := Dao;
  FVMI := TVirtualMethodInterceptor.Create(TObject(FDAO).ClassType);

  SetupVMI;
end;

destructor TGBConnectionDaoInterceptors<T>.Destroy;
begin
  FVMI.Unproxify(TObject( FDAO ));
  FVMI.Free;
  inherited;
end;

function TGBConnectionDaoInterceptors<T>.IsTransactionMethod(Method: TRttiMethod): Boolean;
var
  LAttribute: TCustomAttribute;
begin
  Result := False;
  for LAttribute in Method.GetAttributes do
  begin
    if LAttribute.ClassNameIs(GBDAOTransaction.ClassName) then
      Exit(True);
  end;
end;

procedure TGBConnectionDaoInterceptors<T>.OnAfter(Instance: TObject;
                                                  Method: TRttiMethod;
                                                  const Args: TArray<TValue>;
                                                  var Result: TValue);
begin
  OnAfterTransaction(Method);
end;

procedure TGBConnectionDaoInterceptors<T>.OnAfterTransaction(Method: TRttiMethod);
begin
  if (IsTransactionMethod(Method)) and (FDAO.ManagerTransaction) then
    FDAO.Commit;
end;

procedure TGBConnectionDaoInterceptors<T>.OnBefore(Instance: TObject;
                                                   Method: TRttiMethod;
                                                   const Args: TArray<TValue>;
                                                   out DoInvoke: Boolean;
                                                   out Result: TValue);
begin
  OnBeforeTransaction(Method);
end;

procedure TGBConnectionDaoInterceptors<T>.OnBeforeTransaction(Method: TRttiMethod);
begin
  if (IsTransactionMethod(Method)) and (FDAO.ManagerTransaction) then
    FDAO.StartTransaction;
end;

procedure TGBConnectionDaoInterceptors<T>.OnException(Instance: TObject;
                                                      Method: TRttiMethod;
                                                      const Args: TArray<TValue>;
                                                      out RaiseException: Boolean;
                                                      TheException: Exception;
                                                      out Result: TValue);
begin
  OnExceptionTransaction(Method);
end;

procedure TGBConnectionDaoInterceptors<T>.OnExceptionTransaction(Method: TRttiMethod);
begin
  if (IsTransactionMethod(Method)) and (FDAO.ManagerTransaction) then
    FDAO.Rollback;
end;

procedure TGBConnectionDaoInterceptors<T>.SetupVMI;
begin
  FVMI.OnBefore := OnBefore;
  FVMI.OnAfter := OnAfter;
  FVMI.OnException := OnException;

  FVMI.Proxify( TObject(FDAO));
end;

end.
