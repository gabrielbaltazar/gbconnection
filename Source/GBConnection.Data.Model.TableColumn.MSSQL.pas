unit GBConnection.Data.Model.TableColumn.MSSQL;

interface

uses
  GBConnection.Data.Model.Types,
  GBConnection.Data.Model.TableColumn,
  System.SysUtils;

type TGBConnectionDataModelTableColumnMSSQL = class(TGBConnectionDataModelTableColumn)

  public
    function DelphiType: TDataModelDelphiType; override;
end;

implementation

{ TGBConnectionDataModelTableColumnMSSQL }

function TGBConnectionDataModelTableColumnMSSQL.DelphiType: TDataModelDelphiType;
var
  LDbType: string;
begin
  LDbType := Self.ColumnType.ToUpper;

  if LDbType.Equals('BIGINT') then Exit(dtFloat);
  if LDbType.Equals('BINARY') then Exit(dtInteger);
  if LDbType.Equals('CARACTER') then Exit(dtString);
  if LDbType.Equals('CHAR') then Exit(dtString);
  if LDbType.Equals('DATE') then Exit(dtDateTime);
  if LDbType.Equals('DATETIME') then Exit(dtDateTime);
  if LDbType.Equals('DATETIME2') then Exit(dtDateTime);
  if LDbType.Equals('DECIMAL') then Exit(dtFloat);
  if LDbType.Equals('DOUBLE PRECISION') then Exit(dtFloat);
  if LDbType.Equals('FLOAT') then Exit(dtFloat);
  if LDbType.Equals('GUID') then Exit(dtString);
  if LDbType.Equals('IMAGE') then Exit(dtBlob);
  if LDbType.Equals('INT') then Exit(dtInteger);
  if LDbType.Equals('MONEY') then Exit(dtFloat);
  if LDbType.Equals('NCHAR') then Exit(dtString);
  if LDbType.Equals('NUMERIC') then Exit(dtFloat);
  if LDbType.Equals('NVARCHAR') then Exit(dtString);
  if LDbType.Equals('REAL') then Exit(dtFloat);
  if LDbType.Equals('SMALLINT') then Exit(dtInteger);
  if LDbType.Equals('TEXT') then Exit(dtString);
  if LDbType.Equals('TIMESTAMP') then Exit(dtDateTime);
  if LDbType.Equals('TEXT') then Exit(dtString);
  if LDbType.Equals('TEXT') then Exit(dtString);
  if LDbType.Equals('VARCHAR') then Exit(dtString);

  raise Exception.CreateFmt('Tipo %s n�o mapeado para a coluna %s', [LDbType, Name]);
end;

end.

