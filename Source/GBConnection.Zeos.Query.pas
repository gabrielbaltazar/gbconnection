unit GBConnection.Zeos.Query;

interface

uses
  GBConnection.Model.Interfaces,
  System.SysUtils,
  System.Classes,
  Data.DB,
  Datasnap.DBClient,
  ZDataset,
  ZConnection;

type TGBConnectionQueryZeos = class(TInterfacedObject, IGBQuery)

  private
    [WeakAttribute]
    FConnection: IGBConnection;
    FQuery: TZQuery;

    procedure ShowMonitor(SQL: String);
  public
    function SQL(Value: String): IGBQuery; overload;
    function SQL(Value: string; const Args: array of const): IGBQuery; overload;

    function ParamAsInteger(Name: String; Value: Integer): IGBQuery;
    function ParamAsCurrency(Name: String; Value: Currency): IGBQuery;
    function ParamAsFloat(Name: String; Value: Double): IGBQuery;
    function ParamAsString(Name: String; Value: String): IGBQuery;
    function ParamAsDateTime(Name: String; Value: TDateTime): IGBQuery;
    function ParamAsDate(Name: String; Value: TDateTime): IGBQuery;
    function ParamAsTime(Name: String; Value: TDateTime): IGBQuery;
    function ParamAsBoolean(Name: String; Value: Boolean): IGBQuery;

    function Open: TDataSet;
    function ExecSQL: IGBQuery;
    function ExecSQLAndCommit: IGBQuery;

    constructor Create(AConnection: IGBConnection);
    class function New(AConnection: IGBConnection): IGBQuery;
    destructor Destroy; override;

end;

implementation

{ TGBConnectionQueryZeos }

constructor TGBConnectionQueryZeos.Create(AConnection: IGBConnection);
begin
  FConnection := AConnection;
  FQuery := TZQuery.Create(nil);
  FQuery.Connection := TZConnection(FConnection.Component);
end;

destructor TGBConnectionQueryZeos.Destroy;
begin
  FQuery.Free;
  inherited;
end;

function TGBConnectionQueryZeos.ExecSQL: IGBQuery;
begin
  Result := Self;
  try
    ShowMonitor(FQuery.SQL.Text);
    FQuery.ExecSQL;
  finally
    FQuery.SQL.Clear;
  end;
end;

function TGBConnectionQueryZeos.ExecSQLAndCommit: IGBQuery;
begin
  Result := Self;
  FConnection.StartTransaction;
  try
    try
      ShowMonitor(FQuery.SQL.Text);
      FQuery.ExecSQL;
    finally
      FQuery.SQL.Clear;
    end;
    FConnection.Commit;
  except
    FConnection.Rollback;
    raise;
  end;
end;

class function TGBConnectionQueryZeos.New(AConnection: IGBConnection): IGBQuery;
begin
  Result := Self.create(AConnection);
end;

function TGBConnectionQueryZeos.Open: TDataSet;
var
  LQuery: TZQuery;
  I: Integer;
begin
  LQuery := TZQuery.Create(nil);
  try
    try
      ShowMonitor(FQuery.SQL.Text);
      LQuery.Connection := TZConnection(FConnection.Component);
      LQuery.SQL.Text := FQuery.SQL.Text;
      for I := 0 to Pred(FQuery.Params.Count) do
        LQuery.ParamByName(FQuery.Params[I].Name).Value := FQuery.Params[I].Value;

      LQuery.Open;
      Result := LQuery;
    finally
      FQuery.SQL.Clear;
    end;
  except
    LQuery.Free;
    raise;
  end;
end;

function TGBConnectionQueryZeos.ParamAsBoolean(Name: String; Value: Boolean): IGBQuery;
begin
  Result := Self;
  FQuery.ParamByName(Name).AsBoolean := Value;
end;

function TGBConnectionQueryZeos.ParamAsCurrency(Name: String; Value: Currency): IGBQuery;
begin
  Result := Self;
  FQuery.ParamByName(Name).AsCurrency := Value;
end;

function TGBConnectionQueryZeos.ParamAsDate(Name: String; Value: TDateTime): IGBQuery;
begin
  Result := Self;
  FQuery.ParamByName(Name).AsDate := Value;
end;

function TGBConnectionQueryZeos.ParamAsDateTime(Name: String; Value: TDateTime): IGBQuery;
begin
  Result := Self;
  FQuery.ParamByName(Name).AsDateTime := Value;
end;

function TGBConnectionQueryZeos.ParamAsFloat(Name: String; Value: Double): IGBQuery;
begin
  Result := Self;
  FQuery.ParamByName(Name).AsFloat := Value;
end;

function TGBConnectionQueryZeos.ParamAsInteger(Name: String; Value: Integer): IGBQuery;
begin
  Result := Self;
  FQuery.ParamByName(Name).AsInteger := Value;
end;

function TGBConnectionQueryZeos.ParamAsString(Name, Value: String): IGBQuery;
begin
  Result := Self;
  FQuery.ParamByName(Name).AsString := Value;
end;

function TGBConnectionQueryZeos.ParamAsTime(Name: String; Value: TDateTime): IGBQuery;
begin
  Result := Self;
  FQuery.ParamByName(Name).AsTime := Value;
end;

function TGBConnectionQueryZeos.SQL(Value: String): IGBQuery;
begin
  Result := Self;
  FQuery.SQL.Add(Value);
end;

procedure TGBConnectionQueryZeos.ShowMonitor(SQL: String);
begin
  if Assigned(FConnection.Monitor) then
    FConnection.Monitor.Command(SQL, nil);
end;

function TGBConnectionQueryZeos.SQL(Value: string; const Args: array of const): IGBQuery;
begin
  Result := Self;
  SQL(Format(Value, Args));
end;

end.
