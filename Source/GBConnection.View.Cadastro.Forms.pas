unit GBConnection.View.Cadastro.Forms;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, GBConnection.View.Cadastro, Data.DB,
  GBConnection.View.Cadastro.Crud;

type
  TFGBConnectionViewCadastroForm = class(TFGBConnectionViewCadastro)
  private
    { Private declarations }
  protected
    function Field(Name: String): TField;

    function Crud<T: class, constructor>: TGBConnectionViewCadastro<T>;
    function Model<T: class, constructor>: T;
    { Public declarations }
  end;

implementation

{$R *.dfm}

{ TFGBConnectionViewCadastroForm }

function TFGBConnectionViewCadastroForm.Crud<T>: TGBConnectionViewCadastro<T>;
begin
  if not Assigned(FCrud) then
    FCrud := TGBConnectionViewCadastro<T>.create(Self);

  result := TGBConnectionViewCadastro<T>(FCrud);
end;

function TFGBConnectionViewCadastroForm.Field(Name: String): TField;
begin
  result := DataSource.DataSet.FieldByName(Name);

  if not Assigned(Result) then
    raise EResNotFound.CreateFmt('Field % not Found', [Name]);
end;

function TFGBConnectionViewCadastroForm.Model<T>: T;
begin
  result := Crud<T>.This;
end;

end.
