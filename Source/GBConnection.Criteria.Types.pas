unit GBConnection.Criteria.Types;

interface

type
//  TGBConnectionCriteriaOperator  = (opeAnd, opeOr);
  TGBConnectionCriteriaFieldType = (dftUnknown, dftString, dftInteger, dftFloat, dftDate, dftArray, dftText);

  TGBConnectionCriteriaCompare  = ( fcEqual, fcNotEqual,
                                    fcGreater, fcGreaterEqual,
                                    fcLess, fcLessEqual,
                                    fcIn, fcNotIn,
                                    fcIsNull, fcIsNotNull,
                                    fcBetween, fcNotBetween,
                                    fcExists, fcNotExists,
                                    fcLikeFull, fcLikeLeft, fcLikeRight,
                                    fcNotLikeFull, fcNotLikeLeft, fcNotLikeRight);

implementation

end.
