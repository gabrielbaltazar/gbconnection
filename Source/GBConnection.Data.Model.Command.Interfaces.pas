unit GBConnection.Data.Model.Command.Interfaces;

interface

type
  IGBConnectionDataModelCommandTable = interface
    ['{56EA6908-89DF-41BA-83C1-DB59D8C04FC9}']
    function TableColumns(TableName: string): string;
  end;

  IGBConnectionDataModelCommandFactory = interface
    ['{78B80124-FE5B-4ADD-BA9C-902C55BF6A92}']
    function createCommandTable: IGBConnectionDataModelCommandTable;
  end;

implementation

end.
