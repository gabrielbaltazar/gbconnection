unit GBConnection.Model.Types;

interface

uses
  ormbr.types.mapping,
  ormbr.mapping.attributes,
  ormbr.mapping.classes,
  ormbr.mapping.register;

type
  TRegisterClass = ormbr.mapping.register.TRegisterClass;

  Table = ormbr.mapping.attributes.Table;
  PrimaryKey = ormbr.mapping.attributes.PrimaryKey;
  Sequence = ormbr.mapping.attributes.Sequence;
  Column = ormbr.mapping.attributes.Column;
  NotEmpty = ormbr.mapping.attributes.NotEmpty;
  NullIfEmpty = ormbr.mapping.attributes.NullIfEmpty;
  Size = ormbr.mapping.attributes.Size;
  Association = ormbr.mapping.attributes.Association;
  UniqueCol = ormbr.mapping.attributes.UniqueCol;

  TSequenceType = ormbr.types.mapping.TSequenceType;
  TMultiplicity = ormbr.types.mapping.TMultiplicity;
  TRestriction = ormbr.types.mapping.TRestriction;
  TRestrictions = ormbr.types.mapping.TRestrictions;
  TJoin = ormbr.types.mapping.TJoin;

implementation

end.
