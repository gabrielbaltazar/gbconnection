unit GBConnection.Monitor;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.ExtCtrls, Vcl.Menus,
  Vcl.StdCtrls,
  System.typinfo,
  GBConnection.Model.Interfaces,
  ormbr.factory.interfaces,
  Data.DB;

type
  TGBConnectionMonitor = class(TForm)
    mmoMonitor: TMemo;
    pmLog: TPopupMenu;
    Limpar1: TMenuItem;
    pnlTop: TPanel;
    lbl1: TLabel;
    lblClose: TLabel;
    procedure pnlTopMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Integer);
    procedure Limpar1Click(Sender: TObject);
  private
//    class var
//      FInstance: TGBConnectionMonitor;

//    procedure SendMessage(AMessage: String);
//    procedure Command(const ASQL: string; AParams: TParams);
  public
//    class function GetInstance: IGBMonitorConnection;

    constructor create; reintroduce;
    destructor Destroy; override;
    { Public declarations }
  end;

  type TGBConnectionMonitorImpl = class(TInterfacedObject, IGBMonitorConnection, ICommandMonitor)
    private
      Form: TGBConnectionMonitor;

      procedure SendMessage(AMessage: String);
      procedure Command(const ASQL: string; AParams: TParams);
      procedure Show;
    public
      constructor create;
      destructor  Destroy; override;
  end;

implementation

{$R *.dfm}

{ TGBConnectionMonitor }

constructor TGBConnectionMonitor.create;
begin
  inherited create(nil);
end;

destructor TGBConnectionMonitor.Destroy;
begin

  inherited;
end;

procedure TGBConnectionMonitor.Limpar1Click(Sender: TObject);
begin
  mmoMonitor.Lines.Clear;
end;

procedure TGBConnectionMonitor.pnlTopMouseDown(Sender: TObject;
  Button: TMouseButton; Shift: TShiftState; X, Y: Integer);
const
   sc_DragMove = $f012;
begin
  ReleaseCapture;
  Perform(wm_SysCommand, sc_DragMove, 0);
end;

{ TGBConnectionMonitorImpl }

procedure TGBConnectionMonitorImpl.Command(const ASQL: string; AParams: TParams);
var
  iFor       : Integer;
  AsValue    : string;
  textParams : string;
begin
  //SendMessage('');
  SendMessage(ASQL);
  if AParams <> nil then
  begin
    textParams := EmptyStr;
    for iFor := 0 to AParams.Count -1 do
    begin
      if AParams.Items[iFor].Value = System.Variants.Null then
        AsValue := 'NULL'
      else
      if AParams.Items[iFor].DataType = ftDateTime then
        AsValue := '"' + DateTimeToStr(AParams.Items[iFor].Value) + '"'
      else
      if AParams.Items[iFor].DataType = ftDate then
        AsValue := '"' + DateToStr(AParams.Items[iFor].Value) + '"'
      else
        AsValue := '"' + VarToStr(AParams.Items[iFor].Value) + '"';

      textParams := textParams + AParams.Items[iFor].Name + ' = ' + AsValue + ' (' +
                    GetEnumName(TypeInfo(TFieldType), Ord(AParams.Items[iFor].DataType)) + ')' + sLineBreak;
    end;
    SendMessage(textParams);
  end;
end;

constructor TGBConnectionMonitorImpl.create;
begin
  Form := TGBConnectionMonitor.create;
end;

destructor TGBConnectionMonitorImpl.Destroy;
begin
  Form.Free;
  inherited;
end;

procedure TGBConnectionMonitorImpl.SendMessage(AMessage: String);
begin
  TThread.Synchronize(TThread.CurrentThread,
    procedure
    begin
      if not AMessage.Trim.IsEmpty then
        Form.mmoMonitor.Lines.Add('-- ' + FormatDateTime('dd/MM/yyyy hh:mm:ss', now));
      Form.mmoMonitor.Lines.Add(AMessage);
    end
    );

end;

procedure TGBConnectionMonitorImpl.Show;
begin
  Form.Show;
end;

end.
