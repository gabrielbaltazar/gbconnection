unit GBConnection.Dao.ORMBr.Connection;

interface

uses
  GBConnection.Model.Interfaces,
  GBConnection.Dao.Interfaces,
  GBConnection.Dao.ORMBr.Factory,
  ormbr.factory.interfaces,
  ormbr.mapping.classes,
  ormbr.mapping.explorer,
  System.SysUtils;

type TGBConnectionDaoORMBrConnection<T: class, constructor> = class(TInterfacedObject, IGBDAOConnection)

  protected
    FConnection: IGBConnection;
    FPrimaryKey: TPrimaryKeyColumnsMapping;
    FSequenceMapping: TSequenceMapping;
    FTableMapping: TTableMapping;
    FColumns: TColumnMappingList;

    FORMBrConnection: IDBConnection;
    FManagerTransaction: Boolean;

    function MappingPrimaryKey: TPrimaryKeyColumnsMapping;
    function MappingSequence: TSequenceMapping;
    function MappingTable: TTableMapping;
    function GetColumns: TColumnMappingList;
    function GetColumn(AColumnName: string): TColumnMapping;
    function GetPrimaryKey: TArray<String>;

  public
    procedure StartTransaction;
    procedure Commit;
    procedure Rollback;

    procedure ManagerTransaction(Value: Boolean); overload;
    function ManagerTransaction: Boolean; overload;

    constructor Create(AConnection: IGBConnection); virtual;
    destructor Destroy; override;
end;

implementation

{ TGBConnectionDaoORMBrConnection }

procedure TGBConnectionDaoORMBrConnection<T>.Commit;
begin
  if (ManagerTransaction) and (FConnection.InTransaction) then
    FConnection.Commit;
end;

constructor TGBConnectionDaoORMBrConnection<T>.Create(AConnection: IGBConnection);
begin
  FManagerTransaction := True;
  FConnection := AConnection;
  FORMBrConnection := TGBConnectionDaoORMBrFactory.New.createConnectionORMBr.GetConnection(FConnection);
  FPrimaryKey := TMappingExplorer.GetInstance.GetMappingPrimaryKeyColumns(T);
  FSequenceMapping := TMappingExplorer.GetInstance.GetMappingSequence(T);
end;

destructor TGBConnectionDaoORMBrConnection<T>.Destroy;
begin

  inherited;
end;

function TGBConnectionDaoORMBrConnection<T>.GetColumn(AColumnName: string): TColumnMapping;
var
  I: Integer;
begin
  Result := nil;
  for I := 0 to Pred(GetColumns.Count) do
  begin
    if AColumnName.ToLower = GetColumns[I].ColumnName.ToLower then
      Exit(GetColumns[I]);
  end;
end;

function TGBConnectionDaoORMBrConnection<T>.GetColumns: TColumnMappingList;
begin
  if not Assigned(FColumns) then
    FColumns := TMappingExplorer.GetInstance.GetMappingColumn(T);
  Result := FColumns;
end;

function TGBConnectionDaoORMBrConnection<T>.GetPrimaryKey: TArray<String>;
var
  I: Integer;
begin
  SetLength(Result, MappingPrimaryKey.Columns.Count);
  for I := 0 to Pred(Length(Result)) do
    Result[I] := MappingPrimaryKey.Columns[I].ColumnName;
end;

procedure TGBConnectionDaoORMBrConnection<T>.ManagerTransaction(Value: Boolean);
begin
  FManagerTransaction := Value;
end;

function TGBConnectionDaoORMBrConnection<T>.ManagerTransaction: Boolean;
begin
  Result := FManagerTransaction;
end;

function TGBConnectionDaoORMBrConnection<T>.MappingPrimaryKey: TPrimaryKeyColumnsMapping;
begin
  if not Assigned(FPrimaryKey) then
    FPrimaryKey := TMappingExplorer.GetInstance.GetMappingPrimaryKeyColumns(T);
  Result := FPrimaryKey;
end;

function TGBConnectionDaoORMBrConnection<T>.MappingSequence: TSequenceMapping;
begin
  if not Assigned(FSequenceMapping) then
    FSequenceMapping := TMappingExplorer.GetInstance.GetMappingSequence(T);
  Result := FSequenceMapping;
end;

function TGBConnectionDaoORMBrConnection<T>.MappingTable: TTableMapping;
begin
  if not Assigned(FTableMapping) then
    FTableMapping := TMappingExplorer.GetInstance.GetMappingTable(T);
  Result := FTableMapping;
end;

procedure TGBConnectionDaoORMBrConnection<T>.Rollback;
begin
  if (ManagerTransaction) and (FConnection.InTransaction) then
    FConnection.Rollback;
end;

procedure TGBConnectionDaoORMBrConnection<T>.StartTransaction;
begin
  if (ManagerTransaction) and (not FConnection.InTransaction) then
    FConnection.StartTransaction;
end;

end.
