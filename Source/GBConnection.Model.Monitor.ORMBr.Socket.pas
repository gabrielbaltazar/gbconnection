unit GBConnection.Model.Monitor.ORMBr.Socket;

interface

uses
  System.SysUtils,
  System.Classes,
  System.Win.ScktComp,
  System.TypInfo,
  System.Threading,
  System.Variants,
  Data.DB,
  GBConnection.Model.Interfaces,
  ormbr.factory.interfaces,
  Winapi.ShellAPI,
  Winapi.Windows;

type TWSPW3MonitorORMBrSocket = class(TInterfacedObject, ICommandMonitor, IGBMonitorConnection)

  private
    class var
      FInstance: TWSPW3MonitorORMBrSocket;

    FClientSocket : TClientSocket;

    procedure SendMessage(AMessage: String);
    procedure Command(const ASQL: string; AParams: TParams);
    procedure Show;
  public
    class function GetInstance: ICommandMonitor;

    constructor create;
    destructor Destroy; override;
end;

implementation

{ TWSPW3MonitorORMBrSocket }

procedure TWSPW3MonitorORMBrSocket.Command(const ASQL: string; AParams: TParams);
var
  iFor       : Integer;
  AsValue    : string;
  textParams : string;
begin
  SendMessage('');
  SendMessage(ASQL);
  if AParams <> nil then
  begin
    textParams := sLineBreak;
    for iFor := 0 to AParams.Count -1 do
    begin
      if AParams.Items[iFor].Value = System.Variants.Null then
        AsValue := 'NULL'
      else
      if AParams.Items[iFor].DataType = ftDateTime then
        AsValue := '"' + DateTimeToStr(AParams.Items[iFor].Value) + '"'
      else
      if AParams.Items[iFor].DataType = ftDate then
        AsValue := '"' + DateToStr(AParams.Items[iFor].Value) + '"'
      else
        AsValue := '"' + VarToStr(AParams.Items[iFor].Value) + '"';

      textParams := textParams + AParams.Items[iFor].Name + ' = ' + AsValue + ' (' +
                    GetEnumName(TypeInfo(TFieldType), Ord(AParams.Items[iFor].DataType)) + ')' + sLineBreak;
    end;
    SendMessage(textParams);
  end;
end;

constructor TWSPW3MonitorORMBrSocket.create;
begin
  FClientSocket := TClientSocket.Create(nil);
  FClientSocket.Address := '127.0.0.1';
  FClientSocket.Port    := 15746;
end;

destructor TWSPW3MonitorORMBrSocket.Destroy;
begin
  FClientSocket.Free;
//  if Assigned(FInstance) then
//    FreeAndNil(FInstance);
end;

class function TWSPW3MonitorORMBrSocket.GetInstance: ICommandMonitor;
begin
  if FInstance = nil then
    FInstance := TWSPW3MonitorORMBrSocket.Create;
  Result := FInstance;
end;

procedure TWSPW3MonitorORMBrSocket.SendMessage(AMessage: String);
begin
  try
    FClientSocket.Socket.SendText(AnsiString( AMessage + sLineBreak));
  except on E: Exception do
  end;
end;

procedure TWSPW3MonitorORMBrSocket.Show;
var
  appMonitor: PWideChar;
begin
  appMonitor := PChar( ExtractFilePath(GetModuleName(HInstance)) + 'PW3Monitor.exe' );
  if FileExists(appMonitor) then
  begin
    ShellExecute(0, 'open', appMonitor, '', '', SW_SHOWNORMAL);
    Sleep(3000);
    try
      FClientSocket.Active  := True;
    except
    end;
  end;
end;

end.
