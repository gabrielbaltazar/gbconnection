unit GBConnection.Data.Model.Command.Table.Oracle;

interface

uses
  GBConnection.Data.Model.Command.Interfaces,
  GBConnection.Data.Model.Command.Table.Abstract,
  System.SysUtils,
  System.Classes;

type TGBConnectionDataModelCommandTableOracle = class(TGBConnectionDataModelCommandTableAbstract,
                                                      IGBConnectionDataModelCommandTable)

  public
    function TableColumns(TableName: string): string; override;

end;

implementation

{ TGBConnectionDataModelCommandTableOracle }

function TGBConnectionDataModelCommandTableOracle.TableColumns(TableName: string): string;
begin
  with TStringList.Create do
  begin
    Add('SELECT                                                                                 ');
    Add('  ALL_TAB_COLUMNS.COLUMN_NAME AS COLUMN_NAME                                           ');
    Add(' ,ALL_TAB_COLUMNS.COLUMN_ID AS column_position                                         ');
    Add(' ,ALL_TAB_COLUMNS.DATA_LENGTH AS column_size                                           ');
    Add(' ,ALL_TAB_COLUMNS.DATA_PRECISION AS column_precision                                   ');
    Add(' ,ALL_TAB_COLUMNS.DATA_SCALE AS column_scale                                           ');
    Add(' ,ALL_TAB_COLUMNS.NULLABLE AS column_nullable                                          ');
    Add(' ,ALL_TAB_COLUMNS.DATA_TYPE AS column_typename                                         ');
    Add(' ,ALL_TAB_COLUMNS.CHARACTER_SET_NAME AS column_charset                                 ');
    Add(' ,ALL_TAB_COLUMNS.DATA_DEFAULT as column_default                                       ');
    Add(' ,'' '' as PK                                                                          ');
    Add('FROM                                                                                   ');
    Add('    ALL_TABLES                                                                         ');
    Add('    INNER JOIN ALL_TAB_COLUMNS ON (ALL_TABLES.TABLE_NAME = ALL_TAB_COLUMNS.TABLE_NAME) ');
    Add('WHERE                                                                                  ');
    Add('     UPPER( ALL_TABLES.TABLE_NAME ) = UPPER( :TABLE_NAME ) ');

    if not FSchema.IsEmpty then
    begin
      Add('     AND UPPER( ALL_TABLES.OWNER ) = UPPER( :SCHEMA )');
      Add('     AND ALL_TAB_COLUMNS.OWNER = :SCHEMA ');
    end;

    Add('ORDER BY ALL_TAB_COLUMNS.COLUMN_ID                                                     ');

    Result := Text;
    Result := Result.Replace(':TABLE_NAME', TableName.QuotedString);
    Result := Result.Replace(':SCHEMA', FSchema.QuotedString);
    Free;
  end;
end;

end.
