unit GBconnection.Dao.ORMBr.Interfaces;

interface

uses
  ormbr.factory.interfaces,
  GBConnection.Model.Interfaces;

type
  IGBConnectionDaoORMBr = interface
    ['{817FE70D-F0CB-4942-BAE1-BE1C7ADCBE51}']
    function GetConnection(Connection: IGBConnection): IDBConnection;
  end;

  IGBConnectionDaoORMBrFactory = interface
    ['{EDC5402B-7EE6-4226-BD50-36739D2F3BF2}']
    function CreateConnectionORMBr: IGBConnectionDaoORMBr;
  end;

implementation

end.
