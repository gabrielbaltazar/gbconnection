unit GBConnection.Dao.ORMBr.Firedac;

interface

uses
  GBConnection.Model.Interfaces,
  GBconnection.Dao.ORMBr.Interfaces,
  ormbr.factory.interfaces,
  ormbr.factory.firedac,
  System.SysUtils;

type TGBConnectionDaoORMBrFiredac = class(TInterfacedObject, IGBConnectionDaoORMBr)

  protected
    function GetDriverName(Connection: IGBConnection): TDriverName;
    function GetConnection(Connection: IGBConnection): IDBConnection;

  public
    class function New: IGBConnectionDaoORMBr;
end;

implementation

{ TGBConnectionDaoORMBrFiredac }

function TGBConnectionDaoORMBrFiredac.GetConnection(Connection: IGBConnection): IDBConnection;
begin
  Result := TFactoryFiredac.Create(Connection.Component, GetDriverName(Connection));
end;

function TGBConnectionDaoORMBrFiredac.GetDriverName(Connection: IGBConnection): TDriverName;
begin
  case Connection.Params.Driver of
    gbOracle: result := TDriverName.dnOracle;
    gbMSSQL: result := TDriverName.dnMSSQL;
    gbMySql: result := TDriverName.dnMySQL;
    gbSQLite: result := TDriverName.dnSQLite;
    gbPostgres: result := TDriverName.dnPostgreSQL;
  else
    raise ENotImplemented.Create('Driver n�o implementado.');
  end;
end;

class function TGBConnectionDaoORMBrFiredac.New: IGBConnectionDaoORMBr;
begin
  result := Self.Create;
end;

end.

