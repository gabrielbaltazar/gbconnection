unit GBConnection.Test.Base.Oracle.Connection;

interface

uses
  DUnitX.TestFramework,
  GBConnection.Test.Base.Oracle;

type
  TGBConnectionTestOracleConnection = class(TGBConnectionTestBaseOracle)
  public
    [Test] procedure Connect;

  end;

implementation

{ TGBConnectionTestOracleConnection }

procedure TGBConnectionTestOracleConnection.Connect;
begin
  Self.Connection;
end;

end.
