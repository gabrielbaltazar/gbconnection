unit GBConnection.Test.Base.Postgresql;

interface

uses
  GBConnection.Model.Interfaces,
  GBConnection.Model.Factory,
  GBConnection.Test.Base,
  ormbr.dml.generator.postgresql;

type TGBConnectionTestBasePostgresql = class(TGBConnectionTestBase)

  private

  public
    function Connection: IGBConnection;

end;

var
  FConnection: IGBConnection;

implementation

{ TGBConnectionTestBasePostgresql }

function TGBConnectionTestBasePostgresql.Connection: IGBConnection;
begin
  if not Assigned(FConnection) then
  begin
    FConnection := TGBFactoryConnection.New.createConnection;
    FConnection
      .Params
        .Driver(gbPostgres)
        .Database('GBConnectionTest')
        .UserName('postgres')
        .Password('pw3@#EGS')
        .Server('127.0.0.1')
        .Port(5433)
      .&End
      .Connect;
  end;

  result := FConnection;
end;

end.
