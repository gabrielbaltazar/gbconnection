unit GBConnection.Test.Base.Postgresql.Connection;

interface

uses
  DUnitX.TestFramework,
  GBConnection.Test.Base.Postgresql;

type
  TGBConnectionTestPostgresqlConnection = class(TGBConnectionTestBasePostgresql)
  public
    [Test] procedure Connect;

  end;

implementation

{ TGBConnectionTestPostgresqlConnection }

procedure TGBConnectionTestPostgresqlConnection.Connect;
begin
  Self.Connection;
end;

end.
