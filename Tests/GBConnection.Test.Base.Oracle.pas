unit GBConnection.Test.Base.Oracle;

interface

uses
  GBConnection.Model.Interfaces,
  GBConnection.Model.Factory,
  GBConnection.Test.Base;

type TGBConnectionTestBaseOracle = class(TGBConnectionTestBase)

  private

  protected
    function Connection: IGBConnection;

end;

var
  FConnection: IGBConnection;

implementation

{ TGBConnectionTestBaseOracle }

function TGBConnectionTestBaseOracle.Connection: IGBConnection;
begin
  if not Assigned(FConnection) then
  begin
    FConnection := TGBFactoryConnection.New.createConnection;
    FConnection
      .Params
        .Driver(gbOracle)
        .Database('ATRIO')
        .UserName('cm')
        .Password('cmsol')
      .&End
      .Connect;
  end;

  result := FConnection;
end;

end.
