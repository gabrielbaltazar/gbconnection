unit GBConnection.Test.Base.MySql;

interface

uses
  GBConnection.Test.Base,
  GBConnection.Model.Interfaces,
  GBConnection.Model.Factory,
  ormbr.dml.generator.mysql,
  GBConnection.Monitor;

type TGBConnectionTestMySql = class(TGBConnectionTestBase)

  public
    function Connection: IGBConnection;

end;

var
  FConnection: IGBConnection;

implementation

{ TGBConnectionTestMySql }

function TGBConnectionTestMySql.Connection: IGBConnection;
begin
  if not Assigned(FConnection) then
  begin
    FConnection := TGBFactoryConnection.New.createConnection;
    FConnection
      .Params
        .Driver(gbMySql)
        .Database('GBConnectionTest')
        .UserName('root')
        .Password('root')
        .Server('localhost')
        .Port(3306)
      .&End
      .Connect;
  end;

  result := FConnection;
end;

end.
