unit GBConnection.Model.EmpresaProp;

interface

uses
  System.SysUtils,
  System.Classes,
  Data.DB,
  ormbr.mapping.attributes,
  ormbr.types.mapping,
  System.Generics.Collections,
  ormbr.mapping.register;

type
  [Entity]
  [Table('EmpresaProp')]
  [PrimaryKey('idPessoa', NotInc, NoSort, True)]
  TModelEmpresaProp = class
  private
    FidPessoa: Double;
    FnomeEmpresa: String;

  public
    [Column('idPessoa', ftFloat)]
    [NotExist('empresaProp')]
    property idPessoa: Double read FidPessoa write FidPessoa;

    [Column('nomeEmpresa', ftString)]
    property nomeEmpresa: String read FnomeEmpresa write FnomeEmpresa;

    constructor create;
    destructor Destroy; override;
  end;

implementation

constructor TModelEmpresaProp.create;
begin
end;

destructor TModelEmpresaProp.Destroy;
begin
end;

initialization
  TRegisterClass.RegisterEntity(TModelEmpresaProp);

end.
