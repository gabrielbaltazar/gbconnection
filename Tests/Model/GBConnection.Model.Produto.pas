unit GBConnection.Model.Produto;

interface

uses
  System.SysUtils,
  System.Classes,
  Data.DB,
  ormbr.mapping.attributes,
  ormbr.types.mapping,
  System.Generics.Collections,
  ormbr.mapping.register;

type
  [Entity]
  [Table('Produto')]
  [PrimaryKey('codProduto', NotInc, NoSort, True)]
  TModelProduto = class
  private
    FcodProduto: String;
    Fdescricao: String;

  public
    [Column('codProduto', ftString)]
    [NotExist('Produto', '', '', 'Produto :1 j� existe', True, False)]
    property codProduto: String read FcodProduto write FcodProduto;

    [Column('descricao', ftString)]
    property descricao: String read Fdescricao write Fdescricao;

    constructor create;
    destructor Destroy; override;
  end;

implementation

constructor TModelProduto.create;
begin
end;

destructor TModelProduto.Destroy;
begin
end;

initialization
  TRegisterClass.RegisterEntity(TModelProduto);

end.
