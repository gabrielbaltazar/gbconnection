unit GBConnection.Model.Pessoa;

interface

uses System.SysUtils, System.Classes, Data.DB,
     ormbr.mapping.attributes,
     ormbr.types.mapping,
     ormbr.types.lazy,
     ormbr.types.nullable,
     ormbr.mapping.register;

type
  [Entity]
  [Table('Pessoa')]
  [PrimaryKey('idPessoa', AutoInc, NoSort, True)]
  [Sequence('idPessoa')]
  TModelPessoa = class
  private
    FidPessoa: Double;
    Fnome: String;
    FnumeroDocumento: String;

  public
    [Column('idPessoa', ftFloat)]
    [Exist('empresaprop')]
    property idPessoa: Double read FidPessoa write FidPessoa;

    [NotEmpty]
    [Column('nome', ftString)]
    [NotExist('pessoa')]
    property nome: String read Fnome write Fnome;

    [NotEmpty]
    [Column('numeroDocumento', ftString)]
    [NotExist('pessoa', '', '', 'Pessoa :1 j� existe.')]
    property numeroDocumento: String read FnumeroDocumento write FnumeroDocumento;

    constructor create;
    destructor  Destroy; override;

end;

implementation

constructor TModelPessoa.create;
begin

end;

destructor TModelPessoa.Destroy;
begin

  inherited;
end;

initialization
  TRegisterClass.RegisterEntity(TModelPessoa);

end.
