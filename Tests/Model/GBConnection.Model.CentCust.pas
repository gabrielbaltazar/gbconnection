unit GBConnection.Model.CentCust;

interface

uses
  System.SysUtils,
  System.Classes,
  Data.DB,
  ormbr.mapping.attributes,
  ormbr.types.mapping,
  System.Generics.Collections,
  ormbr.mapping.register;

type
  [Entity]
  [Table('CentCust')]
  [PrimaryKey('codCentroCusto', NotInc, NoSort, True)]
  TModelCentCust = class
  private
    FcodCentroCusto: String;
    FidEmpresa: Double;
    Fdescricao: String;

  public
    [Column('codCentroCusto', ftString)]
    property codCentroCusto: String read FcodCentroCusto write FcodCentroCusto;

    [Column('idEmpresa', ftFloat)]
    property idEmpresa: Double read FidEmpresa write FidEmpresa;

    [Column('descricao', ftString)]
    [Exist('invalidTable')]
    property descricao: String read Fdescricao write Fdescricao;

    constructor create;
    destructor Destroy; override;
  end;

implementation

constructor TModelCentCust.create;
begin
end;

destructor TModelCentCust.Destroy;
begin
end;

initialization
  TRegisterClass.RegisterEntity(TModelCentCust);

end.
