unit GBConnection.DAO.Pessoa;

interface

uses
  GBConnection.Dao.Interfaces,
  GBConnection.Dao.ORMBr,
  GBConnection.Model.Pessoa,
  System.SysUtils;

type TGBConnectionDAOPessoa = class(TGBModelDaoORMBr<TModelPessoa>, IGBModelDao<TModelPessoa>)
end;

implementation

end.
