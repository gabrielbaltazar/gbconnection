unit GBConnection.Model.Documento;

interface

uses
  System.SysUtils,
  System.Classes,
  Data.DB,
  ormbr.mapping.attributes,
  ormbr.types.mapping,
  System.Generics.Collections,
  ormbr.mapping.register;

type
  [Entity]
  [Table('Documento')]
  [PrimaryKey('codDocumento', AutoInc, NoSort, True)]
  [Sequence('codDocumento')]
  TModelDocumento = class
  private
    FcodDocumento: Double;
    FidPessoa: Double;
    FcodCentroCusto: String;
    FdataEmissao: TDateTime;
    Fvalor: Double;
    FdataVencimento: TDateTime;
    FrecPag: string;

  public
    [Column('codDocumento', ftFloat)]
    property codDocumento: Double read FcodDocumento write FcodDocumento;

    [Column('idPessoa', ftFloat)]
    [Exist('empresaprop', '', '', 'Empresa :1 n�o encontrada.')]
    property idPessoa: Double read FidPessoa write FidPessoa;

    [Column('recPag', ftString)]
    [Range('R,P', 'O campo :2 deve conter o valor entre :3. O valor atual � :1')]
    property recPag: string read FrecPag write FrecPag;

    [Column('codCentroCusto', ftString)]
    [Exist('centcust', 'codCentroCusto,idEmpresa', 'codCentroCusto,idPessoa')]
    property codCentroCusto: String read FcodCentroCusto write FcodCentroCusto;

    [Column('dataEmissao', ftDateTime)]
    property dataEmissao: TDateTime read FdataEmissao write FdataEmissao;

    [Column('dataVencimento', ftDateTime)]
    property dataVencimento: TDateTime read FdataVencimento write FdataVencimento;

    [Column('valor', ftFloat)]
    [MinimumValueConstraint(5)]
    [MaximumValueConstraint(15)]
    property valor: Double read Fvalor write Fvalor;

    constructor create;
    destructor Destroy; override;
  end;

implementation

constructor TModelDocumento.create;
begin

end;

destructor TModelDocumento.Destroy;
begin
end;

initialization
  TRegisterClass.RegisterEntity(TModelDocumento);

end.
