unit GBConnection.Model.Classes;

interface

uses
  GBConnection.Model.CentCust,
  GBConnection.Model.Documento,
  GBConnection.Model.EmpresaProp,
  GBConnection.Model.Pessoa,
  GBConnection.Model.Produto;

type
  TModelCentCust    = GBConnection.Model.CentCust.TModelCentCust;
  TModelDocumento   = GBConnection.Model.Documento.TModelDocumento;
  TModelEmpresaProp = GBConnection.Model.EmpresaProp.TModelEmpresaProp;
  TModelPessoa      = GBConnection.Model.Pessoa.TModelPessoa;
  TModelProduto     = GBConnection.Model.Produto.TModelProduto;

implementation

end.
