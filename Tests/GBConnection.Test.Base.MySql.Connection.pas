unit GBConnection.Test.Base.MySql.Connection;

interface

uses
  DUnitX.TestFramework,
  GBConnection.Test.Base.MySql;

type
  TGBConnectionTestMySqlConnection = class(TGBConnectionTestMySql)
  public
    [Test] procedure Connect;

  end;

implementation

{ TGBConnectionTestMySqlConnection }

procedure TGBConnectionTestMySqlConnection.Connect;
begin
  Self.Connection;
end;

end.
