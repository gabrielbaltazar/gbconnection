unit GBConnection.Test.Validator.Exist;

interface

uses
  DUnitX.TestFramework,
  GBConnection.Test.Base,
  GBConnection.Dao.Interfaces,
  GBConnection.Dao.ORMBr.Exceptions,
  GBConnection.Dao.ORMBr.Validator,
  GBConnection.Model.Classes,
  System.SysUtils;

type
  [TestFixture]
  TGBConnectionTestValidatorExist = class(TGBConnectionTestBase)

  private
    FPessoa : TModelPessoa;
    FDocumento: TModelDocumento;
    FValidator: IGBModelDaoValidator<TModelPessoa>;
    FValidatorDocumento: IGBModelDaoValidator<TModelDocumento>;

    procedure createPadrao;
    procedure validarPessoa;
    procedure validarDocumento;
  public
    constructor create;

    [Setup]
    procedure Setup;

    [TearDown]
    procedure TearDown;

    [Test]
    procedure TestAssertExistAttribute;

    [Test]
    procedure TestAssertExistAttributeEmptyValue;

    [Test]
    procedure TestAssertExistPassandoColuna;

    [Test]
    procedure TestAssertExistChaveComposta;

    [Test]
    procedure TestAssertExistChaveCompostaValida;

    [Test]
    procedure TestValidatorAssertExist;

    [Test]
    procedure TestValidatorAssertExistInvalidTable;
  end;

implementation

{ TGBConnectionTestValidatorExist }

constructor TGBConnectionTestValidatorExist.create;
begin
  FValidator := TGBModelDAOValidator<TModelPessoa>.New(ORMConnection);
  FValidatorDocumento := TGBModelDAOValidator<TModelDocumento>.New(ORMConnection);
end;

procedure TGBConnectionTestValidatorExist.createPadrao;
begin
  FPessoa := TModelPessoa.create;
  FPessoa.idPessoa := 1;
  FPessoa.nome := 'Gabriel';
  FPessoa.numeroDocumento := '99999999999';

  FDocumento := TModelDocumento.create;
  FDocumento.idPessoa := 1;
  FDocumento.codCentroCusto := '10';
  FDocumento.valor := 10;
end;

procedure TGBConnectionTestValidatorExist.Setup;
begin
  FPessoa    := nil;
  FDocumento := nil;
end;

procedure TGBConnectionTestValidatorExist.TearDown;
begin
  FreeAndNil(FPessoa);
  FreeAndNil(FDocumento);
end;

procedure TGBConnectionTestValidatorExist.TestAssertExistAttribute;
begin
  createPadrao;
  FPessoa.idPessoa := 5;

  Assert.WillRaiseWithMessage(
    validarPessoa,
    EGBDAOEntityNotFound,
    'empresaprop n�o encontrado.');
end;

procedure TGBConnectionTestValidatorExist.TestAssertExistAttributeEmptyValue;
begin
  createPadrao;
  FPessoa.idPessoa := 0;

  Assert.WillNotRaise(validarPessoa);
end;

procedure TGBConnectionTestValidatorExist.TestAssertExistChaveComposta;
begin
  createPadrao;
  FDocumento.codCentroCusto := '11';

  Assert.WillRaiseWithMessage(
    validarDocumento,
    EGBDAOEntityNotFound,
    'centcust n�o encontrado.');
end;

procedure TGBConnectionTestValidatorExist.TestAssertExistChaveCompostaValida;
begin
  createPadrao;
  Assert.WillNotRaise(validarDocumento);
end;

procedure TGBConnectionTestValidatorExist.TestAssertExistPassandoColuna;
begin
  createPadrao;
  FDocumento.idPessoa := 5;

  Assert.WillRaiseWithMessage(
    validarDocumento,
    EGBDAOEntityNotFound,
    'Empresa 5 n�o encontrada.');
end;

procedure TGBConnectionTestValidatorExist.TestValidatorAssertExist;
begin
  Assert.IsTrue( FValidator.Exists('idPessoa = 1'));
end;

procedure TGBConnectionTestValidatorExist.TestValidatorAssertExistInvalidTable;
var
  centCust: TModelCentCust;
begin
  centCust := TModelCentCust.create;
  try
    centCust.descricao := '12';

    Assert.WillRaiseWithMessage(
      procedure
      begin
        TGBModelDAOValidator<TModelCentCust>.New(ORMConnection)
          .RunValidate(centCust);
      end,
      Exception,
      'Classe referente a tabela invalidTable n�o encontrada.'
    );

  finally
    centCust.Free;
  end;
end;

procedure TGBConnectionTestValidatorExist.validarPessoa;
begin
  FValidator.RunValidate(FPessoa);
end;

procedure TGBConnectionTestValidatorExist.validarDocumento;
begin
  FValidatorDocumento.RunValidate(FDocumento);
end;

end.
