unit GBConnection.Test.Validator;

interface

uses
  DUnitX.TestFramework,
  GBConnection.Test.Base,
  GBConnection.Dao.Interfaces,
  GBConnection.Dao.ORMBr.Exceptions,
  GBConnection.Dao.ORMBr.Validator,
  GBConnection.Model.Classes,
  System.SysUtils;

type
  [TestFixture]
  TGBConnectionTestValidatorNotExist = class(TGBConnectionTestBase)

  private
    FPessoa : TModelPessoa;
    FDocumento: TModelDocumento;
    FValidator: IGBModelDaoValidator<TModelPessoa>;
    FValidatorDocumento: IGBModelDaoValidator<TModelDocumento>;

    procedure createPadrao;
    procedure validarPessoa;
    procedure validarDocumento;
  public
    constructor create;

    [Setup]
    procedure Setup;

    [TearDown]
    procedure TearDown;

    [Test]
    procedure TestAssertExistAttribute;

    [Test]
    procedure TestAssertExistAttributeEmptyValue;

    [Test]
    procedure TestAssertExistPassandoColuna;

    [Test]
    procedure TestAssertExistChaveComposta;

    [Test]
    procedure TestAssertExistChaveCompostaValida;

    [Test]
    procedure TestValidatorAssertExist;

    [Test]
    procedure TestValidatorAssertExistInvalidTable;
  end;

implementation

{ TGBConnectionTestValidatorNotExist }

constructor TGBConnectionTestValidatorNotExist.create;
begin
  FValidator := TGBModelDAOValidator<TModelPessoa>.New(ORMConnection);
  FValidatorDocumento := TGBModelDAOValidator<TModelDocumento>.New(ORMConnection);
end;

procedure TGBConnectionTestValidatorNotExist.createPadrao;
begin
  FPessoa := TModelPessoa.create;
  FPessoa.idPessoa := 1;
  FPessoa.nome := 'Gabriel';
  FPessoa.numeroDocumento := '99999999999';

  FDocumento := TModelDocumento.create;
  FDocumento.idPessoa := 1;
  FDocumento.codCentroCusto := '10';
  FDocumento.valor := 10;
end;

procedure TGBConnectionTestValidatorNotExist.Setup;
begin
  FPessoa    := nil;
  FDocumento := nil;
end;

procedure TGBConnectionTestValidatorNotExist.TearDown;
begin
  FreeAndNil(FPessoa);
  FreeAndNil(FDocumento);
end;

procedure TGBConnectionTestValidatorNotExist.TestAssertExistAttribute;
begin
  createPadrao;
  FPessoa.idPessoa := 5;

  Assert.WillRaiseWithMessage(
    validarPessoa,
    EGBDAOEntityNotFound,
    'empresaprop n�o encontrado.');
end;

procedure TGBConnectionTestValidatorNotExist.TestAssertExistAttributeEmptyValue;
begin
  createPadrao;
  FPessoa.idPessoa := 0;

  Assert.WillNotRaise(validarPessoa);
end;

procedure TGBConnectionTestValidatorNotExist.TestAssertExistChaveComposta;
begin
  createPadrao;
  FDocumento.codCentroCusto := '11';

  Assert.WillRaiseWithMessage(
    validarDocumento,
    EGBDAOEntityNotFound,
    'centcust n�o encontrado.');
end;

procedure TGBConnectionTestValidatorNotExist.TestAssertExistChaveCompostaValida;
begin
  createPadrao;
  Assert.WillNotRaise(validarDocumento);
end;

procedure TGBConnectionTestValidatorNotExist.TestAssertExistPassandoColuna;
begin
  createPadrao;
  FDocumento.idPessoa := 5;

  Assert.WillRaiseWithMessage(
    validarDocumento,
    EGBDAOEntityNotFound,
    'Empresa 5 n�o encontrada.');
end;

procedure TGBConnectionTestValidatorNotExist.TestValidatorAssertExist;
begin
  Assert.IsTrue( FValidator.Exists('idPessoa = 1'));
end;

procedure TGBConnectionTestValidatorNotExist.TestValidatorAssertExistInvalidTable;
var
  centCust: TModelCentCust;
begin
  centCust := TModelCentCust.create;
  try
    centCust.descricao := '12';

    Assert.WillRaiseWithMessage(
      procedure
      begin
        TGBModelDAOValidator<TModelCentCust>.New(ORMConnection)
          .RunValidate(centCust);
      end,
      Exception,
      'Classe referente a tabela invalidTable n�o encontrada.'
    );

  finally
    centCust.Free;
  end;
end;

procedure TGBConnectionTestValidatorNotExist.validarPessoa;
begin
  FValidator.RunValidate(FPessoa);
end;

procedure TGBConnectionTestValidatorNotExist.validarDocumento;
begin
  FValidatorDocumento.RunValidate(FDocumento);
end;

end.
