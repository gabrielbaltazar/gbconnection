unit GBConnection.Test.Validator.MinMaxValue;

interface

uses
  DUnitX.TestFramework,
  GBConnection.Test.Base,
  GBConnection.Dao.Interfaces,
  GBConnection.Dao.ORMBr.Exceptions,
  GBConnection.Dao.ORMBr.Validator,
  GBConnection.Model.Classes,
  ormbr.mapping.exceptions,
  System.SysUtils;

type
  [TestFixture]
  TGBConnectionTestValidatorMinMaxValue = class(TGBConnectionTestBase)

  private
    FModel : TModelDocumento;
    FValidator: IGBModelDaoValidator<TModelDocumento>;

    procedure createPadrao;
    procedure validar;
  public
    constructor create;

    [Setup]
    procedure Setup;

    [TearDown]
    procedure TearDown;

    [Test]
    procedure TestAssertValorAbaixoDoMinimo;

    [Test]
    procedure TestAssertValorAcimaDoMinimo;

    [Test]
    procedure TestAssertValorIgualAoMinimo;

    [Test]
    procedure TestAssertValorAbaixoDoMaximo;

    [Test]
    procedure TestAssertValorAcimaDoMaximo;

    [Test]
    procedure TestAssertValorIgualAoMaximo;
  end;

implementation

{ TGBConnectionTestValidatorMinMaxValue }

constructor TGBConnectionTestValidatorMinMaxValue.create;
begin
  FValidator := TGBModelDAOValidator<TModelDocumento>.New(ORMConnection);
end;

procedure TGBConnectionTestValidatorMinMaxValue.createPadrao;
begin
  FModel := TModelDocumento.create;
  FModel.idPessoa := 1;
  FModel.recPag := 'P';
  FModel.valor := 10;
end;

procedure TGBConnectionTestValidatorMinMaxValue.Setup;
begin
  FModel := nil;
end;

procedure TGBConnectionTestValidatorMinMaxValue.TearDown;
begin
  FreeAndNil(FModel);
end;

procedure TGBConnectionTestValidatorMinMaxValue.TestAssertValorAbaixoDoMaximo;
begin
  createPadrao;
  FModel.valor := 6;

  Assert.WillNotRaise(validar);
end;

procedure TGBConnectionTestValidatorMinMaxValue.TestAssertValorAbaixoDoMinimo;
begin
  createPadrao;
  FModel.valor := 4;

  Assert.WillRaiseWithMessage(
    validar,
    EMinimumValueConstraint,
    'O valor m�nimo do campo [ valor ] permitido � [ 5 ]!');
end;

procedure TGBConnectionTestValidatorMinMaxValue.TestAssertValorAcimaDoMaximo;
begin
  createPadrao;
  FModel.valor := 16;

  Assert.WillRaiseWithMessage(
    validar,
    EMaximumValueConstraint,
    'O valor m�ximo do campo [ valor ] permitido � [ 15 ]!');
end;

procedure TGBConnectionTestValidatorMinMaxValue.TestAssertValorAcimaDoMinimo;
begin
  createPadrao;
  FModel.valor := 10;

  Assert.WillNotRaise(validar);
end;

procedure TGBConnectionTestValidatorMinMaxValue.TestAssertValorIgualAoMaximo;
begin
  createPadrao;
  FModel.valor := 15;

  Assert.WillNotRaise(validar);
end;

procedure TGBConnectionTestValidatorMinMaxValue.TestAssertValorIgualAoMinimo;
begin
  createPadrao;
  FModel.valor := 5;

  Assert.WillNotRaise(validar);
end;

procedure TGBConnectionTestValidatorMinMaxValue.validar;
begin
  FValidator.RunValidate(FModel);
end;

end.
