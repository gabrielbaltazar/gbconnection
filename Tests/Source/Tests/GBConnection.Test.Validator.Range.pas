unit GBConnection.Test.Validator.Range;

interface

uses
  DUnitX.TestFramework,
  GBConnection.Test.Base,
  GBConnection.Dao.Interfaces,
  GBConnection.Dao.ORMBr.Exceptions,
  GBConnection.Dao.ORMBr.Validator,
  GBConnection.Model.Classes,
  ormbr.mapping.exceptions,
  System.SysUtils;

type
  [TestFixture]
  TGBConnectionTestValidatorRange = class(TGBConnectionTestBase)

  private
    FModel : TModelDocumento;
    FValidator: IGBModelDaoValidator<TModelDocumento>;

    procedure createPadrao;
  public
    constructor create;

    [Setup]
    procedure Setup;

    [TearDown]
    procedure TearDown;

    [Test]
    procedure TestAssertForaDoRange;

    [Test]
    procedure TestAssertCampoVazio;

    [Test]
    procedure TestAssertDentroDoRange;
  end;

implementation

{ TGBConnectionTestValidatorRange }

constructor TGBConnectionTestValidatorRange.create;
begin
  FValidator := TGBModelDAOValidator<TModelDocumento>.New(ORMConnection);
end;

procedure TGBConnectionTestValidatorRange.createPadrao;
begin
  FModel := TModelDocumento.create;
  FModel.idPessoa := 1;
  FModel.recPag := 'P';
  FModel.valor := 10;
end;

procedure TGBConnectionTestValidatorRange.Setup;
begin
  FModel := nil;
end;

procedure TGBConnectionTestValidatorRange.TearDown;
begin
  FreeAndNil(FModel);
end;

procedure TGBConnectionTestValidatorRange.TestAssertCampoVazio;
begin
  createPadrao;
  FModel.recPag := '';

  Assert.WillNotRaise(
    procedure
    begin
      Self.FValidator.RunValidate(FModel);
    end);
end;

procedure TGBConnectionTestValidatorRange.TestAssertDentroDoRange;
begin
  createPadrao;
  FModel.recPag := 'P';

  Assert.WillNotRaise(
    procedure
    begin
      Self.FValidator.RunValidate(FModel);
    end);
end;

procedure TGBConnectionTestValidatorRange.TestAssertForaDoRange;
begin
  createPadrao;
  FModel.recPag := 'A';

  Assert.WillRaiseWithMessage(
    procedure
    begin
      Self.FValidator.RunValidate(FModel);
    end,
    ERangeException,
    'O campo recPag deve conter o valor entre [R, P]. O valor atual � A');
end;

end.
