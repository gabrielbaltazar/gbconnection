unit GBConnection.Test.Validator.NotExist;

interface

uses
  DUnitX.TestFramework,
  GBConnection.Test.Base,
  GBConnection.Dao.Interfaces,
  GBConnection.Dao.ORMBr.Exceptions,
  GBConnection.Dao.ORMBr.Validator,
  GBConnection.Model.Classes,
  System.SysUtils;

type
  [TestFixture]
  TGBConnectionTestValidatorNotExist = class(TGBConnectionTestBase)

  private
    FPessoa : TModelPessoa;
    FDocumento: TModelDocumento;
    FValidator: IGBModelDaoValidator<TModelPessoa>;
    FValidatorDocumento: IGBModelDaoValidator<TModelDocumento>;

    procedure createPadrao;
    procedure validarPessoa;

  public
    [Setup]
    procedure Setup;

    [TearDown]
    procedure TearDown;

    [Test]
    procedure TestAssertNotExist;

    [Test]
    procedure TestAssertNotExistComIdDiferente;

    [Test]
    procedure TestAssertNotExistWithMessage;

  end;

implementation

{ TGBConnectionTestValidatorNotExist }

procedure TGBConnectionTestValidatorNotExist.createPadrao;
begin
  FPessoa := TModelPessoa.create;
  FPessoa.idPessoa := 1;
  FPessoa.nome := 'Gabriel';
  FPessoa.numeroDocumento := '99999999999';

  FDocumento := TModelDocumento.create;
  FDocumento.idPessoa := 1;
  FDocumento.codCentroCusto := '10';
  FDocumento.valor := 10;
end;

procedure TGBConnectionTestValidatorNotExist.Setup;
begin
  FValidator := TGBModelDAOValidator<TModelPessoa>.New(ORMConnection);
  FValidatorDocumento := TGBModelDAOValidator<TModelDocumento>.New(ORMConnection);
  FPessoa := nil;
  FDocumento := nil;
end;

procedure TGBConnectionTestValidatorNotExist.TearDown;
begin
  FreeAndNil(FPessoa);
  FreeAndNil(FDocumento);
end;

procedure TGBConnectionTestValidatorNotExist.TestAssertNotExist;
begin
  createPadrao;
  FPessoa.nome := 'PESSOA 1';
  Assert.WillRaiseWithMessage(
    validarPessoa,
    EGBDAOEntityAlreadyExists,
    'pessoa encontrado.');

end;

procedure TGBConnectionTestValidatorNotExist.TestAssertNotExistComIdDiferente;
begin
  createPadrao;
  FPessoa.idPessoa := 2;
  FPessoa.nome := 'PESSOA 1';
  Assert.WillNotRaiseAny(validarPessoa);
end;

procedure TGBConnectionTestValidatorNotExist.TestAssertNotExistWithMessage;
begin
  createPadrao;
  FPessoa.numeroDocumento := '12345678901234';
  Assert.WillRaiseWithMessage(
    validarPessoa,
    EGBDAOEntityAlreadyExists,
    'Pessoa 12345678901234 j� existe.');

end;

procedure TGBConnectionTestValidatorNotExist.validarPessoa;
begin
  FValidator.RunValidate(FPessoa);
end;

end.
