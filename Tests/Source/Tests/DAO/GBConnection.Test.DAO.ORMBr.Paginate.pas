unit GBConnection.Test.DAO.ORMBr.Paginate;

interface

uses
  DUnitX.TestFramework,
  GBConnection.Test.Connection,
  GBConnection.Test.Base,
  GBConnection.DAO.Interfaces,
  GBConnection.DAO.ORMBr,
  GBConnection.Model.Classes,
  System.Generics.Collections,
  System.SysUtils;

type
  [TestFixture]
  TGBConnectionTestDAOOrmbrPaginate = class
  private
    FDAO: IGBModelDao<TModelPessoa>;
    FList: TObjectList<TModelPessoa>;
    FListAux: TObjectList<TModelPessoa>;

  public
    [Setup]
    procedure Setup;

    [Teardown]
    procedure Teardown;

    [Test]
    procedure NotPaginate;

    [Test]
    procedure FirstPage;

    [Test]
    procedure SecondPage;

    [Test]
    procedure ThirdPage;
  end;

implementation

{ TGBConnectionTestDAOOrmbr }

procedure TGBConnectionTestDAOOrmbrPaginate.FirstPage;
begin
  FListAux := FDAO.ListAll;

  FDAO.SetPage(1);
  FDAO.SetPageSize(2);

  FList := FDAO.ListAll;
  Assert.IsNotNull(FList);
  Assert.AreEqual(2, FList.Count);
  Assert.AreEqual(FListAux[0].idPessoa, FList[0].idPessoa);
  Assert.AreEqual(FListAux[1].idPessoa, FList[1].idPessoa);
end;

procedure TGBConnectionTestDAOOrmbrPaginate.NotPaginate;
begin
  FList := FDAO.ListAll;
  Assert.IsNotNull(FList);
  Assert.IsTrue(FList.Count > 2);
end;

procedure TGBConnectionTestDAOOrmbrPaginate.SecondPage;
begin
  FListAux := FDAO.ListAll;

  FDAO.SetPage(2);
  FDAO.SetPageSize(2);

  FList := FDAO.ListAll;
  Assert.IsNotNull(FList);
  Assert.AreEqual(2, FList.Count);
  Assert.AreEqual(FListAux[2].idPessoa, FList[0].idPessoa);
  Assert.AreEqual(FListAux[3].idPessoa, FList[1].idPessoa);
end;

procedure TGBConnectionTestDAOOrmbrPaginate.Setup;
begin
  FDAO := TGBModelDaoORMBr<TModelPessoa>.NewGenericDAO(TestConnection);
end;

procedure TGBConnectionTestDAOOrmbrPaginate.Teardown;
begin
  FreeAndNil(FList);
  FreeAndNil(FListAux);
end;

procedure TGBConnectionTestDAOOrmbrPaginate.ThirdPage;
begin
  FListAux := FDAO.ListAll;

  FDAO.SetPage(3);
  FDAO.SetPageSize(2);

  FList := FDAO.ListAll;
  Assert.IsNotNull(FList);
  Assert.AreEqual(1, FList.Count);
  Assert.AreEqual(FListAux[4].idPessoa, FList[0].idPessoa);
end;

end.
