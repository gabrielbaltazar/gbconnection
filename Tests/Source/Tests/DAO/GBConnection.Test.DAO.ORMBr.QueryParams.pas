unit GBConnection.Test.DAO.ORMBr.QueryParams;

interface

uses
  DUnitX.TestFramework,
  GBConnection.Test.Connection,
  GBConnection.Test.Base,
  GBConnection.DAO.Interfaces,
  GBConnection.DAO.ORMBr,
  GBConnection.Model.Classes,
  System.Generics.Collections,
  System.SysUtils;

type
  [TestFixture]
  TGBConnectionTestDAOOrmbrQueryParams = class
  private
    FDAO: IGBModelDao<TModelPessoa>;
    FList: TObjectList<TModelPessoa>;
    FQueryParams: TDictionary<String, String>;

  public
    [Setup]
    procedure Setup;

    [Teardown]
    procedure Teardown;

    [Test]
    procedure ListAll;

    [Test]
    procedure BuscaPorNome;

    [Test]
    procedure BuscaPorDocumento;

    [Test]
    procedure BuscaPorNomeEDocumento;

    [Test]
    procedure BuscaPorNomeEDocumentoComFindWhere;

    [Test]
    procedure BuscaPorColunaInexistente;

    [Test]
    procedure BuscaIgnoreCase;

    [Test]
    procedure Search;

    [Test]
    procedure SearchWithQuery;
  end;

implementation

{ TGBConnectionTestDAOOrmbrQueryParams }

procedure TGBConnectionTestDAOOrmbrQueryParams.BuscaIgnoreCase;
begin
  FQueryParams.Add('nome', 'pessoa 2');
  FList := FDAO.ListAll;

  Assert.AreEqual(1, FList.Count);
  Assert.AreEqual('PESSOA 2', FList[0].nome);
end;

procedure TGBConnectionTestDAOOrmbrQueryParams.BuscaPorColunaInexistente;
begin
  FQueryParams.Add('column123', '12345678901234');
  Assert.WillNotRaiseAny(
    procedure
    begin
      FList := FDAO.ListAll;
    end);
end;

procedure TGBConnectionTestDAOOrmbrQueryParams.BuscaPorDocumento;
begin
  FQueryParams.Add('numeroDocumento', '12345678901234');
  FList := FDAO.ListAll;

  Assert.AreEqual(2, FList.Count);
  Assert.AreEqual('12345678901234', FList[0].numeroDocumento);
  Assert.AreEqual('12345678901234', FList[1].numeroDocumento);
end;

procedure TGBConnectionTestDAOOrmbrQueryParams.BuscaPorNome;
begin
  FQueryParams.Add('nome', 'PESSOA 2');
  FList := FDAO.ListAll;

  Assert.AreEqual(1, FList.Count);
  Assert.AreEqual('PESSOA 2', FList[0].nome);
end;

procedure TGBConnectionTestDAOOrmbrQueryParams.BuscaPorNomeEDocumento;
begin
  FQueryParams.Add('numeroDocumento', '12345678901234');
  FQueryParams.Add('nome', 'PESSOA 1');
  FList := FDAO.ListAll;

  Assert.AreEqual(1, FList.Count);
  Assert.AreEqual('12345678901234', FList[0].numeroDocumento);
  Assert.AreEqual('PESSOA 1', FList[0].nome);
end;

procedure TGBConnectionTestDAOOrmbrQueryParams.BuscaPorNomeEDocumentoComFindWhere;
begin
  FQueryParams.Add('numeroDocumento', '12345678901234');
  FList := FDAO.ListWhere('nome = ''PESSOA 2'' ');

  Assert.AreEqual(1, FList.Count);
  Assert.AreEqual('12345678901234', FList[0].numeroDocumento);
  Assert.AreEqual('PESSOA 2', FList[0].nome);
end;

procedure TGBConnectionTestDAOOrmbrQueryParams.ListAll;
begin
  FQueryParams.Clear;
  FList := FDAO.ListAll;

  Assert.AreEqual(5, FList.Count);
end;

procedure TGBConnectionTestDAOOrmbrQueryParams.Search;
begin
  FQueryParams.Add('search', '8901');
  FList := FDAO.ListAll;

  Assert.AreEqual(3, FList.Count);
end;

procedure TGBConnectionTestDAOOrmbrQueryParams.SearchWithQuery;
begin
  FQueryParams.Add('search', 'pessoa 1');
  FQueryParams.Add('numeroDocumento', '12345678901234');
  FList := FDAO.ListAll;

  Assert.AreEqual(1, FList.Count);
  Assert.AreEqual('PESSOA 1', FList[0].nome);
  Assert.AreEqual('12345678901234', FList[0].numeroDocumento);
end;

procedure TGBConnectionTestDAOOrmbrQueryParams.Setup;
begin
  FDAO := TGBModelDaoORMBr<TModelPessoa>.NewGenericDAO(TestConnection);
  FQueryParams := TDictionary<String, String>.Create;
  FDAO.QueryParams(FQueryParams);
end;

procedure TGBConnectionTestDAOOrmbrQueryParams.Teardown;
begin
  FreeAndNil(FList);
  FreeAndNil(FQueryParams);
end;

end.
