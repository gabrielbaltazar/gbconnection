unit GBConnection.Test.DAO.ORMBr.OrderBy;

interface

uses
  DUnitX.TestFramework,
  GBConnection.Test.Connection,
  GBConnection.Test.Base,
  GBConnection.DAO.Interfaces,
  GBConnection.DAO.ORMBr,
  GBConnection.Model.Classes,
  System.Generics.Collections,
  System.SysUtils;

type
  [TestFixture]
  TGBConnectionTestDAOOrmbrOrderBy = class
  private
    FDAO: IGBModelDao<TModelPessoa>;
    FList: TObjectList<TModelPessoa>;
    FQueryParams: TDictionary<String, String>;

  public
    [Setup]
    procedure Setup;

    [Teardown]
    procedure Teardown;

    [Test]
    procedure OrderIdPessoaAsc;

    [Test]
    procedure OrderIdPessoaDesc;

    [Test]
    procedure OrderAscWithPaginate;

    [Test]
    procedure OrderDescWithPaginate;
  end;

implementation

{ TGBConnectionTestDAOOrmbrOrderBy }

procedure TGBConnectionTestDAOOrmbrOrderBy.OrderAscWithPaginate;
begin
  FQueryParams.Add('orderBy', 'idPessoa');
  FDAO.SetPage(1);
  FDAO.SetPageSize(2);
  FList := FDAO.ListAll;

  Assert.IsNotNull(FList);
  Assert.AreEqual(2, FList.Count);
  Assert.IsTrue(FList[0].idPessoa < FList[1].idPessoa);
end;

procedure TGBConnectionTestDAOOrmbrOrderBy.OrderDescWithPaginate;
begin
  FQueryParams.Add('orderBy', 'idPessoa desc');
  FDAO.SetPage(1);
  FDAO.SetPageSize(2);
  FList := FDAO.ListAll;

  Assert.IsNotNull(FList);
  Assert.AreEqual(2, FList.Count);
  Assert.IsTrue(FList[0].idPessoa > FList[1].idPessoa);
end;

procedure TGBConnectionTestDAOOrmbrOrderBy.OrderIdPessoaAsc;
var
  I: Integer;
begin
  FQueryParams.Add('orderBy', 'idPessoa');
  FList := FDAO.ListAll;

  for I := 0 to Pred(FList.Count) do
  begin
    if I > 0 then
      Assert.IsTrue(FList[I].idPessoa > FList[I - 1].idPessoa);
  end;
end;

procedure TGBConnectionTestDAOOrmbrOrderBy.OrderIdPessoaDesc;
var
  I: Integer;
begin
  FQueryParams.Add('orderBy', 'idPessoa desc');
  FList := FDAO.ListAll;

  for I := 0 to Pred(FList.Count) do
  begin
    if I > 0 then
      Assert.IsTrue(FList[I].idPessoa < FList[I - 1].idPessoa);
  end;
end;

procedure TGBConnectionTestDAOOrmbrOrderBy.Setup;
begin
  FDAO := TGBModelDaoORMBr<TModelPessoa>.NewGenericDAO(TestConnection);
  FQueryParams := TDictionary<String, String>.Create;
  FDAO.QueryParams(FQueryParams);
end;

procedure TGBConnectionTestDAOOrmbrOrderBy.Teardown;
begin
  FreeAndNil(FList);
  FreeAndNil(FQueryParams);
end;

end.
