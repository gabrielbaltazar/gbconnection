unit GBConnection.Test.Base;

interface

uses
  GBConnection.Model.Interfaces,
  GBConnection.Test.Connection,
  ormbr.factory.interfaces,
  System.Classes,
  System.SysUtils;

type TGBConnectionTestBase = class

  private

  protected
    function Connection: IGBConnection;
    function ORMConnection: IDBConnection;

    function NotEmptyConstraintMessage(Campo: String): string;
    function MaxValueConstraintMessage(Campo: String; Tamanho: Integer): string;
end;

implementation

{ TGBConnectionTestBase }

function TGBConnectionTestBase.Connection: IGBConnection;
begin
  result := TestConnection;
end;

function TGBConnectionTestBase.MaxValueConstraintMessage(Campo: String; Tamanho: Integer): string;
begin
  result := Format('O campo [ %s ] n�o pode ter o tamanho maior que %s!', [Campo, Tamanho.ToString]);
end;

function TGBConnectionTestBase.NotEmptyConstraintMessage(Campo: String): string;
begin
  result := Format('O campo [ %s ] n�o pode ser vazio!', [Campo]);
end;

function TGBConnectionTestBase.ORMConnection: IDBConnection;
begin
  Result := TestORMConnection;
end;

end.
