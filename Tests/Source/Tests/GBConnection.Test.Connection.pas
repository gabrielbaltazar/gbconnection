unit GBConnection.Test.Connection;

interface

uses
  GBConnection.Model.Interfaces,
  GBConnection.Model.Factory,
  GBConnection.Dao.ORMBr.Factory,
  ormbr.factory.interfaces,
  System.SysUtils;

var
  TestConnection: IGBConnection;
  TestORMConnection: IDBConnection;
  FQuery: IGBQuery;

implementation

initialization
  ReportMemoryLeaksOnShutdown := True;
  IsConsole := False;

  TestConnection := TGBFactoryConnection.New.createConnection;
  TestConnection
    .Params
      .Driver(gbSQLite)
      .Database(ExtractFilePath(GetModuleName(HInstance)) + 'Data\banco.db3')
    .&End;

  TestConnection.Connect;

  TestORMConnection := TGBConnectionDaoORMBrFactory.New
                        .createConnectionORMBr.GetConnection(TestConnection);

  {$IFDEF DEBUG}
//  UseMonitorLogFile(TestConnection);
//  FQuery := TGBFactoryConnection.New.createQuery(TestConnection);
//  FQuery.SQL('select * from pessoa').Open.Free;
  {$ENDIF}
end.
