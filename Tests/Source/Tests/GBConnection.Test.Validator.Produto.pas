unit GBConnection.Test.Validator.Produto;

interface

uses
  DUnitX.TestFramework,
  GBConnection.Test.Base,
  GBConnection.Dao.Interfaces,
  GBConnection.Dao.ORMBr.Exceptions,
  GBConnection.Dao.ORMBr.Validator,
  GBConnection.Model.Classes,
  System.SysUtils;

type
  [TestFixture]
  TGBConnectionTestValidatorProduto = class(TGBConnectionTestBase)

  private
    FModel : TModelProduto;
    FValidator: IGBModelDaoValidator<TModelProduto>;

    procedure createPadrao;
  public
    constructor create;

    [Setup]
    procedure Setup;

    [TearDown]
    procedure TearDown;

    [Test]
    procedure TestAssertNotExistInsert;

    [Test]
    procedure TestAssertNotExistUpdate;

  end;

implementation

{ TGBConnectionTestValidatorProduto }

constructor TGBConnectionTestValidatorProduto.create;
begin
  FValidator := TGBModelDAOValidator<TModelProduto>.New(ORMConnection);
end;

procedure TGBConnectionTestValidatorProduto.createPadrao;
begin
  FModel := TModelProduto.create;
  FModel.codProduto := '00001';
  FModel.descricao  := 'TESTE';
end;

procedure TGBConnectionTestValidatorProduto.Setup;
begin
  FModel := nil;
end;

procedure TGBConnectionTestValidatorProduto.TearDown;
begin
  FreeAndNil(FModel);
end;

procedure TGBConnectionTestValidatorProduto.TestAssertNotExistInsert;
begin
  createPadrao;
  Assert.WillRaiseWithMessage(
    procedure
    begin
      FValidator.RunValidate(FModel, True);
    end,
    EGBDAOEntityAlreadyExists,
    'Produto 00001 j� existe');
end;

procedure TGBConnectionTestValidatorProduto.TestAssertNotExistUpdate;
begin
  createPadrao;
  Assert.WillNotRaise(
    procedure
    begin
      FValidator.RunValidate(FModel, False);
    end);
end;

end.
