unit GBConnection.Test.DAO.Pessoa;

interface

uses
  DUnitX.TestFramework,
  GBConnection.DAO.Pessoa,
  GBConnection.Model.Pessoa,
  GBConnection.Model.Interfaces,
  GBConnection.Dao.Interfaces,
  GBConnection.Dao.ORMBr,
  ormbr.mapping.exceptions,
  System.SysUtils,
  System.Generics.Collections;

const
  ID_PESSOA = 1;

type TGBConnectionTestDAOPessoa = class

  private
    FDAOPessoa : IGBModelDao<TModelPessoa>;
    FPessoa    : TModelPessoa;
    FLista     : TObjectList<TModelPessoa>;

    procedure inicializaTabela;

    function insereNovaPessoaPadrao: Double;
  protected
    function Connection: IGBConnection; virtual; abstract;

  public
    [Setup]    procedure Setup;
    [TearDown] procedure TearDown;

    [Test] procedure findIDInexistente;
    [Test] procedure findIDExistente;

    [Test] procedure listaTodos;
    [Test] procedure listaVazia;
    [Test] procedure listaComUmResultado;

    [Test] procedure inserirPessoa;
    [Test] procedure inserirPessoaSemNome;

    [Test] procedure excluirPessoa;
    [Test] procedure excluirPessoaQueNaoExiste;

    [Test] procedure alterarPessoa;

    constructor create; virtual;
    destructor  Destroy; override;


end;

implementation

{ TGBConnectionTestDAOPessoa }

procedure TGBConnectionTestDAOPessoa.alterarPessoa;
var
  nomeAtual: string;
  nomeNovo : string;
begin
  FPessoa := FDAOPessoa.findWhere('IDPESSOA = ' + ID_PESSOA.ToString);
  Assert.IsNotNull(FPessoa);

  nomeAtual := FPessoa.nome;
  nomeNovo  := nomeAtual + ' NOVO';

  FDAOPessoa.modify(FPessoa);
  FPessoa.nome := nomeNovo;
  FDAOPessoa.update(FPessoa);
  FPessoa.Free;

  FPessoa := FDAOPessoa.findWhere('IDPESSOA = ' + ID_PESSOA.ToString);
  Assert.AreEqual(nomeNovo, FPessoa.nome);

  FDAOPessoa.modify(FPessoa);
  FPessoa.nome := nomeAtual;

  FDAOPessoa.update(FPessoa);
end;

constructor TGBConnectionTestDAOPessoa.create;
begin
  FDAOPessoa := TGBModelDaoORMBr<TModelPessoa>.NewGenericDAO(Connection);

  inicializaTabela;
end;

destructor TGBConnectionTestDAOPessoa.Destroy;
begin

  inherited;
end;

procedure TGBConnectionTestDAOPessoa.excluirPessoa;
var
  idPessoa: Double;
begin
  idPessoa := insereNovaPessoaPadrao;
  FPessoa  := FDAOPessoa.findWhere('IDPESSOA = ' + idPessoa.ToString);
  Assert.IsNotNull(FPessoa);


  FDAOPessoa.delete(FPessoa);
  FPessoa.Free;
  FPessoa  := FDAOPessoa.findWhere('IDPESSOA = ' + idPessoa.ToString);
  Assert.IsNull(FPessoa);

end;

procedure TGBConnectionTestDAOPessoa.excluirPessoaQueNaoExiste;
begin
  Assert.WillNotRaise(
    procedure
    begin
      FPessoa := TModelPessoa.create;
      FPessoa.idPessoa := -5;
      FPessoa.nome := 'TESTE';

      FDAOPessoa.delete(FPessoa);
    end);
end;

procedure TGBConnectionTestDAOPessoa.findIDExistente;
begin
  FPessoa := FDAOPessoa.findWhere('IDPESSOA = ' + ID_PESSOA.ToString);
  Assert.IsNotNull(FPessoa);
  Assert.AreEqual(FPessoa.idPessoa.ToString, ID_PESSOA.ToString);
end;

procedure TGBConnectionTestDAOPessoa.findIDInexistente;
begin
  FPessoa := FDAOPessoa.findWhere('IDPESSOA = -5');
  Assert.IsNull(FPessoa);
end;

procedure TGBConnectionTestDAOPessoa.inicializaTabela;
begin
  FPessoa := FDAOPessoa.findWhere('IDPESSOA = 1');
  if not Assigned(FPessoa) then
  begin
    FPessoa := TModelPessoa.create;
    FPessoa.nome := 'PESSOA 1';
    FPessoa.numeroDocumento := '12345678901234';

    FDAOPessoa.insert(FPessoa);
  end;
  FreeAndNil(FPessoa);

  FPessoa := FDAOPessoa.findWhere('IDPESSOA = 2');
  if not Assigned(FPessoa) then
  begin
    FPessoa := TModelPessoa.create;
    FPessoa.nome := 'PESSOA 2';
    FPessoa.numeroDocumento := '12345678901234';

    FDAOPessoa.insert(FPessoa);
  end;
  FreeAndNil(FPessoa);
end;

function TGBConnectionTestDAOPessoa.insereNovaPessoaPadrao: Double;
begin
  FPessoa := TModelPessoa.create;
  FPessoa.nome := 'PESSOA PADRAO';
  FPessoa.numeroDocumento := '00000000000000';

  FDAOPessoa.insert(FPessoa);
  Result := FPessoa.idPessoa;
  FreeAndNil(FPessoa);
end;

procedure TGBConnectionTestDAOPessoa.inserirPessoa;
var
  idPessoa: Double;
begin
  idPessoa := insereNovaPessoaPadrao;
  FPessoa  := FDAOPessoa.findWhere('IDPESSOA = ' + idPessoa.ToString);

  Assert.IsNotNull(FPessoa);
  FDAOPessoa.delete(FPessoa);
end;

procedure TGBConnectionTestDAOPessoa.inserirPessoaSemNome;
begin
  Assert.WillRaise(
    procedure
    begin
      FPessoa := TModelPessoa.create;
      FPessoa.numeroDocumento := '12345678901234';

      FDAOPessoa.insert(FPessoa);
    end,
    ENotEmptyConstraint
  );
end;

procedure TGBConnectionTestDAOPessoa.listaComUmResultado;
begin
  FLista := FDAOPessoa.listWhere('IDPESSOA = ' + ID_PESSOA.ToString);
  Assert.AreEqual(1, FLista.Count);
end;

procedure TGBConnectionTestDAOPessoa.listaTodos;
begin
  FLista := FDAOPessoa.listAll;
  Assert.IsTrue(FLista.Count > 1);
end;

procedure TGBConnectionTestDAOPessoa.listaVazia;
begin
  FLista := FDAOPessoa.listWhere('IDPESSOA = -5');
  Assert.IsNotNull(FLista);
  Assert.IsTrue(FLista.Count = 0);
end;

procedure TGBConnectionTestDAOPessoa.Setup;
begin
  FPessoa := nil;
  FLista  := nil;
end;

procedure TGBConnectionTestDAOPessoa.TearDown;
begin
  FPessoa.Free;
  FLista.Free;
end;

end.
