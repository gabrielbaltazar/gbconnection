unit GBConnection.Test.Base.SQLite.Connection;

interface

uses
  DUnitX.TestFramework,
  GBConnection.Test.Base.SQLite;

type
  TGBConnectionTestSQLiteConnection = class(TGBConnectionTestSQLite)
  public
    [Test] procedure Connect;

  end;

implementation

{ TGBConnectionTestSQLiteConnection }

procedure TGBConnectionTestSQLiteConnection.Connect;
begin
  Self.Connection;
end;

end.
