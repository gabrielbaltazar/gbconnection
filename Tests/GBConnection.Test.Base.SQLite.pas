unit GBConnection.Test.Base.SQLite;

interface

uses
  GBConnection.Test.Base,
  GBConnection.Model.Interfaces,
  GBConnection.Model.Factory,
  ormbr.dml.generator.sqlite,
  GBConnection.Monitor;

type TGBConnectionTestSQLite = class(TGBConnectionTestBase)

  public
    function Connection: IGBConnection;

end;

var
  FConnection: IGBConnection;

implementation

{ TGBConnectionTestSQLite }

function TGBConnectionTestSQLite.Connection: IGBConnection;
begin
  if not Assigned(FConnection) then
  begin
    FConnection := TGBFactoryConnection.New.createConnection;
    FConnection
      .Params
        .Driver(gbSQLite)
        .Database('Data\banco.db3')
      .&End
      .Connect;
  end;

  result := FConnection;
end;

end.
