unit GBConnection.Test.Register;

interface

uses
  // Oracle
  GBConnection.Test.Base.Oracle.Connection,

  // SQLite
  GBConnection.Test.Base.SQLite.Connection,
  GBConnection.Test.SQLite.Pessoa,

  // Postgresql
  GBConnection.Test.Base.Postgresql.Connection,
  GBConnection.Test.Postgresql.Pessoa,

  // MySql
  GBConnection.Test.Base.MySql.Connection,
  GBConnection.Test.MySql.Pessoa,

  DUnitX.TestFramework;

implementation

initialization

  // Oracle
//  TDUnitX.RegisterTestFixture(TGBConnectionTestOracleConnection);

  // SQLite
  TDUnitX.RegisterTestFixture(TGBConnectionTestSQLiteConnection);
  TDUnitX.RegisterTestFixture(TGBConnectionTestSQLitePessoa);

  // MySQL
  TDUnitX.RegisterTestFixture(TGBConnectionTestMySqlConnection);
  TDUnitX.RegisterTestFixture(TGBConnectionTestMySqlPessoa);

  // Postgresql
//  TDUnitX.RegisterTestFixture(TGBConnectionTestPostgresqlConnection);
//  TDUnitX.RegisterTestFixture(TGBConnectionTestPostgresqlPessoa);

end.
