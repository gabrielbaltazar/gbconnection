unit GBConnection.Test.SQLite.Pessoa;

interface

uses
  DUnitX.TestFramework,
  GBConnection.Model.Interfaces,
  GBConnection.Test.Base.SQLite,
  GBConnection.Test.DAO.Pessoa,
  System.SysUtils;

type TGBConnectionTestSQLitePessoa = class(TGBConnectionTestDAOPessoa)

  private
    FDataBase: TGBConnectionTestSQLite;

  protected
    function Connection: IGBConnection; override;
  public
    constructor create; override;
    destructor  Destroy; override;

end;

implementation

{ TGBConnectionTestSQLitePessoa }

function TGBConnectionTestSQLitePessoa.Connection: IGBConnection;
begin
  result := FDataBase.Connection;
end;

constructor TGBConnectionTestSQLitePessoa.create;
begin
  inherited;
  FDataBase := TGBConnectionTestSQLite.Create;
end;

destructor TGBConnectionTestSQLitePessoa.Destroy;
begin
  FDataBase.Free;
  inherited;
end;

end.
