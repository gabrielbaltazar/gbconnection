unit GBConnection.Test.MySql.Pessoa;

interface

uses
  DUnitX.TestFramework,
  GBConnection.Model.Interfaces,
  GBConnection.Test.Base.MySql,
  GBConnection.Test.DAO.Pessoa,
  System.SysUtils;

type TGBConnectionTestMySqlPessoa = class(TGBConnectionTestDAOPessoa)

  private
    FDataBase: TGBConnectionTestMySql;

  protected
    function Connection: IGBConnection; override;
  public
    constructor create; override;
    destructor  Destroy; override;

end;

implementation

{ TGBConnectionTestMySqlPessoa }

function TGBConnectionTestMySqlPessoa.Connection: IGBConnection;
begin
  result := FDataBase.Connection;
end;

constructor TGBConnectionTestMySqlPessoa.create;
begin
  inherited;
  FDataBase := TGBConnectionTestMySql.Create;
end;

destructor TGBConnectionTestMySqlPessoa.Destroy;
begin
  FDataBase.Free;
  inherited;
end;

end.
