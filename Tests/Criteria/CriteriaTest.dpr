program CriteriaTest;

{$IFNDEF TESTINSIGHT}
{$APPTYPE CONSOLE}
{$ENDIF}
{$STRONGLINKTYPES ON}
uses
  System.SysUtils,
  {$IFDEF TESTINSIGHT}
  TestInsight.DUnitX,
  {$ELSE}
  DUnitX.Loggers.Console,
  {$ENDIF }
  DUnitX.TestFramework,
  GBConnection.Criteria.Interfaces in '..\..\Source\GBConnection.Criteria.Interfaces.pas',
  GBConnection.Criteria.Operators in '..\..\Source\GBConnection.Criteria.Operators.pas',
  GBConnection.Criteria in '..\..\Source\GBConnection.Criteria.pas',
  GBConnection.Criteria.Types in '..\..\Source\GBConnection.Criteria.Types.pas',
  GBConnection.Criteria.Utils in '..\..\Source\GBConnection.Criteria.Utils.pas',
  GBConnection.Test.Criteria.WhereOperator in 'GBConnection.Test.Criteria.WhereOperator.pas',
  GBConnection.Test.Criteria.AndOperators in 'GBConnection.Test.Criteria.AndOperators.pas',
  GBConnection.Test.Criteria.OrOperators in 'GBConnection.Test.Criteria.OrOperators.pas',
  GBConnection.Test.Criteria.DateTimeFormat in 'GBConnection.Test.Criteria.DateTimeFormat.pas',
  GBConnection.Test.Criteria.NullOperators in 'GBConnection.Test.Criteria.NullOperators.pas',
  GBConnection.Test.Criteria.InOperators in 'GBConnection.Test.Criteria.InOperators.pas';

begin
  IsConsole := False;
  ReportMemoryLeaksOnShutdown := True;

  TestInsight.DUnitX.RunRegisteredTests;

end.
