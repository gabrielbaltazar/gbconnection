unit GBConnection.Test.Criteria.OrOperators;

interface

uses
  DUnitX.TestFramework,
  GBConnection.Criteria.Interfaces,
  GBConnection.Criteria,
  System.DateUtils,
  System.SysUtils;

type
  [TestFixture]
  TTestCriteriaOrOperators = class

  protected
    FCriteria: IGBConnectionCriteria;
    FSql: string;

  public
    [Setup]
    procedure Setup;

    [Test]
    procedure TestOrStringValue;

    [Test]
    procedure TestOrIntegerValue;

    [Test]
    procedure TestOrDoubleValue;

    [Test]
    procedure TestOrDateValue;

    [Test]
    procedure TestOrDateTimeValue;

    [Test]
    procedure TestOrNotStringValue;

    [Test]
    procedure TestOrNotIntegerValue;

    [Test]
    procedure TestOrNotDoubleValue;

    [Test]
    procedure TestOrNotDateValue;

    [Test]
    procedure TestOrNotDateTimeValue;
  end;

implementation

{ TTestCriteriaOrOperators }

procedure TTestCriteriaOrOperators.Setup;
begin
  FCriteria := createCriteria;
  FSql := EmptyStr;
end;

procedure TTestCriteriaOrOperators.TestOrDateTimeValue;
begin
  FCriteria
    .Where('column1').Equal(1)
    .&Or('column2').EqualDt(EncodeDateTime(2021, 10, 12, 8, 30, 20, 25), True);

  FSql := FCriteria.AsString;

  Assert.AreEqual(' column1 = 1 or column2 = ''12/10/2021 08:30:20''', FSql);
end;

procedure TTestCriteriaOrOperators.TestOrDateValue;
begin
  FCriteria
    .Where('column1').Equal(1)
    .&Or('column2').EqualDt(EncodeDateTime(2021, 10, 12, 8, 30, 20, 25));

  FSql := FCriteria.AsString;

  Assert.AreEqual(' column1 = 1 or column2 = ''12/10/2021''', FSql);
end;

procedure TTestCriteriaOrOperators.TestOrDoubleValue;
begin
  FCriteria
    .Where('column1').Equal(1)
    .&Or('column2').Equal(12.34);

  FSql := FCriteria.AsString;

  Assert.AreEqual(' column1 = 1 or column2 = 12.34', FSql);
end;

procedure TTestCriteriaOrOperators.TestOrIntegerValue;
begin
  FCriteria
    .Where('column1').Equal(1)
    .&Or('column2').Equal(1234);

  FSql := FCriteria.AsString;

  Assert.AreEqual(' column1 = 1 or column2 = 1234', FSql);
end;

procedure TTestCriteriaOrOperators.TestOrNotDateTimeValue;
begin
  FCriteria
    .Where('column1').Equal(1)
    .&Or('column2').NotEqualDt(EncodeDateTime(2021, 10, 12, 8, 30, 20, 25), True);

  FSql := FCriteria.AsString;

  Assert.AreEqual(' column1 = 1 or column2 <> ''12/10/2021 08:30:20''', FSql);
end;

procedure TTestCriteriaOrOperators.TestOrNotDateValue;
begin
  FCriteria
    .Where('column1').Equal(1)
    .&Or('column2').NotEqualDt(EncodeDateTime(2021, 10, 12, 8, 30, 20, 25));

  FSql := FCriteria.AsString;

  Assert.AreEqual(' column1 = 1 or column2 <> ''12/10/2021''', FSql);
end;

procedure TTestCriteriaOrOperators.TestOrNotDoubleValue;
begin
  FCriteria
    .Where('column1').Equal(1)
    .&Or('column2').NotEqual(12.34);

  FSql := FCriteria.AsString;

  Assert.AreEqual(' column1 = 1 or column2 <> 12.34', FSql);
end;

procedure TTestCriteriaOrOperators.TestOrNotIntegerValue;
begin
  FCriteria
    .Where('column1').Equal(1)
    .&Or('column2').NotEqual(1234);

  FSql := FCriteria.AsString;

  Assert.AreEqual(' column1 = 1 or column2 <> 1234', FSql);
end;

procedure TTestCriteriaOrOperators.TestOrNotStringValue;
begin
  FCriteria
    .Where('column1').Equal(1)
    .&Or('column2').NotEqual('1234');

  FSql := FCriteria.AsString;

  Assert.AreEqual(' column1 = 1 or column2 <> ''1234''', FSql);
end;

procedure TTestCriteriaOrOperators.TestOrStringValue;
begin
  FCriteria
    .Where('column1').Equal(1)
    .&Or('column2').Equal('1234');

  FSql := FCriteria.AsString;

  Assert.AreEqual(' column1 = 1 or column2 = ''1234''', FSql);
end;

end.

