unit GBConnection.Test.Criteria.AndOperators;

interface

uses
  DUnitX.TestFramework,
  GBConnection.Criteria.Interfaces,
  GBConnection.Criteria,
  System.DateUtils,
  System.SysUtils;

type
  [TestFixture]
  TTestCriteriaAndOperators = class

  protected
    FCriteria: IGBConnectionCriteria;
    FSql: string;

  public
    [Setup]
    procedure Setup;

    [Test]
    procedure TestAndStringValue;

    [Test]
    procedure TestAndIntegerValue;

    [Test]
    procedure TestAndDoubleValue;

    [Test]
    procedure TestAndDateValue;

    [Test]
    procedure TestAndDateTimeValue;

    [Test]
    procedure TestAndNotStringValue;

    [Test]
    procedure TestAndNotIntegerValue;

    [Test]
    procedure TestAndNotDoubleValue;

    [Test]
    procedure TestAndNotDateValue;

    [Test]
    procedure TestAndNotDateTimeValue;
  end;

implementation

{ TTestCriteriaAndOperators }

procedure TTestCriteriaAndOperators.Setup;
begin
  FCriteria := createCriteria;
  FSql := EmptyStr;
end;

procedure TTestCriteriaAndOperators.TestAndDateTimeValue;
begin
  FCriteria
    .Where('column1').Equal(1)
    .&And('column2').EqualDt(EncodeDateTime(2021, 10, 12, 8, 30, 20, 25), True);

  FSql := FCriteria.AsString;

  Assert.AreEqual(' column1 = 1 and column2 = ''12/10/2021 08:30:20''', FSql);
end;

procedure TTestCriteriaAndOperators.TestAndDateValue;
begin
  FCriteria
    .Where('column1').Equal(1)
    .&And('column2').EqualDt(EncodeDateTime(2021, 10, 12, 8, 30, 20, 25));

  FSql := FCriteria.AsString;

  Assert.AreEqual(' column1 = 1 and column2 = ''12/10/2021''', FSql);
end;

procedure TTestCriteriaAndOperators.TestAndDoubleValue;
begin
  FCriteria
    .Where('column1').Equal(1)
    .&And('column2').Equal(12.34);

  FSql := FCriteria.AsString;

  Assert.AreEqual(' column1 = 1 and column2 = 12.34', FSql);
end;

procedure TTestCriteriaAndOperators.TestAndIntegerValue;
begin
  FCriteria
    .Where('column1').Equal(1)
    .&And('column2').Equal(1234);

  FSql := FCriteria.AsString;

  Assert.AreEqual(' column1 = 1 and column2 = 1234', FSql);
end;

procedure TTestCriteriaAndOperators.TestAndNotDateTimeValue;
begin
  FCriteria
    .Where('column1').Equal(1)
    .&And('column2').NotEqualDt(EncodeDateTime(2021, 10, 12, 8, 30, 20, 25), True);

  FSql := FCriteria.AsString;

  Assert.AreEqual(' column1 = 1 and column2 <> ''12/10/2021 08:30:20''', FSql);
end;

procedure TTestCriteriaAndOperators.TestAndNotDateValue;
begin
  FCriteria
    .Where('column1').Equal(1)
    .&And('column2').NotEqualDt(EncodeDateTime(2021, 10, 12, 8, 30, 20, 25));

  FSql := FCriteria.AsString;

  Assert.AreEqual(' column1 = 1 and column2 <> ''12/10/2021''', FSql);
end;

procedure TTestCriteriaAndOperators.TestAndNotDoubleValue;
begin
  FCriteria
    .Where('column1').Equal(1)
    .&And('column2').NotEqual(12.34);

  FSql := FCriteria.AsString;

  Assert.AreEqual(' column1 = 1 and column2 <> 12.34', FSql);
end;

procedure TTestCriteriaAndOperators.TestAndNotIntegerValue;
begin
  FCriteria
    .Where('column1').Equal(1)
    .&And('column2').NotEqual(1234);

  FSql := FCriteria.AsString;

  Assert.AreEqual(' column1 = 1 and column2 <> 1234', FSql);
end;

procedure TTestCriteriaAndOperators.TestAndNotStringValue;
begin
  FCriteria
    .Where('column1').Equal(1)
    .&And('column2').NotEqual('1234');

  FSql := FCriteria.AsString;

  Assert.AreEqual(' column1 = 1 and column2 <> ''1234''', FSql);
end;

procedure TTestCriteriaAndOperators.TestAndStringValue;
begin
  FCriteria
    .Where('column1').Equal(1)
    .&And('column2').Equal('1234');

  FSql := FCriteria.AsString;

  Assert.AreEqual(' column1 = 1 and column2 = ''1234''', FSql);
end;

end.
