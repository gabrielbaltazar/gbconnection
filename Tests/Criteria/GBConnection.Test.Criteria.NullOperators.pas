unit GBConnection.Test.Criteria.NullOperators;

interface

uses
  DUnitX.TestFramework,
  GBConnection.Criteria.Interfaces,
  GBConnection.Criteria,
  System.DateUtils,
  System.SysUtils;

type
  [TestFixture]
  TTestCriteriaNullOperators = class

  protected
    FCriteria: IGBConnectionCriteria;
    FSql: string;

  public
    [Setup]
    procedure Setup;

    [Test]
    procedure ColumnIsNull;

    [Test]
    procedure ColumnIsNotNull;

    [Test]
    procedure ColumnIsNullIfEmpty;

    [Test]
    procedure ColumnIsNullIfEmptyInteger;

    [Test]
    procedure ColumnIsNullIfEmptyIntegerWithNotNullField;

    [Test]
    procedure ColumnIsNullIfEmptyWithNotNullField;
  end;

implementation

{ TTestCriteriaNullOperators }

procedure TTestCriteriaNullOperators.ColumnIsNotNull;
begin
  FCriteria
    .Where('column1').IsNotNull;

  FSql := FCriteria.AsString;

  Assert.AreEqual(' column1 is not null', FSql);
end;

procedure TTestCriteriaNullOperators.ColumnIsNull;
begin
  FCriteria
    .Where('column1').IsNull;

  FSql := FCriteria.AsString;

  Assert.AreEqual(' column1 is null', FSql);
end;

procedure TTestCriteriaNullOperators.ColumnIsNullIfEmpty;
begin
  FCriteria
    .Where('column1').Equal(EmptyStr).IsNullIfEmpty;

  FSql := FCriteria.AsString;

  Assert.AreEqual(' column1 is null', FSql);
end;

procedure TTestCriteriaNullOperators.ColumnIsNullIfEmptyInteger;
begin
  FCriteria
    .Where('column1').Equal(0).IsNullIfEmpty;

  FSql := FCriteria.AsString;

  Assert.AreEqual(' column1 is null', FSql);
end;

procedure TTestCriteriaNullOperators.ColumnIsNullIfEmptyIntegerWithNotNullField;
begin
  FCriteria
    .Where('column1').Equal(1).IsNullIfEmpty;

  FSql := FCriteria.AsString;

  Assert.AreEqual(' column1 = 1', FSql);
end;

procedure TTestCriteriaNullOperators.ColumnIsNullIfEmptyWithNotNullField;
begin
  FCriteria
    .Where('column1').Equal('1').IsNullIfEmpty;

  FSql := FCriteria.AsString;

  Assert.AreEqual(' column1 = ''1''', FSql);
end;

procedure TTestCriteriaNullOperators.Setup;
begin
  FCriteria := createCriteria;
  FSql := EmptyStr;
end;

end.
