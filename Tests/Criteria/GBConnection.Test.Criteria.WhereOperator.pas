unit GBConnection.Test.Criteria.WhereOperator;

interface

uses
  DUnitX.TestFramework,
  GBConnection.Criteria.Interfaces,
  GBConnection.Criteria,
  System.DateUtils,
  System.SysUtils;

type
  [TestFixture]
  TTestCriteriaWhereOperators = class

  protected
    FCriteria: IGBConnectionCriteria;
    FSql: string;

  public
    [Setup]
    procedure Setup;

    [Test]
    procedure TestWhereStringValue;

    [Test]
    procedure TestWhereIntegerValue;

    [Test]
    procedure TestWhereDoubleValue;

    [Test]
    procedure TestWhereDateValue;

    [Test]
    procedure TestWhereDateTimeValue;

    [Test]
    procedure TestWhereNotStringValue;

    [Test]
    procedure TestWhereNotIntegerValue;

    [Test]
    procedure TestWhereNotDoubleValue;

    [Test]
    procedure TestWhereNotDateValue;

    [Test]
    procedure TestWhereNotDateTimeValue;

end;

implementation

{ TTestCriteriaOperators }

procedure TTestCriteriaWhereOperators.Setup;
begin
  FCriteria := createCriteria;
  FSql := EmptyStr;
end;

procedure TTestCriteriaWhereOperators.TestWhereDateTimeValue;
begin
  FCriteria.Where('column1').EqualDt(EncodeDateTime(2021, 10, 12, 8, 30, 20, 20), True);
  FSql := FCriteria.AsString;

  Assert.AreEqual(' column1 = ''12/10/2021 08:30:20''', FSql);
end;

procedure TTestCriteriaWhereOperators.TestWhereDateValue;
begin
  FCriteria.Where('column1').EqualDt(EncodeDateTime(2021, 10, 12, 8, 30, 20, 20));
  FSql := FCriteria.AsString;

  Assert.AreEqual(' column1 = ''12/10/2021''', FSql);
end;

procedure TTestCriteriaWhereOperators.TestWhereDoubleValue;
begin
  FCriteria.Where('column1').Equal(12.34);
  FSql := FCriteria.AsString;

  Assert.AreEqual(' column1 = 12.34', FSql);
end;

procedure TTestCriteriaWhereOperators.TestWhereIntegerValue;
begin
  FCriteria.Where('column1').Equal(1234);
  FSql := FCriteria.AsString;

  Assert.AreEqual(' column1 = 1234', FSql);
end;

procedure TTestCriteriaWhereOperators.TestWhereNotDateTimeValue;
begin
  FCriteria.Where('column1').NotEqualDt(EncodeDateTime(2021, 10, 12, 8, 30, 20, 20), True);
  FSql := FCriteria.AsString;

  Assert.AreEqual(' column1 <> ''12/10/2021 08:30:20''', FSql);
end;

procedure TTestCriteriaWhereOperators.TestWhereNotDateValue;
begin
  FCriteria.Where('column1').NotEqualDt(EncodeDateTime(2021, 10, 12, 8, 30, 20, 20));
  FSql := FCriteria.AsString;

  Assert.AreEqual(' column1 <> ''12/10/2021''', FSql);
end;

procedure TTestCriteriaWhereOperators.TestWhereNotDoubleValue;
begin
  FCriteria.Where('column1').NotEqual(12.34);
  FSql := FCriteria.AsString;

  Assert.AreEqual(' column1 <> 12.34', FSql);
end;

procedure TTestCriteriaWhereOperators.TestWhereNotIntegerValue;
begin
  FCriteria.Where('column1').NotEqual(1234);
  FSql := FCriteria.AsString;

  Assert.AreEqual(' column1 <> 1234', FSql);
end;

procedure TTestCriteriaWhereOperators.TestWhereNotStringValue;
begin
  FCriteria.Where('column1').NotEqual('1234');
  FSql := FCriteria.AsString;

  Assert.AreEqual(' column1 <> ''1234''', FSql);
end;

procedure TTestCriteriaWhereOperators.TestWhereStringValue;
begin
  FCriteria.Where('column1').Equal('1234');
  FSql := FCriteria.AsString;

  Assert.AreEqual(' column1 = ''1234''', FSql);
end;

end.
