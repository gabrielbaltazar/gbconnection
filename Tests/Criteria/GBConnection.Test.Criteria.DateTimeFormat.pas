unit GBConnection.Test.Criteria.DateTimeFormat;

interface

uses
  DUnitX.TestFramework,
  GBConnection.Criteria.Interfaces,
  GBConnection.Criteria,
  System.DateUtils,
  System.SysUtils;

type
  [TestFixture]
  TTestCriteriaDateTimeFormat = class

  protected
    FCriteria: IGBConnectionCriteria;
    FDate: TDateTime;
    FSql: string;

  public
    [Setup]
    procedure Setup;

    [Test]
    procedure DateEqualDtDefaultFormat;

    [Test]
    procedure DateTimeEqualDtDefaultFormat;

    [Test]
    procedure DateEqualDtCustomFormat;

    [Test]
    procedure DateTimeEqualDtCustomFormat;

    [Test]
    procedure DateGreaterThanDefaultFormat;

    [Test]
    procedure DateGreaterThanCustomFormat;

    [Test]
    procedure DateGreaterEqThanDefaultFormat;

    [Test]
    procedure DateGreaterEqThanCustomFormat;

    [Test]
    procedure DateLessThanDefaultFormat;

    [Test]
    procedure DateLessThanCustomFormat;

    [Test]
    procedure DateLessEqThanDefaultFormat;

    [Test]
    procedure DateLessEqThanCustomFormat;
  end;

implementation

{ TTestCriteriaDateTimeFormat }

procedure TTestCriteriaDateTimeFormat.DateEqualDtCustomFormat;
begin
  FDate := EncodeDateTime(2021, 10, 12, 8, 25, 53, 2);
  FCriteria
    .DateFormat('yyyy-MM-dd')
    .DateTimeFormat('yyyy-MM-dd hh:mm:ss')
    .Where('column1').EqualDt(FDate);

  FSql := FCriteria.AsString;

  Assert.AreEqual(' column1 = ''2021-10-12''', FSql);
end;

procedure TTestCriteriaDateTimeFormat.DateEqualDtDefaultFormat;
begin
  FDate := EncodeDateTime(2021, 10, 12, 8, 25, 53, 2);
  FCriteria
    .Where('column1').EqualDt(FDate);

  FSql := FCriteria.AsString;

  Assert.AreEqual(' column1 = ''12/10/2021''', FSql);
end;

procedure TTestCriteriaDateTimeFormat.DateGreaterEqThanCustomFormat;
begin
  FDate := EncodeDateTime(2021, 10, 12, 8, 25, 53, 2);
  FCriteria
    .DateFormat('yyyy-MM-dd')
    .DateTimeFormat('yyyy-MM-dd hh:mm:ss')
    .Where('column1').GreaterEqThanDt(FDate);

  FSql := FCriteria.AsString;

  Assert.AreEqual(' column1 >= ''2021-10-12''', FSql);
end;

procedure TTestCriteriaDateTimeFormat.DateGreaterEqThanDefaultFormat;
begin
  FDate := EncodeDateTime(2021, 10, 12, 8, 25, 53, 2);
  FCriteria
    .Where('column1').GreaterEqThanDt(FDate);

  FSql := FCriteria.AsString;

  Assert.AreEqual(' column1 >= ''12/10/2021''', FSql);
end;

procedure TTestCriteriaDateTimeFormat.DateGreaterThanCustomFormat;
begin
  FDate := EncodeDateTime(2021, 10, 12, 8, 25, 53, 2);
  FCriteria
    .DateFormat('yyyy-MM-dd')
    .DateTimeFormat('yyyy-MM-dd hh:mm:ss')
    .Where('column1').GreaterThanDt(FDate);

  FSql := FCriteria.AsString;

  Assert.AreEqual(' column1 > ''2021-10-12''', FSql);
end;

procedure TTestCriteriaDateTimeFormat.DateGreaterThanDefaultFormat;
begin
  FDate := EncodeDateTime(2021, 10, 12, 8, 25, 53, 2);
  FCriteria
    .Where('column1').GreaterThanDt(FDate);

  FSql := FCriteria.AsString;

  Assert.AreEqual(' column1 > ''12/10/2021''', FSql);
end;

procedure TTestCriteriaDateTimeFormat.DateLessEqThanCustomFormat;
begin
  FDate := EncodeDateTime(2021, 10, 12, 8, 25, 53, 2);
  FCriteria
    .DateFormat('yyyy-MM-dd')
    .DateTimeFormat('yyyy-MM-dd hh:mm:ss')
    .Where('column1').LessEqThanDt(FDate);

  FSql := FCriteria.AsString;

  Assert.AreEqual(' column1 <= ''2021-10-12''', FSql);
end;

procedure TTestCriteriaDateTimeFormat.DateLessEqThanDefaultFormat;
begin
  FDate := EncodeDateTime(2021, 10, 12, 8, 25, 53, 2);
  FCriteria
    .Where('column1').LessEqThanDt(FDate);

  FSql := FCriteria.AsString;

  Assert.AreEqual(' column1 <= ''12/10/2021''', FSql);
end;

procedure TTestCriteriaDateTimeFormat.DateLessThanCustomFormat;
begin
  FDate := EncodeDateTime(2021, 10, 12, 8, 25, 53, 2);
  FCriteria
    .DateFormat('yyyy-MM-dd')
    .DateTimeFormat('yyyy-MM-dd hh:mm:ss')
    .Where('column1').LessThanDt(FDate);

  FSql := FCriteria.AsString;

  Assert.AreEqual(' column1 < ''2021-10-12''', FSql);
end;

procedure TTestCriteriaDateTimeFormat.DateLessThanDefaultFormat;
begin
  FDate := EncodeDateTime(2021, 10, 12, 8, 25, 53, 2);
  FCriteria
    .Where('column1').LessThanDt(FDate);

  FSql := FCriteria.AsString;

  Assert.AreEqual(' column1 < ''12/10/2021''', FSql);
end;

procedure TTestCriteriaDateTimeFormat.DateTimeEqualDtCustomFormat;
begin
  FDate := EncodeDateTime(2021, 10, 12, 8, 25, 53, 2);
  FCriteria
    .DateFormat('yyyy-MM-dd')
    .DateTimeFormat('yyyy-MM-dd hh:mm:ss')
    .Where('column1').EqualDt(FDate, True);

  FSql := FCriteria.AsString;

  Assert.AreEqual(' column1 = ''2021-10-12 08:25:53''', FSql);
end;

procedure TTestCriteriaDateTimeFormat.DateTimeEqualDtDefaultFormat;
begin
  FDate := EncodeDateTime(2021, 10, 12, 8, 25, 53, 2);
  FCriteria
    .Where('column1').EqualDt(FDate, True);

  FSql := FCriteria.AsString;

  Assert.AreEqual(' column1 = ''12/10/2021 08:25:53''', FSql);
end;

procedure TTestCriteriaDateTimeFormat.Setup;
begin
  FCriteria := createCriteria;
  FDate := 0;
  FSql := EmptyStr;
end;

end.
