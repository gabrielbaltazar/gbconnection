unit GBConnection.Test.Criteria.InOperators;

interface

uses
  DUnitX.TestFramework,
  GBConnection.Criteria.Interfaces,
  GBConnection.Criteria,
  System.DateUtils,
  System.SysUtils;

type
  [TestFixture]
  TTestCriteriaInOperators = class

  protected
    FCriteria: IGBConnectionCriteria;
    FSql: string;

  public
    [Setup]
    procedure Setup;

    [Test]
    procedure TestInOneIntegerValue;

    [Test]
    procedure TestInTwoIntegerValues;

    [Test]
    procedure TestInManyIntegerValues;

    [Test]
    procedure TestNotInOneIntegerValue;

    [Test]
    procedure TestNotInTwoIntegerValues;

    [Test]
    procedure TestNotInManyIntegerValues;

    [Test]
    procedure TestInOneStringValue;

    [Test]
    procedure TestInTwoStringValues;

    [Test]
    procedure TestInManyStringValues;

    [Test]
    procedure TestNotInOneStringValue;

    [Test]
    procedure TestNotInTwoStringValues;

    [Test]
    procedure TestNotInManyStringValues;

  end;

implementation

{ TTestCriteriaInOperators }

procedure TTestCriteriaInOperators.Setup;
begin
  FCriteria := createCriteria;
  FSql := EmptyStr;
end;

procedure TTestCriteriaInOperators.TestInManyIntegerValues;
begin
  FCriteria
    .Where('column1').IsIn([1, 2, 3]);

  FSql := FCriteria.AsString;

  Assert.AreEqual(' column1 in (1, 2, 3)', FSql);
end;

procedure TTestCriteriaInOperators.TestInManyStringValues;
begin
  FCriteria
    .Where('column1').IsIn(['1', '2', '3']);

  FSql := FCriteria.AsString;

  Assert.AreEqual(' column1 in (''1'', ''2'', ''3'')', FSql);
end;

procedure TTestCriteriaInOperators.TestInOneIntegerValue;
begin
  FCriteria
    .Where('column1').IsIn([1]);

  FSql := FCriteria.AsString;

  Assert.AreEqual(' column1 in (1)', FSql);
end;

procedure TTestCriteriaInOperators.TestInOneStringValue;
begin
  FCriteria
    .Where('column1').IsIn(['1']);

  FSql := FCriteria.AsString;

  Assert.AreEqual(' column1 in (''1'')', FSql);
end;

procedure TTestCriteriaInOperators.TestInTwoIntegerValues;
begin
  FCriteria
    .Where('column1').IsIn([1, 2]);

  FSql := FCriteria.AsString;

  Assert.AreEqual(' column1 in (1, 2)', FSql);
end;

procedure TTestCriteriaInOperators.TestInTwoStringValues;
begin
  FCriteria
    .Where('column1').IsIn(['1', '2']);

  FSql := FCriteria.AsString;

  Assert.AreEqual(' column1 in (''1'', ''2'')', FSql);
end;

procedure TTestCriteriaInOperators.TestNotInManyIntegerValues;
begin
  FCriteria
    .Where('column1').IsNotIn([1, 2, 3]);

  FSql := FCriteria.AsString;

  Assert.AreEqual(' column1 not in (1, 2, 3)', FSql);
end;

procedure TTestCriteriaInOperators.TestNotInManyStringValues;
begin
  FCriteria
    .Where('column1').IsNotIn(['1', '2', '3']);

  FSql := FCriteria.AsString;

  Assert.AreEqual(' column1 not in (''1'', ''2'', ''3'')', FSql);
end;

procedure TTestCriteriaInOperators.TestNotInOneIntegerValue;
begin
  FCriteria
    .Where('column1').IsNotIn([1]);

  FSql := FCriteria.AsString;

  Assert.AreEqual(' column1 not in (1)', FSql);
end;

procedure TTestCriteriaInOperators.TestNotInOneStringValue;
begin
  FCriteria
    .Where('column1').IsNotIn(['1']);

  FSql := FCriteria.AsString;

  Assert.AreEqual(' column1 not in (''1'')', FSql);
end;

procedure TTestCriteriaInOperators.TestNotInTwoIntegerValues;
begin
  FCriteria
    .Where('column1').IsNotIn([1, 2]);

  FSql := FCriteria.AsString;

  Assert.AreEqual(' column1 not in (1, 2)', FSql);
end;

procedure TTestCriteriaInOperators.TestNotInTwoStringValues;
begin
  FCriteria
    .Where('column1').IsNotIn(['1', '2']);

  FSql := FCriteria.AsString;

  Assert.AreEqual(' column1 not in (''1'', ''2'')', FSql);
end;

end.
