unit GBConnection.Test.Postgres;

interface

uses
  DUnitX.TestFramework,
  GBConnection.Model.Interfaces;

type
  [TestFixture]
  TGBConnectionTestPostgres = class
  private
    FConnection: IGBConnection;

  public
    [Setup]
    procedure Setup;

    [Test]
    procedure Connect;

  end;

implementation

{ TGBConnectionTestPostgres }

procedure TGBConnectionTestPostgres.Connect;
begin
  Assert.WillNotRaiseAny(
    procedure
    begin
      FConnection.Params
        .Server('127.0.0.1')
        .Database('postgres')
        .UserName('postgres')
        .Password('postgres')
        .Port(5432)
        .Schema('public')
        .Driver(gbPostgres)
      .&End
      .Connect;
    end);
end;

procedure TGBConnectionTestPostgres.Setup;
begin
  FConnection := FactoryConnection.createConnection;
end;

end.
