unit GBConnection.Test.Postgresql.Pessoa;

interface

uses
  DUnitX.TestFramework,
  GBConnection.Model.Interfaces,
  GBConnection.Test.Base.Postgresql,
  GBConnection.Test.DAO.Pessoa,
  System.SysUtils;

type TGBConnectionTestPostgresqlPessoa = class(TGBConnectionTestDAOPessoa)

  private
    FDataBase: TGBConnectionTestBasePostgresql;

  protected
    function Connection: IGBConnection; override;
  public
    constructor create; override;
    destructor  Destroy; override;

end;

implementation

{ TGBConnectionTestPostgresqlPessoa }

function TGBConnectionTestPostgresqlPessoa.Connection: IGBConnection;
begin
  result := FDataBase.Connection;
end;

constructor TGBConnectionTestPostgresqlPessoa.create;
begin
  inherited;
  FDataBase := TGBConnectionTestBasePostgresql.Create;
end;

destructor TGBConnectionTestPostgresqlPessoa.Destroy;
begin
  FDataBase.Free;
  inherited;
end;

end.
