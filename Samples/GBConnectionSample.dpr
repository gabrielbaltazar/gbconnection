program GBConnectionSample;

uses
  Vcl.Forms,
  GBConnection.Samples.View.Main in 'GBConnection.Samples.View.Main.pas' {frmMain},
  GBConnection.Criteria.Interfaces in '..\GBConnection.Criteria.Interfaces.pas',
  GBConnection.Criteria in '..\GBConnection.Criteria.pas',
  GBConnection.Criteria.Types in '..\GBConnection.Criteria.Types.pas',
  GBConnection.Criteria.Operators in '..\GBConnection.Criteria.Operators.pas',
  GBConnection.Criteria.Utils in '..\GBConnection.Criteria.Utils.pas',
  GBConnection.Samples.Model.Empresa in 'GBConnection.Samples.Model.Empresa.pas',
  GBConnection.Monitor.LogFile in '..\Source\GBConnection.Monitor.LogFile.pas',
  GBConnection.Model.Firedac.DriverLink in '..\Source\GBConnection.Model.Firedac.DriverLink.pas',
  GBConnection.Model.Interfaces in '..\Source\GBConnection.Model.Interfaces.pas',
  GBConnection.Model.FiredacConnection in '..\Source\GBConnection.Model.FiredacConnection.pas';

{$R *.res}

begin
  Application.Initialize;
  ReportMemoryLeaksOnShutdown   := True;
  Application.MainFormOnTaskbar := True;
  Application.CreateForm(TfrmMain, frmMain);
  Application.Run;
end.
