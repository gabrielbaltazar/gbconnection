unit GBConnection.Samples.Model.Igreja;

interface

uses System.SysUtils, System.Classes, Data.DB,
     ormbr.mapping.attributes,
     ormbr.types.mapping,
     ormbr.types.lazy,
     ormbr.types.nullable,
     ormbr.mapping.register;

type
  [Entity]
  [Table('Igreja')]
  [PrimaryKey('idIgreja', AutoInc, NoSort, True)]
  TIgreja = class
  private
    FidIgreja: Integer;
    Fnome: String;

  public
    [Column('idIgreja', ftInteger)]
    property idIgreja: Integer read FidIgreja write FidIgreja;

    [Column('nome', ftString)]
    property nome: String read Fnome write Fnome;

    constructor create;
    destructor  Destroy; override;

end;

implementation

constructor TIgreja.create;
begin

end;

destructor TIgreja.Destroy;
begin

  inherited;
end;

end.
