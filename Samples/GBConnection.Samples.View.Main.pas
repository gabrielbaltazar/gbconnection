unit GBConnection.Samples.View.Main;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs,
  Data.DB, Vcl.Grids, Vcl.DBGrids,
  ormbr.dml.generator.sqlite,
  GBConnection.Criteria,
  GBConnection.Criteria.Interfaces, Vcl.StdCtrls,
  GBConnection.Model.Interfaces,
  GBConnection.Model.Factory,
  GBConnection.Dao.Interfaces,
  GBConnection.Dao.ORMBr,
  GBConnection.Samples.Model.Empresa;

type
  TfrmMain = class(TForm)
    DataSource1: TDataSource;
    mmoText: TMemo;
    procedure FormCreate(Sender: TObject);
  private
    connection : IGBConnection;
    { Private declarations }
  public
    { Public declarations }
  end;

var
  frmMain: TfrmMain;

implementation

uses
  System.Generics.Collections;

{$R *.dfm}

procedure TfrmMain.FormCreate(Sender: TObject);
var
  model : IGBModelDao<TAPIEntidadesEmpresa>;
  lista : TObjectList<TAPIEntidadesEmpresa>;
  i     : Integer;
begin

  connection := TGBFactoryConnection.New.createConnection;
  connection.Params
    .Driver(gbSQLite)
    .Database('D:\Desenvolvimento\workspace\PW3\Bin\PW3.db3')
    .&End
  .Connect;

  UseMonitorLogFile(connection);

  model := TGBModelDaoORMBr<TAPIEntidadesEmpresa>.create(connection);
  model.Validator.AssertExists('1 <> 1000');
  for i := 0 to 10 do
  begin
    lista := model.listWhere('1 = 1000');
    lista.Free;
  end;

end;

initialization
  ReportMemoryLeaksOnShutdown := True;

end.
