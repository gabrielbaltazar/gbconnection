unit GBConnection.Samples.Model.Empresa;

interface

uses
  System.SysUtils,
  System.Classes,
  Data.DB,
  GBConnection.Model.Types;

type
  [Entity]
  [Table('Empresa')]
  [PrimaryKey('idEmpresa', TSequenceType.AutoInc)]
  TAPIEntidadesEmpresa = class
  private
    FidEmpresa: Double;
    Fnome: String;

  public
    [Column('idEmpresa', ftFloat)]
    property idEmpresa: Double read FidEmpresa write FidEmpresa;

    [UniqueCol]
    [Column('nome', ftString)]
    property nome: String read Fnome write Fnome;

    constructor create;
    destructor  Destroy; override;

end;

implementation

constructor TAPIEntidadesEmpresa.create;
begin

end;

destructor TAPIEntidadesEmpresa.Destroy;
begin

  inherited;
end;

initialization
  TRegisterClass.RegisterEntity(TAPIEntidadesEmpresa);

end.
